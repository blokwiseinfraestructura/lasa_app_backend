<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Utility;

use AppBundle\Constant\Constant;
use Symfony\Component\DependencyInjection\ContainerInterface;
use RMS\PushNotificationsBundle\Message\AndroidMessage;
use RMS\PushNotificationsBundle\Message\iOSMessage;

class PushNotificationsService {
   
    private $deviceToken;
    private $deviceType;
    private $title;
    private $body;
    private $achievementCode;
    private $container;
    
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }
    
    function getAchievementCode() {
        return $this->achievementCode;
    }

    function setAchievementCode($achievementCode) {
        $this->achievementCode = $achievementCode;
    }

        
    function getDeviceToken() {
        return $this->deviceToken;
    }

    function getDeviceType() {
        return $this->deviceType;
    }

    function getTitle() {
        return $this->title;
    }

    function getBody() {
        return $this->body;
    }

    function setDeviceToken($deviceToken) {
        $this->deviceToken = $deviceToken;
    }

    function setDeviceType($deviceType) {
        $this->deviceType = $deviceType;
    }

    function setTitle($title) {
        $this->title = $title;
    }

    function setBody($body) {
        $this->body = $body;
    }

    public function sendPushNotification() {
        if ($this->getDeviceType() == Constant::APP_DEVICE_TYPE_ANDROID) {
            
            //NOTIFIES THE ANDROID DEVICE
            $messageAndroid = new AndroidMessage();
            $messageAndroid->setGCM(true); // DEFAULT TRUE FOR DEVICE GCM
            $messageAndroid->setGCMOptions(
                    array('notification' => array('body' => $this->getBody(),
                    'title' => $this->getTitle(),
                    'icon' => 'myicon',
                    'sound' => 'default')), array('data' => array('Achievement' => $this->getAchievementCode()))
            ); // SENDS MESSAGE OF FORM
            $messageAndroid->setDeviceIdentifier($this->getDeviceToken()); // GET THE TOKEN DEVICE DATABASE
            $this->container->get('rms_push_notifications')->send($messageAndroid); // GET PROPERTIES OF FILE rms_push_notifications OF config.yml    
        } elseif ($this->getDeviceType() == Constant::APP_DEVICE_TYPE_IOS) {

            //NOTIFIES THE IOS DEVICE
            $messageIos = new iOSMessage();
            $messageIos->setMessage(array('title' => $this->getTitle(), 'body' => $this->getBody())); // SENDS MESSAGE OF FORM
            $messageIos->setAPSSound('default');
            $messageIos->setAPSBadge(3);
            $messageIos->setDeviceIdentifier($this->getDeviceToken()); // GET THE TOKEN DEVICE DATABASE
            $this->container->get('rms_push_notifications')->send($messageIos); // GET PROPERTIES OF FILE rms_push_notifications OF config.yml
        }
    }

}
