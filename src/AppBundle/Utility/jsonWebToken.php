<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 19/12/16
 * Time: 8:54 AM
 */

namespace AppBundle\Utility;


use \Firebase\JWT\JWT;
use AppBundle\Constant\Constant;

class jsonWebToken {

    private $userId;
    private $storeId;
    private $date;
    private $token;

    function getUserId() {
        return $this->userId;
    }

    function getStoreId() {
        return $this->storeId;
    }

    function getDate() {
        return $this->date;
    }

    function getToken() {
        return $this->token;
    }

    function setUserId($userId) {
        $this->userId = $userId;
    }

    function setStoreId($storeId) {
        $this->storeId = $storeId;
    }

    function setDate($date) {
        $this->date = $date;
    }

    function setToken($token) {
        $this->token = $token;
    }

    public function tokenEncrypt() {

        $key = Constant::AUTHORIZATION_KEY;
        $token = array(
            "iss" => $this->getUserId(), //idusuario
            "aud" => $this->getStoreId(), //idstore
            "iat" => $this->getDate() //fecha
        );

        /**
         * IMPORTANT:
         * You must specify supported algorithms for your application. See
         * https://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-40
         * for a list of spec-compliant algorithms.
         */
        $jwt = JWT::encode($token, $key);
        $decoded = JWT::decode($jwt, $key, array('HS256'));

        /*
          NOTE: This will now be an object instead of an associative array. To get
          an associative array, you will need to cast it as such:
         */

        $decoded_array = (array) $decoded;

        /**
         * You can add a leeway to account for when there is a clock skew times between
         * the signing and verifying servers. It is recommended that this leeway should
         * not be bigger than a few minutes.
         *
         * Source: http://self-issued.info/docs/draft-ietf-oauth-json-web-token.html#nbfDef
         */
        JWT::$leeway = 60; // $leeway in seconds
        $decoded = JWT::decode($jwt, $key, array('HS256'));

        return $jwt;
    }

    public function tokenDecrypt() {
        $response = 0;
        $explode = explode(" ", $this->getToken());
        if ($explode[0] == 'Bearer') {
            try {
                $key = Constant::AUTHORIZATION_KEY;
                $decoded = JWT::decode($explode[1], $key, array('HS256'));
                /*
                  NOTE: This will now be an object instead of an associative array. To get
                  an associative array, you will need to cast it as such:
                 */

                $decoded_array = (array) $decoded;
                /**
                 * You can add a leeway to account for when there is a clock skew times between
                 * the signing and verifying servers. It is recommended that this leeway should
                 * not be bigger than a few minutes.
                 *
                 * Source: http://self-issued.info/docs/draft-ietf-oauth-json-web-token.html#nbfDef
                 */
                JWT::$leeway = 60; // $leeway in seconds
                $decoded = JWT::decode($explode[1], $key, array('HS256'));

                $response = $decoded->iss;
            } catch (Exception $e) {
                $response = 0;
            }
        } else {
            $response = 0;
        }
        return $response;
    }

}