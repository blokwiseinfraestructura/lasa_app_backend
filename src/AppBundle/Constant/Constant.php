<?php

/**
 * Created by PhpStorm.
 * User: BW_HG
 * Date: 19/12/16
 * Time: 8:55 AM
 */
namespace AppBundle\Constant;

class Constant
{
    /* ****DEFAULT VALUES****** */
    const AUTHORIZATION_KEY = 'bwstudiosKey';
    const PROGRESS_ARRAY = 'progressArray';
    const RANKING_USERS_TOTAL = 10;
    
    const ACHIEVEMENT_CODE_21 = 21;
    const ACHIEVEMENT_CODE_22 = 22;
    const ACHIEVEMENT_CODE_23 = 23;
    
    const NUMBER_CONDITION_ACHIEVEMENT_21 = 1;
    const NUMBER_CONDITION_ACHIEVEMENT_22 = 20;
    const NUMBER_CONDITION_ACHIEVEMENT_23 = 100;
    
    const APP_DEVICE_TYPE_ANDROID = 'ANDROID';
    const APP_DEVICE_TYPE_IOS = 'IOS';

    const SECONDS_TIME_LIMIT_FOR_NON_DECREASE_PROGRESS = 'secondsTimeLimitForNonDecreaseProgress';

    /* ****ENTITY CONSTANT****** */

    //Entities
    const ENTITY_ADMIN_MODULE = 'AppBundle:AdminModule';
    const ENTITY_ADMIN_USER = 'AppBundle:AdminUser';
    const ENTITY_ANSWER = 'AppBundle:Answer';
    const ENTITY_APP_CONSTANT = 'AppBundle:AppConstant';
    const ENTITY_APP_MODULE = 'AppBundle:AppModule';
    const ENTITY_APP_USER = 'AppBundle:AppUser';
    const ENTITY_APP_USER_CHALLENGE = 'AppBundle:AppUserChallenge';
    const ENTITY_APP_USER_MODULE = 'AppBundle:AppUserModule';
    const ENTITY_APP_USER_STATISTICS = 'AppBundle:AppUserStatistics';
    const ENTITY_CHALLENGE = 'AppBundle:Challenge';
    const ENTITY_CITY = 'AppBundle:City';
    const ENTITY_LEVEL = 'AppBundle:Level';
    const ENTITY_PUSH_NOTIFICATION = 'AppBundle:PushNotification';
    const ENTITY_NEWS = 'AppBundle:News';
    const ENTITY_QUESTION = 'AppBundle:Question';
    const ENTITY_READING = 'AppBundle:Reading';
    const ENTITY_TOPIC = 'AppBundle:Topic';
    const ENTITY_USER_ADMIN_MODULE = 'AppBundle:UserAdminModule';
    const ENTITY_WORK_AREA = 'AppBundle:WorkArea';
    const ENTITY_USER_IMAGE = 'AppBundle:UserImage';
    const ENTITY_USER_ACHIEVEMENT = 'AppBundle:AppUserAchievement';
    const ENTITY_ACHIEVEMENT = 'AppBundle:Achievement';
    const ENTITY_FINAL_TEST_RESULT = 'AppBundle:FinalTestResult';

    //Global entity identifier
    const ID = 'id';

    //AdminModule fields constant
    const ADMIN_MODULE_MODULE_NAME = 'moduleName';
    const ADMIN_MODULE_MODULE_CODE = 'moduleCode';
    const ADMIN_MODULE_ADMIN_USER_MODULES_MODULE = 'adminUserModulesModule';

    //AdminUser fields constant
    const ADMIN_USER_NAME = 'name';
    const ADMIN_USER_LAST_NAME = 'lastName';
    const ADMIN_USER_EMAIL = 'email';
    const ADMIN_USER_ROLE = 'role';
    const ADMIN_USER_PASSWORD = 'password';
    const ADMIN_USER_ADMIN_TOKEN = 'adminToken';
    const ADMIN_USER_RECOVER_PASSWORD_TOKEN = 'recoverPasswordToken';
    const ADMIN_USER_RECOVER_PASSWORD_TOKEN_EXPIRES_AT = 'recoverPasswordTokenExpiresAt';
    const ADMIN_USER_CREATED_AT = 'createAt';
    const ADMIN_USER_CREATE_AT = 'createAt';
    const ADMIN_USER_UPDATE_AT = 'updateAt';
    const ADMIN_USER_MODULES_USER = 'adminUserModulesUser';

    //Answer fields constant
    const ANSWER_ANSWER_DESCRIPTION = 'answerDescription';
    const ANSWER_ANSWER_IS_CORRECT = 'isCorrect';
    const ANSWER_ANSWER_QUESTION_ID = 'questionId';

    //AppConstant fields Constant
    const APP_CONSTANT_APP_CONSTANT_NAME = 'appConstantName';
    const APP_CONSTANT_APP_CONSTANT_VALUE = 'appConstantValue';
    const APP_CONSTANT_APP_CONSTANT_CODE = 'appConstantCode';

    //AppModule fields constant
    const APP_MODULE_APP_MODULE_NAME = 'appModuleName';
    const APP_MODULE_APP_MODULE_LEVEL_ID = 'levelId';

    //AppUser fields Constant
    const APP_USER_NAME = 'name';
    const APP_USER_LAST_NAME = 'lastName';
    const APP_USER_IDENTIFICATION_NUMBER = 'identificationNumber';
    const APP_USER_EMAIL = 'email';
    const APP_USER_PASSWORD = 'password';
    const APP_USER_RECOVER_PASSWORD_TOKEN = 'recoverPasswordToken';
    const APP_USER_RECOVER_PASSWORD_TOKEN_EXPIRES_AT = 'recoverPasswordTokenExpiresAt';
    const APP_USER_DEVICE_TYPE = 'deviceType';
    const APP_USER_DEVICE_TOKEN = 'deviceToken';
    const APP_USER_ACCOUNT_ID = 'accountId';
    const APP_USER_IS_LASA_WORKER = 'isLasaWorker';
    const APP_USER_HAVE_FINAL_TEST_ACCESS = 'haveFinalTestAccess';
    const APP_USER_WORK_AREA_ID = 'workAreaId';
    const APP_USER_CITY_ID = 'cityId';
    const APP_USER_LEVEL_ID = 'levelId';
    const APP_USER_APP_USER_STATISTICS_ID = 'appUserStatisticsId';
    const APP_USER_ACCESS = 'access';

    //AppUserChallenge fields constant
    const APP_USER_CHALLENGE_SCORE = 'score';
    const APP_USER_CHALLENGE_IS_CHALLENGER = 'isChallenger';
    const APP_USER_CHALLENGE_IS_WINNER = 'isWinner';
    const APP_USER_CHALLENGE_APP_USER_ID = 'appUserId';
    const APP_USER_CHALLENGE_CHALLENGE_ID = 'challengeId';

    //AppUserModule fields Constant
    const APP_USER_MODULE_SCORE = 'score';
    const APP_USER_MODULE_MAX_SCORE = 'maxScore';
    const APP_USER_MODULE_APP_MODULE_ID = 'appModuleId';
    const APP_USER_MODULE_APP_USER_ID = 'appUserId';

    //AppUserStatistics fields Constant
    const APP_USER_STATISTICS_CHALLENGES_WON = 'challengesWon';
    const APP_USER_STATISTICS_CHALLENGES_LOST = 'challengesLost';
    const APP_USER_STATISTICS_WIN_STREAK = 'winStreak';
    const APP_USER_STATISTICS_LOSE_STREAK = 'loseStreak';
    const APP_USER_STATISTICS_LAST_CHALLENGE_AT = 'lastChallengeAt';
    const APP_USER_STATISTICS_TOTAL_CHALLENGES_PLAYED = 'totalChallengesPlayed';
    const APP_USER_STATISTICS_FINAL_TEST_SPEND_TIME = 'finalTestSpendTime';
    const APP_USER_STATISTICS_TIME_PLAYED = 'timePlayed';
    const APP_USER_STATISTICS_LEVELS_UNLOCKED = 'levelsUnlocked';
    const APP_USER_STATISTICS_ACHIEVEMENTS_UNLOCKED = 'achievementsUnlocked';
    const APP_USER_STATISTICS_LAST_PRACTICE_AT = 'lastPracticeAt';
    const APP_USER_STATISTICS_MINI_GAMES_TOTAL_SCORE = 'minigamesTotalScore';

    //Challenge fields Constant
    const CHALLENGE_IDENTIFIER = 'challengeIdentifier';
    const CHALLENGE_CHALLENGE_CREATE_AT = 'challengeCreateAt';
    const CHALLENGE_CHALLENGE_FINISHED_AT = 'challengeFinishedAt';
    const CHALLENGE_IS_FINISHED = 'isFinished';
    const CHALLENGE_CHALLENGE_APP_USER = 'challengesAppUser';

    //City fields Constant
    const CITY_NAME = 'cityName';
    const CITY_APP_USERS_BY_CITY = 'appUsersByCity';

    //Level fields constant
    const LEVEL_LEVEL_DESCRIPTION = 'levelDescription';
    const LEVEL_LEVEL_CODE = 'levelCode';
    const LEVEL_LEVEL_QUESTIONS_PER_LEVEL = 'questionsPerLevel';
    const LEVEL_APP_MODULES = 'appModules';

    //PushNotification fields Constant
    const PUSH_NOTIFICATION_ID = 'pushNotificationId';
    const PUSH_NOTIFICATION_TITLE = 'title';
    const PUSH_NOTIFICATION_DESCRIPTION = 'description';
    const PUSH_NOTIFICATION_CREATE_AT = 'createAt';
    const PUSH_NOTIFICATION_ADMIN_USER_ID = 'adminUserId';

    //News fields constant
    const NEWS_NEWS_TITTLE = 'newsTittle';
    const NEWS_NEWS_DESCRIPTION = 'newsDescription';
    const NEWS_NEWS_IMAGE_PATH = 'newsImagePath';
    const NEWS_NEWS_IMAGE_NAME = 'newsImageName';
    const NEWS_NEWS_FILE = 'file';
    const NEWS_CREATE_AT = 'createAt';

    //Question fields constant
    const QUESTION_QUESTION_DESCRIPTION = 'questionDescription';
    const QUESTION_QUESTION_TOPIC_ID = 'topicId';
    const QUESTION_QUESTION_LEVEL_ID = 'levelId';
    const QUESTION_QUESTION_ANSWERS = 'answers';

    //Reading fields constant
    const READING_READING_ID = 'readingId';
    const READING_READING_DESCRIPTION = 'readingDescription';
    const READING_READING_IMAGE_PATH = 'readingImagePath';
    const READING_READING_IMAGE_NAME = 'readingImageName';
    const READING_READING_TOPIC_ID = 'topicId';
    const READING_READING_LEVEL_ID = 'levelId';
    const READING_READING_FILE = 'file';

    //Topic fields constant
    const TOPIC_TOPIC_DESCRIPTION = 'topicDescription';
    const TOPIC_TOPIC_CODE = 'topicCode';
    const TOPIC_TOPIC_QUESTIONS_PER_TOPIC = 'questionsPerTopic';

    //UserAdminModule fields constant
    const USER_ADMIN_MODULE_ID = 'adminModuleId';
    const USER_ADMIN_MODULE_IS_AVAILABLE = 'isAvailable';
    const USER_ADMIN_MODULE_ADMIN_MODULE_ID = 'adminModuleId';
    const USER_ADMIN_MODULE_ADMIN_USER_ID = 'adminUserId';

    //WorkArea fields constant
    const WORK_AREA_WORK_AREA_NAME = 'workAreaName';
    const WORK_AREA_APP_USERS_BY_WORK_AREA = 'appUsersByWorkArea';
    
    //Achievement fields constant
    const ACHIEVEMENT_NAME_ID = 'achievementId';
    const ACHIEVEMENT_NAME = 'achievementName';
    const ACHIEVEMENT_CODE = 'achievementCode';

    //AppUserAchievement Constant
    const APP_USER_ACHIEVEMENT_ID = 'achievementId';
    const APP_USER_ACHIEVEMENT_APP_USER_ID = 'appUserId';

    /* ****RESPONSE MESSAGES & STATUS**** */

    // Messages
    const MESSAGE_RESPONSE_MESSAGE = 'message';
    const MESSAGE_RESPONSE_ERROR_FATAL = 'fatal error';
    const MESSAGE_RESPONSE_JSON_INVALID = 'Invalid json';
    const MESSAGE_RESPONSE_STATUS = 'status';
    const MESSAGE_RESPONSE_SUCCESSFUL = 'successful';
    const MESSAGE_MISSING_PARAMETERS = 'missing parameters';
    const MESSAGE_INVALID_PARAMETERS = 'invalid parameters';
    const MESSAGE_INVALID_AUTHORIZATION = 'invalid authorization';
    const MESSAGE_MISSING_AUTHORIZATION = 'missing authorization';
    const MESSAGE_USER_DOES_NOT_EXIST = 'user does not exist';
    const MESSAGE_NO_TOPICS_FOUND = 'no topics found';
    const MESSAGE_TOPIC_DOES_NOT_EXIST_OR_NOT_FOUND = 'topic does not exist or not found';
    const MESSAGE_NO_LEVELS_FOUND = 'no levels found';
    const MESSAGE_LEVEL_DOES_NOT_EXIST_OR_NOT_FOUND = 'level does not exist or not found';
    const MESSAGE_NO_QUESTIONS_FOUND = 'no questions found';
    const MESSAGE_REQUESTED_QUESTION_NOT_FOUND = 'requested question not found';
    const MESSAGE_REQUESTED_CITY_NOT_FOUND = 'requested city not found';
    const MESSAGE_REQUESTED_WORK_AREA_NOT_FOUND = 'requested workArea not found';
    const MESSAGE_REQUESTED_NEWS_NOT_FOUND = 'requested news not found';
    const MESSAGE_REQUESTED_READING_NOT_FOUND = 'requested reading not found';
    const MESSAGE_QUESTION_ID_MUST_BE_A_NUMBER = 'questionId must be a number';
    const MESSAGE_QUESTION_WAS_DELETED_CORRECTLY = 'question successfully deleted';
    const MESSAGE_NEWS_WAS_DELETED_CORRECTLY = 'news successfully deleted';
    const MESSAGE_READING_WAS_DELETED_CORRECTLY = 'reading successfully deleted';
    const MESSAGE_CHALLENGE_WAS_DELETED_CORRECTLY = 'challenge successfully deleted';
    const MESSAGE_NO_ANSWERS_FOUND = 'no answers found';
    const MESSAGE_FOUR_ANSWERS_EXACTLY_IS_MANDATORY = 'four answers is mandatory';
    const MESSAGE_INVALID_EMAIL_FORMAT = 'invalid email format';
    const MESSAGE_EMAIL_IS_ALREADY_IN_USE = 'email is already in use';
    const MESSAGE_EMAIL_OR_PASSWORD_DOES_NOT_MATCH = 'email or password does not match';
    const MESSAGE_NO_USERS_FOUND = 'no users found';
    const MESSAGE_IDENTIFICATION_NUMBER_ALREADY_EXIST = 'identification number already exist';
    const MESSAGE_ADMIN_USER_ID_MUST_BE_A_NUMBER = 'adminUserId must be a number';
    const MESSAGE_USER_WAS_DELETED_CORRECTLY = 'user successfully deleted';
    const MESSAGE_UNAUTHORIZED_MODULE = 'unauthorized module';
    const MESSAGE_WRONG_LEVEL_TO_THIS_MODULE = 'wrong level to this module';
    const MESSAGE_EMAIL_SENT_SUCCESSFULY = 'email sent successfully';
    const MESSAGE_TOKEN_HAS_EXPIRED = 'token has expired';
    const MESSAGE_INVALID_TOKEN = 'invalid token';
    const MESSAGE_IS_ADMIN_MUST_BE_A_BOOL = 'isAdmin must be a bool';
    const MESSAGE_PASSWORD_MISMATCH = 'password mismatch';
    const MESSAGE_PASSWORD_MATCH = 'password match';
    const MESSAGE_NO_NEWS_FOUND = 'no news found';
    const MESSAGE_NO_PUSH_NOTIFICATIONS_FOUND = 'no push notifications found';
    const MESSAGE_THERE_ARE_NOT_USERS_TO_CHALLENGE = 'there aren\'t users to challenge';
    const MESSAGE_CHALLENGE_DOES_NOT_EXIST = 'challenge does not exist';
    const MESSAGE_THE_CHALLENGE_IS_OVER = 'the challenge is over';
    const MESSAGE_CHALLENGE_CANNOT_BE_DELETED = 'this challenge cannot be deleted';
    const MESSAGE_ACCESS_DENIED = 'Access denied';
    const MESSAGE_ERROR_SAVING = 'Error saving';


    //Spanish Messages
    const SPANISH_MESSAGE_EMAIL_SENT_SUCCESSFULY = 'correo satisfactoriamente enviado';
    const SPANISH_MESSAGE_EMAIL_IS_ALREADY_IN_USE = 'este correo ya esta en uso';
    const SPANISH_MESSAGE_RESPONSE_MESSAGE = 'mensaje';
    const SPANISH_MESSAGE_RESPONSE_ERROR_FATAL = 'error fatal';
    const SPANISH_MESSAGE_RESPONSE_JSON_INVALID = 'Invalid json';
    const SPANISH_MESSAGE_RESPONSE_STATUS = 'estado';
    const SPANISH_MESSAGE_RESPONSE_SUCCESSFUL = 'satisfactorio';
    const SPANISH_MESSAGE_MISSING_PARAMETERS = 'faltan parametros';
    const SPANISH_MESSAGE_INVALID_PARAMETERS = 'parametros no válidos';
    const SPANISH_MESSAGE_INVALID_AUTHORIZATION = 'autorización no válida';
    const SPANISH_MESSAGE_MISSING_AUTHORIZATION = 'falta autorización';
    const SPANISH_MESSAGE_USER_DOES_NOT_EXIST = 'el usuario no existe';
    const SPANISH_MESSAGE_NO_TOPICS_FOUND = 'no se encontraron tópicos';
    const SPANISH_MESSAGE_TOPIC_DOES_NOT_EXIST_OR_NOT_FOUND = 'tópico no encontrado o no existe';
    const SPANISH_MESSAGE_NO_LEVELS_FOUND = 'no se encontraron niveles';
    const SPANISH_MESSAGE_LEVEL_DOES_NOT_EXIST_OR_NOT_FOUND = 'nivel no encontrado o no existe';
    const SPANISH_MESSAGE_NO_QUESTIONS_FOUND = 'no se encontraron preguntas';
    const SPANISH_MESSAGE_REQUESTED_QUESTION_NOT_FOUND = 'la pregunta solicitada no fue encontrada';
    const SPANISH_MESSAGE_REQUESTED_CITY_NOT_FOUND = 'la ciudad solicitada no fue encontrada';
    const SPANISH_MESSAGE_REQUESTED_WORK_AREA_NOT_FOUND = 'area de trabajo no fue encontrada';
    const SPANISH_MESSAGE_REQUESTED_NEWS_NOT_FOUND = 'la noticia soliciatada no fue encontrada';
    const SPANISH_MESSAGE_REQUESTED_READING_NOT_FOUND = 'la lectura soliciatada no fue encontrada';
    const SPANISH_MESSAGE_QUESTION_ID_MUST_BE_A_NUMBER = 'questionId debería ser un número';
    const SPANISH_MESSAGE_QUESTION_WAS_DELETED_CORRECTLY = 'la pregunta fué eliminada correctamente';
    const SPANISH_MESSAGE_NEWS_WAS_DELETED_CORRECTLY = 'la noticia fué eliminada correctamente';
    const SPANISH_MESSAGE_NO_ANSWERS_FOUND = 'no se encontraron preguntas';
    const SPANISH_MESSAGE_FOUR_ANSWERS_EXACTLY_IS_MANDATORY = 'cuatro respuestas es obligación';
    const SPANISH_MESSAGE_INVALID_EMAIL_FORMAT = 'formato de correo no válido';
    const SPANISH_MESSAGE_EMAIL_OR_PASSWORD_DOES_NOT_MATCH = 'el correo o la contraseña no coinciden';
    const SPANISH_MESSAGE_NO_USERS_FOUND = 'no se encontraron usuarios';
    const SPANISH_MESSAGE_IDENTIFICATION_NUMBER_ALREADY_EXIST = 'el numero de identificación ya existe';
    const SPANISH_MESSAGE_ADMIN_USER_ID_MUST_BE_A_NUMBER = 'adminUserId debería ser un número';
    const SPANISH_MESSAGE_USER_WAS_DELETED_CORRECTLY = 'usuario satisfactoriamente eliminado';
    const SPANISH_MESSAGE_UNAUTHORIZED_MODULE = 'módulo no autorizado';
    const SPANISH_MESSAGE_TOKEN_HAS_EXPIRED = 'el token ha expirado';
    const SPANISH_MESSAGE_INVALID_TOKEN = 'el token no es válido';
    const SPANISH_MESSAGE_IS_ADMIN_MUST_BE_A_BOOL = 'isAdmin debería ser un booleano';
    const SPANISH_MESSAGE_PASSWORD_MISMATCH = 'la contraseña no coincide';
    const SPANISH_MESSAGE_PASSWORD_MATCH = 'la contraseña coincide';
    const SPANISH_MESSAGE_NO_NEWS_FOUND = 'no se encontraron noticias';
    const SPANISH_MESSAGE_NO_PUSH_NOTIFICATIONS_FOUND = 'no se encontraron notificaciones push';
    const SPANISH_MESSAGE_ACCESS_DENIED = 'Acceso denegado';


    //Status
    const STATUS_TRUE = true;
    const STATUS_FALSE = false;

    /* ****JSON FIELDS**** */

    //Dates
    const JSON_DATE_SERVER_CURRENT_DATE = 'serverDate';

    //Helpful Constant
    const JSON_NEW_PASSWORD = 'newPassword';
    const JSON_RECOVER_TOKEN = 'recoverToken';
    const JSON_IS_ADMIN = 'isAdmin';
    const JSON_APP_USER_ENABLE_FINAL_TEST = 'enableFinalTest';
    const JSON_APP_PERCENT_FOR_LEVEL_UP = 'percentForLevelUp';
    const JSON_APP_SECONDS_TIME_LIMIT_FOR_FINAL_TEST = 'secondsTimeLimitForFinalTest';
    const JSON_EMAIL_EMAIL_SUBJECT = 'emailSubject';
    const JSON_EMAIL_EMAIL_BODY = 'emailBody';
    const JSON_OPPONENT_DATA = 'opponentData';
    const JSON_APP_USER_MY_SCORE = 'myScore';

    //arithmetic constant
    const PERCENTAGE_TO_GET_NEXT_LEVEL = 0.8;
    const REAL_PERCENTAGE_TO_GET_NEXT_LEVEL = Constant::PERCENTAGE_TO_GET_NEXT_LEVEL * 100;
    const DIVISOR = Constant::PERCENTAGE_TO_GET_NEXT_LEVEL * 3;

    //parameters
    const JSON_APP_GENERAL_PARAMETERS = 'appGeneralParameters';
    const JSON_APP_AVAILABLE_APP_MODULES_ARRAY = 'availableAppModulesArray';
    const JSON_APP_USER_GENERAL_LEVEL_PROGRESS = 'generalLevelProgress';
    const JSON_APP_ELAPSED_TIME = 'elapsedTime';

    //Useful constant
    const JSON_USEFUL_IS_ADMIN_EDIT = 'isAdminEdit';
    const JSON_USEFUL_DEFAULT_NEWS_IMAGES_DIRECTORY = '/web/uploads/news/';
    const JSON_USEFUL_DEFAULT_READINGS_IMAGES_DIRECTORY = '/web/uploads/readings/';
    const JSON_PENDING_CHALLENGES_FOR_ME_ARRAY = 'pendingChallengesForMeArray';
    const JSON_PENDING_CHALLENGES_FOR_OTHERS_ARRAY = 'pendingChallengesForOthersArray';
    const JSON_CHALLENGES_HISTORY_ARRAY = 'challengesHistoryArray';

    //AdminModule fields
    const JSON_ADMIN_MODULE = 'module';
    const JSON_ADMIN_MODULE_MODULES = 'modules';
    const JSON_ADMIN_MODULE_MODULES_ARRAY = 'modulesArray';
    const JSON_ADMIN_MODULE_MODULE_ID = 'moduleId';
    const JSON_ADMIN_MODULE_MODULE_NAME = 'moduleName';
    const JSON_ADMIN_MODULE_MODULE_CODE = 'moduleCode';
    const JSON_ADMIN_MODULE_ADMIN_USER_MODULES_MODULE = 'adminUserModulesModule';

    //AdminUser fields constant
    const JSON_ADMIN_USER = 'adminUser';
    const JSON_ADMIN_USER_DATA = 'userData';
    const JSON_ADMIN_USERS = 'adminUsers';
    const JSON_ADMIN_USERS_ARRAY = 'adminUsersArray';
    const JSON_ADMIN_USER_ID = 'adminUserId';
    const JSON_ADMIN_USER_NAME = 'name';
    const JSON_ADMIN_USER_LAST_NAME = 'lastName';
    const JSON_ADMIN_USER_EMAIL = 'email';
    const JSON_ADMIN_USER_ROLE = 'role';
    const JSON_ADMIN_USER_PASSWORD = 'password';
    const JSON_ADMIN_USER_ADMIN_TOKEN = 'adminToken';
    const JSON_ADMIN_USER_RECOVER_PASSWORD_TOKEN = 'recoverPasswordToken';
    const JSON_ADMIN_USER_RECOVER_PASSWORD_TOKEN_EXPIRES_AT = 'recoverPasswordTokenExpiresAt';
    const JSON_ADMIN_USER_CREATED_AT = 'createAt';
    const JSON_ADMIN_USER_CREATE_AT = 'createAt';
    const JSON_ADMIN_USER_UPDATE_AT = 'updateAt';
    const JSON_ADMIN_USER_MODULES_USER = 'adminUserModulesUser';

    //AppModule fields constant
    const JSON_APP_MODULE_APP_MODULE = 'appModule';
    const JSON_APP_MODULE_APP_MODULES = 'appModules';
    const JSON_APP_MODULE_APP_MODULES_ARRAY = 'appModulesArray';
    const JSON_APP_USER_MODULE_APP_MODULES_PROGRESS_ARRAY = 'userAppModulesProgressArray';
    const JSON_APP_MODULE_APP_MODULE_ID = 'appModuleId';
    const JSON_APP_MODULE_APP_MODULE_NAME = 'appModuleName';
    const JSON_APP_MODULE_APP_MODULE_LEVEL_ID = 'levelId';

    //AppUser fields Constant
    const JSON_APP_USER = 'appUser';
    const JSON_APP_USER_DATA = 'userData';
    const JSON_APP_RANKING_USER_POSITION = 'userPosition';
    const JSON_APP_USERS = 'appUsers';
    const JSON_APP_USERS_ARRAY = 'appUsersArray';
    const JSON_APP_USER_ID = 'userId';
    const JSON_APP_USER_NAME = 'name';
    const JSON_APP_USER_IMAGE = 'userImage';
    const JSON_APP_USER_LAST_NAME = 'lastName';
    const JSON_APP_USER_IDENTIFICATION_NUMBER = 'identificationNumber';
    const JSON_APP_USER_EMAIL = 'email';
    const JSON_APP_USER_PASSWORD = 'password';
    const JSON_APP_USER_RECOVER_PASSWORD_TOKEN = 'recoverPasswordToken';
    const JSON_APP_USER_RECOVER_PASSWORD_TOKEN_EXPIRES_AT = 'recoverPasswordTokenExpiresAt';
    const JSON_APP_USER_DEVICE_TYPE = 'deviceType';
    const JSON_APP_USER_DEVICE_TOKEN = 'deviceToken';
    const JSON_APP_USER_ACCOUNT_ID = 'accountId';
    const JSON_APP_USER_IS_LASA_WORKER = 'isLasaWorker';
    const JSON_APP_USER_HAVE_FINAL_TEST_ACCESS = 'haveFinalTestAccess';
    const JSON_APP_USER_WORK_AREA_ID = 'workAreaId';
    const JSON_APP_USER_CITY_ID = 'cityId';
    const JSON_APP_USER_LEVEL_ID = 'levelId';
    const JSON_APP_USER_LEVEL = 'userLevel';
    const JSON_APP_USER_APP_USER_STATISTICS_ID = 'appUserStatisticsId';
    const JSON_APP_USER_ACCESS = 'access';

    //AppUserChallenge fields constant
    const JSON_APP_USER_CHALLENGE = 'appUserChallenge';
    const JSON_APP_USER_CHALLENGES = 'appUserChallenges';
    const JSON_APP_USER_CHALLENGES_ARRAY = 'appUserChallengesArray';
    const JSON_APP_USER_CHALLENGE_ID = 'challengeId';
    const JSON_APP_USER_CHALLENGE_IS_CHALLENGER = 'isChallenger';
    const JSON_APP_USER_CHALLENGE_SCORE = 'score';
    const JSON_APP_USER_CHALLENGE_IS_WINNER = 'isWinner';
    const JSON_APP_USER_CHALLENGE_APP_USER_ID = 'appUserId';
    const JSON_APP_USER_CHALLENGE_CHALLENGE_ID = 'challengeId';

    //AppUserModule fields Constant
    const JSON_APP_USER_MODULE_SCORE = 'score';
    const JSON_APP_USER_MODULE_MAX_SCORE = 'maxScore';
    const JSON_APP_USER_MODULE_APP_MODULE_ID = 'appModuleId';
    const JSON_APP_USER_MODULE_APP_USER_ID = 'appUserId';

    //AppUserStatistics fields Constant
    const JSON_APP_USER_STATISTICS = 'userStatistics';
    const JSON_APP_USER_STATISTICS_ID = 'userStatisticsId';
    const JSON_APP_USER_STATISTICS_CHALLENGES_WON = 'challengesWon';
    const JSON_APP_USER_STATISTICS_CHALLENGES_LOST = 'challengesLost';
    const JSON_APP_USER_STATISTICS_WIN_STREAK = 'winStreak';
    const JSON_APP_USER_STATISTICS_LOSE_STREAK = 'loseStreak';
    const JSON_APP_USER_STATISTICS_LAST_CHALLENGE_AT = 'lastChallengeAt';
    const JSON_APP_USER_STATISTICS_TOTAL_CHALLENGES_PLAYED = 'totalChallengesPlayed';
    const JSON_APP_USER_STATISTICS_TIME_PLAYED = 'timePlayed';
    const JSON_APP_USER_STATISTICS_FINAL_TEST_SPEND_TIME = 'finalTestSpendTime';
    const JSON_APP_USER_STATISTICS_LEVELS_UNLOCKED = 'levelsUnlocked';
    const JSON_APP_USER_STATISTICS_ACHIEVEMENTS_UNLOCKED = 'achievementsUnlocked';
    const JSON_APP_USER_STATISTICS_LAST_PRACTICE_AT = 'lastPracticeAt';
    const JSON_APP_USER_STATISTICS_MINI_GAMES_TOTAL_SCORE = 'minigamesTotalScore';

    //Challenge fields constant
    const JSON_CHALLENGE_ID = 'challengeId';
    const JSON_CHALLENGE_DATA = 'challengeData';
    const JSON_CHALLENGE_CHALLENGE = 'challenge';
    const JSON_CHALLENGE_CHALLENGES = 'challenges';
    const JSON_CHALLENGE_CHALLENGES_ARRAY = 'challengesArray';
    const JSON_CHALLENGE_IDENTIFIER = 'challengeIdentifier';
    const JSON_CHALLENGE_CHALLENGE_CREATE_AT = 'challengeCreateAt';
    const JSON_CHALLENGE_CHALLENGE_FINISHED_AT = 'challengeFinishedAt';
    const JSON_CHALLENGE_IS_FINISHED = 'isFinished';
    const JSON_CHALLENGE_CHALLENGE_APP_USER = 'challengesAppUser';

    //City fields Constant
    const JSON_CITY = 'city';
    const JSON_CITIES = 'cities';
    const JSON_CITIES_ARRAY = 'citiesArray';
    const JSON_CITY_ID = 'cityId';
    const JSON_CITY_NAME = 'cityName';
    const JSON_CITY_APP_USERS_BY_CITY = 'appUsersByCity';

    //answer fields
    const JSON_ANSWER = 'answer';
    const JSON_ANSWER_ANSWERS = 'answers';
    const JSON_ANSWER_ANSWERS_ARRAY = 'answersArray';
    const JSON_ANSWER_ANSWER_ID = 'answerId';
    const JSON_ANSWER_ANSWER_DESCRIPTION = 'answerDescription';
    const JSON_ANSWER_ANSWER_IS_CORRECT = 'isCorrect';
    const JSON_ANSWER_ANSWER_QUESTION_ID = 'questionId';

    //AppConstant fields Constant
    const JSON_APP_CONSTANT_APP_CONSTANT_ID = 'appConstantId';
    const JSON_APP_CONSTANT_APP_CONSTANT = 'appConstant';
    const JSON_APP_CONSTANT_APP_CONSTANTS = 'appConstants';
    const JSON_APP_CONSTANT_APP_CONSTANTS_ARRAY = 'appConstantArray';
    const JSON_APP_CONSTANT_APP_CONSTANT_NAME = 'appConstantName';
    const JSON_APP_CONSTANT_APP_CONSTANT_VALUE = 'appConstantValue';

    //Level fields constant
    const JSON_LEVEL = 'level';
    const JSON_LEVEL_LEVELS = 'levels';
    const JSON_LEVEL_LEVELS_ARRAY = 'levelsArray';
    const JSON_LEVEL_APP_LEVELS_ARRAY = 'appLevels';
    const JSON_LEVEL_LEVEL_ID = 'levelId';
    const JSON_LEVEL_LEVEL_DESCRIPTION = 'levelDescription';
    const JSON_LEVEL_LEVEL_CODE = 'levelCode';
    const JSON_LEVEL_LEVEL_QUESTIONS_PER_LEVEL = 'questionsPerLevel';
    const JSON_LEVEL_APP_MODULES = 'appModules';
    const JSON_LEVEL_LEVEL_QUESTIONS_ARRAY = 'questionsArray';

    //PushNotification fields Constant
    const JSON_PUSH_NOTIFICATION_DATA = 'pushNotificationData';
    const JSON_PUSH_NOTIFICATION = 'pushNotification';
    const JSON_PUSH_NOTIFICATIONS = 'pushNotifications';
    const JSON_PUSH_NOTIFICATIONS_ARRAY = 'pushNotificationsArray';
    const JSON_PUSH_NOTIFICATION_ID = 'pushNotificationId';
    const JSON_PUSH_NOTIFICATION_TITLE = 'title';
    const JSON_PUSH_NOTIFICATION_DESCRIPTION = 'description';
    const JSON_PUSH_NOTIFICATION_CREATE_AT = 'createAt';
    const JSON_PUSH_NOTIFICATION_ADMIN_USER_ID = 'adminUserId';

    //News fields constant
    const JSON_NEWS_NEWS_TITLE = 'newsTitle';
    const JSON_NEWS_NEWS = 'news';
    const JSON_NEWS_NEWS_ARRAY = 'newsArray';
    const JSON_NEWS_NEWS_ID = 'newsId';
    const JSON_NEWS_NEWS_DESCRIPTION = 'newsDescription';
    const JSON_NEWS_NEWS_IMAGE_PATH = 'newsImagePath';
    const JSON_NEWS_NEWS_IMAGE_NAME = 'newsImageName';
    const JSON_NEWS_NEWS_IMAGE_FILE = 'imageFile';
    const JSON_NEWS_CREATE_AT = 'createAt';

    //Question fields constant
    const JSON_QUESTION = 'question';
    const JSON_QUESTION_QUESTIONS = 'questions';
    const JSON_QUESTION_QUESTIONS_ARRAY = 'questionsArray';
    const JSON_QUESTION_APP_QUESTIONS_ARRAY = 'appQuestions';
    const JSON_QUESTION_QUESTION_ID = 'questionId';
    const JSON_QUESTION_QUESTION_DESCRIPTION = 'questionDescription';
    const JSON_QUESTION_QUESTION_TOPIC_ID = 'topicId';
    const JSON_QUESTION_QUESTION_LEVEL_ID = 'levelId';
    const JSON_QUESTION_QUESTION_ANSWERS = 'answers';
    const JSON_QUESTION_QUESTION_ANSWERS_ARRAY = 'answersArray';


    //Reading fields constant
    const JSON_READING_READING_ID = 'readingId';
    const JSON_READING_READING = 'reading';
    const JSON_READING_READINGS = 'readings';
    const JSON_READING_READINGS_ARRAY = 'readingsArray';
    const JSON_READING_APP_READINGS_ARRAY = 'appReadingsArray';
    const JSON_READING_READING_DESCRIPTION = 'readingDescription';
    const JSON_READING_READING_IMAGE_PATH = 'readingImagePath';
    const JSON_READING_READING_IMAGE_NAME = 'readingImageName';
    const JSON_READING_READING_FILE = 'file';


    //Topic fields constant
    const JSON_TOPIC = 'topic';
    const JSON_TOPIC_TOPICS = 'topics';
    const JSON_TOPIC_TOPICS_ARRAY = 'topicsArray';
    const JSON_TOPIC_APP_TOPICS_ARRAY = 'appTopics';
    const JSON_TOPIC_TOPIC_ID = 'topicId';
    const JSON_TOPIC_TOPIC_DESCRIPTION = 'topicDescription';
    const JSON_TOPIC_TOPIC_CODE = 'topicCode';
    const JSON_TOPIC_TOPIC_QUESTIONS_PER_TOPIC = 'questionsPerTopic';
    const JSON_TOPIC_TOPIC_QUESTIONS_ARRAY = 'questionsArray';

    //UserAdminModule fields constant
    const JSON_USER_ADMIN_MODULE = 'adminModule';
    const JSON_USER_ADMIN_MODULES = 'adminModules';
    const JSON_USER_ADMIN_MODULES_ARRAY = 'adminModulesArray';
    const JSON_USER_ADMIN_MODULE_ID = 'adminModuleId';
    const JSON_USER_ADMIN_MODULE_IS_AVAILABLE = 'isAvailable';
    const JSON_USER_ADMIN_MODULE_ADMIN_MODULE_ID = 'adminModuleId';
    const JSON_USER_ADMIN_MODULE_ADMIN_USER_ID = 'adminUserId';

    //WorkArea fields constant
    const JSON_WORK_AREA = 'workArea';
    const JSON_WORK_AREAS = 'workAreas';
    const JSON_WORK_AREAS_ARRAY = 'workAreasArray';
    const JSON_WORK_AREA_ID = 'workAreaId';
    const JSON_WORK_AREA_WORK_AREA_NAME = 'workAreaName';
    const JSON_WORK_AREA_APP_USERS_BY_WORK_AREA = 'appUsersByWorkArea';
    
    //AppUser fields Ranking
    const JSON_APP_RANKING = 'appRanking';

    //Achievement fields constant
    const JSON_APP_ACHIEVEMENT = 'achievement';
    const JSON_APP_ACHIEVEMENTS = 'achievements';
    const JSON_APP_ACHIEVEMENTS_ARRAY = 'achievementsArray';
    const JSON_APP_ACHIEVEMENT_NAME_ID = 'achievementId';
    const JSON_APP_ACHIEVEMENT_NAME = 'achievementName';
    const JSON_APP_ACHIEVEMENT_CODE = 'achievementCode';
    const JSON_APP_ACHIEVEMENT_DETAIL = 'achievementDetail';

    //AppUserAchievement fields Constant
    const JSON_APP_USER_ACHIEVEMENT_ID = 'achievementId';
    const JSON_APP_USER_ACHIEVEMENT_APP_USER_ID = 'appUserId';
    const JSON_APP_USER_ACHIEVEMENTS_ARRAY = 'userAchievementsArray';

    const JSON_FINAL_TEST_RESULT = 'finalTestResults';
    const JSON_FINAL_TEST_RESULT_RIGHT_ANSWERS = 'rightAnswers';
    const JSON_FINAL_TEST_RESULT_WRONG_ANSWERS = 'wrongAnswers';
}