<?php
/**
 * Created by PhpStorm.
 * User: higom
 * Date: 30/12/2016
 * Time: 2:20 PM
 */

namespace AppBundle\Service;

use AppBundle\Entity\AppUser;
use AppBundle\Entity\AppUserModule;
use AppBundle\Entity\AppUserStatistics;
use AppBundle\Entity\FinalTestResult;
use AppBundle\Utility\jsonWebToken;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;
use AppBundle\Constant\Constant;
use Symfony\Component\Validator\Tests\Fixtures\ConstraintA;

class AppModuleService
{
    private $repositoryAdminModule;
    private $repositoryAdminUser;
    private $repositoryAnswer;
    private $repositoryAppModule;
    private $repositoryAppUser;
    private $repositoryAppUserModule;
    private $repositoryAppUserStatistics;
    private $repositoryCity;
    private $repositoryLevel;
    private $repositoryQuestion;
    private $repositoryTopic;
    private $repositoryUserAdminModule;
    private $repositoryWorkArea;
    private $repositoryFinalTestResult;
    private $container;
    private $em;

    public function __construct(EntityManager $entityManager, ContainerInterface $container)
    {
        $this->em = $entityManager;
        $this->container = $container;
        $this->repositoryAdminModule = $this->em->getRepository(Constant::ENTITY_ADMIN_MODULE);
        $this->repositoryAdminUser = $this->em->getRepository(Constant::ENTITY_ADMIN_USER);
        $this->repositoryAnswer = $this->em->getRepository(Constant::ENTITY_ANSWER);
        $this->repositoryAppModule = $this->em->getRepository(Constant::ENTITY_APP_MODULE);
        $this->repositoryAppUser = $this->em->getRepository(Constant::ENTITY_APP_USER);
        $this->repositoryAppUserModule = $this->em->getRepository(Constant::ENTITY_APP_USER_MODULE);
        $this->repositoryAppUserStatistics = $this->em->getRepository(Constant::ENTITY_APP_USER_STATISTICS);
        $this->repositoryCity = $this->em->getRepository(Constant::ENTITY_CITY);
        $this->repositoryLevel = $this->em->getRepository(Constant::ENTITY_LEVEL);
        $this->repositoryQuestion = $this->em->getRepository(Constant::ENTITY_QUESTION);
        $this->repositoryTopic = $this->em->getRepository(Constant::ENTITY_TOPIC);
        $this->repositoryUserAdminModule = $this->em->getRepository(Constant::ENTITY_USER_ADMIN_MODULE);
        $this->repositoryWorkArea = $this->em->getRepository(Constant::ENTITY_WORK_AREA);
        $this->repositoryFinalTestResult = $this->em->getRepository(Constant::ENTITY_FINAL_TEST_RESULT);
    }

    public function registerAppUserModules($userId, $appModuleId)
    {
        $response = null;
        $appUserEntity = $this->repositoryAppUser->find($userId);
        $appModuleEntity = $this->repositoryAppModule->find($appModuleId);

        if (count($appUserEntity) > 0 && count($appModuleEntity) > 0) {

            $appUserModuleEntity = new AppUserModule();

            $appUserModuleEntity->setAppUserId($appUserEntity);
            $appUserModuleEntity->setAppModuleId($appModuleEntity);
            $appUserModuleEntity->setScore(0);
            $appUserModuleEntity->setMaxScore(0);
            $this->em->persist($appUserModuleEntity);
            $this->em->flush();

        } else {

            $appUserModuleEntity = null;
        }

        return $appUserModuleEntity;
    }

    public function updateModuleProgress(Request $request)
    {
        $requestData = array(
            Constant::JSON_APP_MODULE_APP_MODULE => $request->request->get(Constant::JSON_APP_MODULE_APP_MODULE),
            Constant::JSON_APP_USER_MODULE_SCORE => $request->request->get(Constant::JSON_APP_USER_MODULE_SCORE),
            Constant::JSON_APP_ELAPSED_TIME => $request->request->get(Constant::JSON_APP_ELAPSED_TIME),
            Constant::JSON_APP_ACHIEVEMENTS_ARRAY => $request->request->get(Constant::JSON_APP_ACHIEVEMENTS_ARRAY),
        );
        $jsonResponse = array();

        $apiAuthorization = $request->headers->get('Authorization');

        if ($apiAuthorization != null ) {

            $jwt = new jsonWebToken();
            $jwt->setToken($apiAuthorization);

            if ($jwt->tokenDecrypt() > 0) {

                $userId = $jwt->tokenDecrypt();
                $appUserEntity = $this->repositoryAppUser->findOneBy(array(Constant::ID => $userId));

                if (count($appUserEntity) > 0) {

                    if (is_numeric($requestData[Constant::JSON_APP_MODULE_APP_MODULE][Constant::JSON_APP_MODULE_APP_MODULE_ID]) &&
                        is_numeric($requestData[Constant::JSON_APP_MODULE_APP_MODULE][Constant::JSON_LEVEL][Constant::JSON_LEVEL_LEVEL_ID]) &&
                        is_numeric($requestData[Constant::JSON_APP_MODULE_APP_MODULE][Constant::JSON_LEVEL][Constant::JSON_LEVEL_LEVEL_CODE]) &&
                        is_numeric($requestData[Constant::JSON_APP_USER_MODULE_SCORE]) && is_numeric($requestData[Constant::JSON_APP_ELAPSED_TIME])) {

                        $levelEntity = $this->repositoryLevel->findOneBy(array(Constant::ID => $requestData[Constant::JSON_APP_MODULE_APP_MODULE][Constant::JSON_LEVEL][Constant::JSON_LEVEL_LEVEL_ID],
                            Constant::LEVEL_LEVEL_CODE => $requestData[Constant::JSON_APP_MODULE_APP_MODULE][Constant::JSON_LEVEL][Constant::JSON_LEVEL_LEVEL_CODE]));

                        if (count($levelEntity) > 0) {

                            $appUserModuleEntity = $this->repositoryAppUserModule->findOneBy(array(Constant::APP_USER_MODULE_APP_MODULE_ID =>
                                $requestData[Constant::JSON_APP_MODULE_APP_MODULE][Constant::JSON_APP_MODULE_APP_MODULE_ID], Constant::APP_USER_MODULE_APP_USER_ID => $appUserEntity));

                            if (count($appUserModuleEntity) > 0) {

                                $realModuleLevelEntity = $this->repositoryLevel->findOneBy(array(Constant::ID => $appUserModuleEntity->getAppModuleId()->getLevelId()->getId()));

                                if($levelEntity == $realModuleLevelEntity){

                                    if ($requestData[Constant::JSON_APP_USER_MODULE_SCORE] < 0) {

                                        $appUserModuleEntity->setScore(0);
                                        $this->em->persist($appUserModuleEntity);


                                    } elseif ($requestData[Constant::JSON_APP_USER_MODULE_SCORE] > $appUserModuleEntity->getMaxScore()) {


                                        $appUserModuleEntity->setScore($requestData[Constant::JSON_APP_USER_MODULE_SCORE]);
                                        $appUserModuleEntity->setMaxScore($requestData[Constant::JSON_APP_USER_MODULE_SCORE]);
                                        $this->em->persist($appUserModuleEntity);

                                    } else {
                                        $appUserModuleEntity->setScore($requestData[Constant::JSON_APP_USER_MODULE_SCORE]);
                                        $this->em->persist($appUserModuleEntity);
                                    }

                                    $userStatistics = $appUserEntity->getAppUserStatisticsId();

                                    /* modulo de achievements */

                                    if(is_array($requestData[Constant::JSON_APP_ACHIEVEMENTS_ARRAY]) && $requestData[Constant::JSON_APP_ACHIEVEMENTS_ARRAY] != null){

                                        $achievements = $this->container->get('api.AchievementService')->registerAchievement($requestData[Constant::JSON_APP_ACHIEVEMENTS_ARRAY],
                                            $appUserEntity);

                                    }

                                    /* modulo de achievements fin*/



                                    if($realModuleLevelEntity->getLevelCode() == 9 ){

                                        $newFinalTestSpendTime = $userStatistics->getFinalTestSpendTime() + intval($requestData[Constant::JSON_APP_ELAPSED_TIME]);

                                        $userStatistics->setFinalTestSpendTime($newFinalTestSpendTime);
                                    }

                                    $serverDate = new \DateTime();

                                    $newTotalTime = $userStatistics->getTimePlayed() + intval($requestData[Constant::JSON_APP_ELAPSED_TIME]);
                                    $userStatistics->setLastPracticeAt($serverDate->getTimestamp());
                                    $userStatistics->setLastUpdateDecreaseProgressAt($serverDate->getTimestamp());
                                    $userStatistics->setTimePlayed($newTotalTime);
                                    $this->em->persist($userStatistics);

                                    $this->em->flush();

                                    /* obtener suma del puntaje de todos los minijuegos */
                                    $minigamesTotalScore = $this->container->get('api.RankingService')->getTotalScoreByMiniGames($appUserEntity->getId());
                                    $realvalueminigamesTotalScore = $minigamesTotalScore['total'];

                                    $userStatistics->setMinigamesTotalScore(intval($realvalueminigamesTotalScore));
                                    $this->em->persist($userStatistics);
                                    $this->em->flush();

                                    $this->updateGeneralLevelProgress($levelEntity, $appUserEntity);

                                    $appUserData = $this->container->get('api.AppUserService')->getAppUserData($appUserEntity, false);
                                    $appUserStatistics = $this->container->get('api.AppUserService')->getAppUserStatistics($appUserEntity);
                                    $appUserAchievements = $this->container->get('api.AchievementService')->getAppUserAchievements($appUserEntity);
                                    $availableAppModules = $this->getAvailableAppUserModules($appUserEntity);


                                    $appUserLevelResponse = array(
                                        Constant::JSON_LEVEL_LEVEL_ID => $appUserEntity->getLevelId()->getId(),
                                        Constant::JSON_LEVEL_LEVEL_DESCRIPTION => $appUserEntity->getLevelId()->getLevelDescription(),
                                        Constant::JSON_LEVEL_LEVEL_CODE => $appUserEntity->getLevelId()->getLevelCode(),
                                    );

                                    $jsonResponse = array(
                                        Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                                        Constant::JSON_DATE_SERVER_CURRENT_DATE => $serverDate->getTimestamp(),
                                        Constant::JSON_APP_USER_DATA => $appUserData,
                                        Constant::JSON_APP_USER_STATISTICS => $appUserStatistics,
                                        Constant::JSON_APP_USER_LEVEL => $appUserLevelResponse,
                                        Constant::JSON_APP_USER_ACHIEVEMENTS_ARRAY => $appUserAchievements,
                                        Constant::JSON_APP_USER_MODULE_APP_MODULES_PROGRESS_ARRAY => $availableAppModules,
                                    );
                                } else {
                                    $jsonResponse = array(
                                        Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                                        Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_WRONG_LEVEL_TO_THIS_MODULE
                                    );
                                }



                            } else {
                                $jsonResponse = array(
                                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_UNAUTHORIZED_MODULE
                                );
                            }

                        } else {
                            $jsonResponse = array(
                                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_LEVEL_DOES_NOT_EXIST_OR_NOT_FOUND
                            );
                        }


                    } else {
                        $jsonResponse = array(
                            Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                            Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_PARAMETERS
                        );
                    }

                } else {
                    $jsonResponse = array(
                        Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                        Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_USER_DOES_NOT_EXIST
                    );
                }

            } else {
                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_INVALID_AUTHORIZATION
                );
            }

        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_AUTHORIZATION
            );
        }
        return $jsonResponse;
    }

    public function updateGeneralLevelProgress($level, $user)
    {
        $appModules = $this->repositoryAppModule->findBy(array(Constant::APP_MODULE_APP_MODULE_LEVEL_ID => $level));

        $scoresArray = array();
        foreach ($appModules as $appModule){
            $userAppModule = $this->repositoryAppUserModule->findOneBy(array(Constant::APP_USER_MODULE_APP_USER_ID => $user, Constant::APP_USER_MODULE_APP_MODULE_ID => $appModule));

            if (count($userAppModule) > 0) {
                if ($userAppModule->getAppModuleId()->getAppModuleName() != 'minijuego') {
                    $scoresArray[] = $userAppModule->getMaxScore();
                }
            }
        }

        $newLevel = 0;
        if ($level == $user->getLevelId() && (($scoresArray[0]  >= (Constant::PERCENTAGE_TO_GET_NEXT_LEVEL) * 100) &&
            ($scoresArray[1] >= (Constant::PERCENTAGE_TO_GET_NEXT_LEVEL) * 100)  &&
            ($scoresArray[2] >= (Constant::PERCENTAGE_TO_GET_NEXT_LEVEL) * 100))) {

            $newLevel = $this->updateUserLevel($user);
        }
    }

    public function getAvailableAppUserModules($userId)
    {
        $appUserEntity = $this->repositoryAppUser->find($userId);

        if (count($appUserEntity) > 0) {

            $availableAppModulesArray = $appUserEntity->getAppModulesUser();
            $appUserModulesArray = array();
            foreach ($availableAppModulesArray as $appUserModule){
                $appUserModulesArray[] = array(
                    Constant::JSON_APP_MODULE_APP_MODULE => [
                        Constant::JSON_APP_MODULE_APP_MODULE_ID => $appUserModule->getAppModuleId()->getId(),
                        Constant::JSON_APP_MODULE_APP_MODULE_NAME => $appUserModule->getAppModuleId()->getAppModuleName(),
                        Constant::JSON_LEVEL => [
                            Constant::JSON_LEVEL_LEVEL_ID => $appUserModule->getAppModuleId()->getLevelId()->getId(),
                            Constant::JSON_LEVEL_LEVEL_DESCRIPTION => $appUserModule->getAppModuleId()->getLevelId()->getLevelDescription(),
                            Constant::JSON_LEVEL_LEVEL_CODE => $appUserModule->getAppModuleId()->getLevelId()->getLevelCode(),
                        ]
                    ],
                    Constant::JSON_APP_USER_MODULE_SCORE => $appUserModule->getScore(),
                    Constant::JSON_APP_USER_MODULE_MAX_SCORE => $appUserModule->getMaxScore(),
                );
            }
            $availableAppModules = $appUserModulesArray;


        } else {
            $availableAppModules = null;
        }
        return $availableAppModules;
    }

    public function getAllAppModules()
    {
        $appModules = $this->repositoryAppModule->findBy(array(), array(Constant::APP_MODULE_APP_MODULE_LEVEL_ID => 'ASC'));

        $appModulesArray = array();
        foreach ($appModules as $appModule){
            $appModulesArray[] = array(
                Constant::JSON_APP_MODULE_APP_MODULE_ID => $appModule->getId(),
                Constant::JSON_APP_MODULE_APP_MODULE_NAME => $appModule->getAppModuleName(),
                Constant::JSON_LEVEL => [
                    Constant::JSON_LEVEL_LEVEL_ID => $appModule->getLevelId()->getId(),
                    Constant::JSON_LEVEL_LEVEL_DESCRIPTION => $appModule->getLevelId()->getLevelDescription(),
                    Constant::JSON_LEVEL_LEVEL_CODE => $appModule->getLevelId()->getLevelCode(),
                ]
            );
        }
        return $appModulesArray;
    }

    public function updateUserLevel($userId)
    {
        $appUserEntity = $this->repositoryAppUser->find($userId);

        $actualLevelCode = $appUserEntity->getLevelId()->getLevelCode();

        $newLevel = $this->repositoryLevel->findOneBy(array(Constant::LEVEL_LEVEL_CODE => $actualLevelCode + 1));

        if (count($newLevel) > 0) {

            if ($appUserEntity->getHaveFinalTestAccess() == true || $appUserEntity->getHaveFinalTestAccess() == 1) {
                $appUserEntity->setLevelId($newLevel);
                $this->em->persist($appUserEntity);
                $this->em->flush();

                $appModules = $this->repositoryAppModule->findBy(array(Constant::APP_MODULE_APP_MODULE_LEVEL_ID => $newLevel));
                $appUserModulesArray = array();
                foreach ($appModules as $appModule){
                    $appModules = $this->registerAppUserModules($appUserEntity, $appModule);
                }
                $appUserStatistics = $this->repositoryAppUserStatistics->find($appUserEntity->getAppUserStatisticsId());
                if (count($appUserStatistics) > 0) {
                    $appUserStatistics->setLevelsUnlocked($appUserStatistics->getLevelsUnlocked() + 1);
                    $this->em->persist($appUserStatistics);
                    $this->em->flush();
                }
                return $newLevel->getLevelDescription();//prueba

            } elseif ($appUserEntity->getHaveFinalTestAccess() == false || $appUserEntity->getHaveFinalTestAccess() == 0) {

                if ($newLevel->getLevelCode() < 9) {
                    $appUserEntity->setLevelId($newLevel);
                    $this->em->persist($appUserEntity);
                    $this->em->flush();

                    $appModules = $this->repositoryAppModule->findBy(array(Constant::APP_MODULE_APP_MODULE_LEVEL_ID => $newLevel));
                    $appUserModulesArray = array();
                    foreach ($appModules as $appModule){
                        $appModules = $this->registerAppUserModules($appUserEntity, $appModule);
                    }
                    $appUserStatistics = $this->repositoryAppUserStatistics->find($appUserEntity->getAppUserStatisticsId());
                    if (count($appUserStatistics) > 0) {
                        $appUserStatistics->setLevelsUnlocked($appUserStatistics->getLevelsUnlocked() + 1);
                        $this->em->persist($appUserStatistics);
                        $this->em->flush();
                    }
                    return $newLevel->getLevelDescription();//prueba
                }

            }
        }

    }

    public function updateMultipleProgress(Request $request)
    {
        $requestData = array(
            Constant::PROGRESS_ARRAY => $request->request->get(Constant::PROGRESS_ARRAY)
        );
        $jsonResponse = array();

        $apiAuthorization = $request->headers->get('Authorization');

        if ($apiAuthorization != null ) {

            $jwt = new jsonWebToken();
            $jwt->setToken($apiAuthorization);

            if ($jwt->tokenDecrypt() > 0) {

                $userId = $jwt->tokenDecrypt();
                $appUserEntity = $this->repositoryAppUser->findOneBy(array(Constant::ID => $userId));

                if (count($appUserEntity) > 0) {

                    if (is_array($requestData[Constant::PROGRESS_ARRAY])) {


                        foreach ($requestData[Constant::PROGRESS_ARRAY] as $modulesProgress) {

                            $levelEntity = $this->repositoryLevel->findOneBy(array(Constant::ID =>
                                $modulesProgress[Constant::JSON_APP_MODULE_APP_MODULE][Constant::JSON_LEVEL][Constant::JSON_LEVEL_LEVEL_ID],
                                Constant::LEVEL_LEVEL_CODE => $modulesProgress[Constant::JSON_APP_MODULE_APP_MODULE][Constant::JSON_LEVEL][Constant::JSON_LEVEL_LEVEL_CODE]));

                            if (count($levelEntity) > 0) {

                                $appUserModuleEntity = $this->repositoryAppUserModule->findOneBy(array(Constant::APP_USER_MODULE_APP_MODULE_ID =>
                                    $modulesProgress[Constant::JSON_APP_MODULE_APP_MODULE][Constant::JSON_APP_MODULE_APP_MODULE_ID], Constant::APP_USER_MODULE_APP_USER_ID => $appUserEntity));

                                if (count($appUserModuleEntity) > 0) {

                                    $realModuleLevelEntity = $this->repositoryLevel->findOneBy(array(Constant::ID => $appUserModuleEntity->getAppModuleId()->getLevelId()->getId()));

                                    if($levelEntity == $realModuleLevelEntity){

                                        if ($modulesProgress[Constant::JSON_APP_USER_MODULE_SCORE] < 0) {

                                            $appUserModuleEntity->setScore(0);
                                            $this->em->persist($appUserModuleEntity);


                                        } elseif ($modulesProgress[Constant::JSON_APP_USER_MODULE_SCORE] > $appUserModuleEntity->getMaxScore()) {


                                            $appUserModuleEntity->setScore($modulesProgress[Constant::JSON_APP_USER_MODULE_SCORE]);
                                            $appUserModuleEntity->setMaxScore($modulesProgress[Constant::JSON_APP_USER_MODULE_SCORE]);
                                            $this->em->persist($appUserModuleEntity);

                                        } else {
                                            $appUserModuleEntity->setScore($modulesProgress[Constant::JSON_APP_USER_MODULE_SCORE]);
                                            $this->em->persist($appUserModuleEntity);
                                        }

                                        $userStatistics = $appUserEntity->getAppUserStatisticsId();

                                        /* modulo de achievements */

                                        if(is_array($modulesProgress[Constant::JSON_APP_ACHIEVEMENTS_ARRAY]) &&
                                            $modulesProgress[Constant::JSON_APP_ACHIEVEMENTS_ARRAY] != null){

                                            $achievements = $this->container->get('api.AchievementService')->registerAchievement($modulesProgress[Constant::JSON_APP_ACHIEVEMENTS_ARRAY],
                                                $appUserEntity);

                                        }

                                        /* modulo de achievements fin*/



                                        if($realModuleLevelEntity->getLevelCode() == 9 ){

                                            $newFinalTestSpendTime = $userStatistics->getFinalTestSpendTime() + intval($modulesProgress[Constant::JSON_APP_ELAPSED_TIME]);

                                            $userStatistics->setFinalTestSpendTime($newFinalTestSpendTime);
                                        }

                                        $serverDate = new \DateTime();

                                        $newTotalTime = $userStatistics->getTimePlayed() + intval($modulesProgress[Constant::JSON_APP_ELAPSED_TIME]);
                                        $userStatistics->setLastPracticeAt($serverDate->getTimestamp());
                                        $userStatistics->setLastUpdateDecreaseProgressAt($serverDate->getTimestamp());
                                        $userStatistics->setTimePlayed($newTotalTime);
                                        $this->em->persist($userStatistics);

                                        $this->em->flush();

                                        /* obtener suma del puntaje de todos los minijuegos */
                                        $minigamesTotalScore = $this->container->get('api.RankingService')->getTotalScoreByMiniGames($appUserEntity->getId());
                                        $realvalueminigamesTotalScore = $minigamesTotalScore['total'];

                                        $userStatistics->setMinigamesTotalScore(intval($realvalueminigamesTotalScore));
                                        $this->em->persist($userStatistics);
                                        $this->em->flush();

                                        //$this->updateGeneralLevelProgress($levelEntity, $appUserEntity);

                                        $appUserData = $this->container->get('api.AppUserService')->getAppUserData($appUserEntity, false);
                                        $appUserStatistics = $this->container->get('api.AppUserService')->getAppUserStatistics($appUserEntity);
                                        $appUserAchievements = $this->container->get('api.AchievementService')->getAppUserAchievements($appUserEntity);
                                        $availableAppModules = $this->getAvailableAppUserModules($appUserEntity);


                                        $appUserLevelResponse = array(
                                            Constant::JSON_LEVEL_LEVEL_ID => $appUserEntity->getLevelId()->getId(),
                                            Constant::JSON_LEVEL_LEVEL_DESCRIPTION => $appUserEntity->getLevelId()->getLevelDescription(),
                                            Constant::JSON_LEVEL_LEVEL_CODE => $appUserEntity->getLevelId()->getLevelCode(),
                                        );

                                        $jsonResponse = array(
                                            Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                                            Constant::JSON_DATE_SERVER_CURRENT_DATE => $serverDate->getTimestamp(),
                                            Constant::JSON_APP_USER_DATA => $appUserData,
                                            Constant::JSON_APP_USER_STATISTICS => $appUserStatistics,
                                            Constant::JSON_APP_USER_LEVEL => $appUserLevelResponse,
                                            Constant::JSON_APP_USER_ACHIEVEMENTS_ARRAY => $appUserAchievements,
                                            Constant::JSON_APP_USER_MODULE_APP_MODULES_PROGRESS_ARRAY => $availableAppModules,
                                        );
                                    } else {
                                        $jsonResponse = array(
                                            Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                                            Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_WRONG_LEVEL_TO_THIS_MODULE
                                        );
                                    }



                                } else {
                                    $jsonResponse = array(
                                        Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                                        Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_UNAUTHORIZED_MODULE
                                    );
                                }

                            } else {
                                $jsonResponse = array(
                                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_LEVEL_DOES_NOT_EXIST_OR_NOT_FOUND
                                );
                            }
                        }



                    } else {
                        $jsonResponse = array(
                            Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                            Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_PARAMETERS
                        );
                    }

                } else {
                    $jsonResponse = array(
                        Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                        Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_USER_DOES_NOT_EXIST
                    );
                }

            } else {
                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_INVALID_AUTHORIZATION
                );
            }

        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_AUTHORIZATION
            );
        }
        return $jsonResponse;
    }

    public function insertFinalTestResult(Request $request)
    {
        $jsonResponse = array();
        $apiAuthorization = $request->headers->get('Authorization');
        if ($apiAuthorization != null) {
            $jwt = new jsonWebToken();
            $jwt->setToken($apiAuthorization);
            if ($jwt->tokenDecrypt() > 0) {
                $userId = $jwt->tokenDecrypt();
                $appUserEntity = $this->repositoryAppUser->findOneBy(array(Constant::ID => $userId));
                if (count($appUserEntity) > 0) {

                    $requestData = array(
                        Constant::JSON_LEVEL_LEVELS => $request->request->get(Constant::JSON_LEVEL_LEVELS)
                    );
                    if (is_array($requestData[Constant::JSON_LEVEL_LEVELS]) && count($requestData[Constant::JSON_LEVEL_LEVELS])) {
                        $serverDate = new \DateTime();
                        $finalTestResultEntity = $this->repositoryFinalTestResult->findBy(array(Constant::APP_USER_MODULE_APP_USER_ID => $appUserEntity));
                        if (count($finalTestResultEntity) > 0) {
                            $clearResult = $this->clearFinalTestResult($appUserEntity);
                            if ($clearResult) {
                                $finalTestResults = $this->saveFinalTestResult($requestData[Constant::JSON_LEVEL_LEVELS], $appUserEntity);
                                if (count($finalTestResults) > 0) {
                                    $jsonResponse = array(
                                        Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                                        Constant::JSON_DATE_SERVER_CURRENT_DATE => $serverDate->getTimestamp(),
                                        Constant::JSON_LEVEL_LEVELS => $finalTestResults
                                    );
                                } else {
                                    $jsonResponse = array(
                                        Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                                        Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_ERROR_SAVING
                                    );
                                }
                            }
                        } else {
                            $finalTestResults = $this->saveFinalTestResult($requestData[Constant::JSON_LEVEL_LEVELS], $appUserEntity);
                            if (count($finalTestResults) > 0) {
                                $jsonResponse = array(
                                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                                    Constant::JSON_DATE_SERVER_CURRENT_DATE => $serverDate->getTimestamp(),
                                    Constant::JSON_LEVEL_LEVELS => $finalTestResults
                                );
                            } else {
                                $jsonResponse = array(
                                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_ERROR_SAVING
                                );
                            }
                        }
                    } else {
                        $jsonResponse = array(
                            Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                            Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_PARAMETERS
                        );
                    }
                } else {
                    $jsonResponse = array(
                        Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                        Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_USER_DOES_NOT_EXIST
                    );
                }
            } else {
                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_INVALID_AUTHORIZATION
                );
            }
        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_AUTHORIZATION
            );
        }

        return $jsonResponse;
    }

    public function clearFinalTestResult($appUserEntity)
    {
        $finalTestResults = $this->repositoryFinalTestResult->findBy(array(Constant::APP_USER_MODULE_APP_USER_ID => $appUserEntity));
        if (count($finalTestResults) > 0) {
            foreach ($finalTestResults as $finalTestResult) {
                $this->em->remove($finalTestResult);
                $this->em->flush();
            }
        }
        return true;
    }

    public function saveFinalTestResult($levels, $appUserEntity)
    {
        $response = array();
        foreach ($levels as $level) {
            if (isset($level[Constant::JSON_LEVEL])) {
                $levelEntity = $this->repositoryLevel->find($level[Constant::JSON_LEVEL][Constant::JSON_LEVEL_LEVEL_ID]);
                if (count($levelEntity) > 0) {
                    $finalTestResult = new FinalTestResult();
                    $finalTestResult->setAppUserId($appUserEntity);
                    $finalTestResult->setLevelId($levelEntity);
                    $finalTestResult->setRightAnswers($level[Constant::JSON_FINAL_TEST_RESULT_RIGHT_ANSWERS]);
                    $finalTestResult->setWrongAnswers($level[Constant::JSON_FINAL_TEST_RESULT_WRONG_ANSWERS]);
                    $this->em->persist($finalTestResult);
                    array_push($response, array(
                        Constant::JSON_LEVEL => array(
                            Constant::JSON_LEVEL_LEVEL_ID => $levelEntity->getId(),
                            Constant::JSON_LEVEL_LEVEL_DESCRIPTION => $levelEntity->getLevelDescription(),
                            Constant::JSON_LEVEL_LEVEL_CODE => $levelEntity->getLevelCode()
                        ),
                        Constant::JSON_FINAL_TEST_RESULT_RIGHT_ANSWERS => $finalTestResult->getRightAnswers(),
                        Constant::JSON_FINAL_TEST_RESULT_WRONG_ANSWERS => $finalTestResult->getWrongAnswers()
                    ));
                }
            }
        }
        $this->em->flush();
        return $response;
    }
}