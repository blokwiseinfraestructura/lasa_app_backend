<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 27/01/17
 * Time: 9:28 AM
 */

namespace AppBundle\Service;

use AppBundle\Entity\AdminUser;
use AppBundle\Entity\PushNotification;
use AppBundle\Utility\PushNotificationsService;
use Symfony\Component\ExpressionLanguage\Tests\Node\Obj;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;
use AppBundle\Constant\Constant;

class PushNotificationService
{

    private $repositoryAdminUser;
    private $repositoryPushNotification;
    private $repositoryAppUser;
    private $container;
    private $em;

    public function __construct(EntityManager $entityManager, ContainerInterface $container)
    {
        $this->em = $entityManager;
        $this->container = $container;
        $this->repositoryAdminUser = $this->em->getRepository(Constant::ENTITY_ADMIN_USER);
        $this->repositoryPushNotification = $this->em->getRepository(Constant::ENTITY_PUSH_NOTIFICATION);
        $this->repositoryAppUser = $this->em->getRepository(Constant::ENTITY_APP_USER);
    }

    public function getAllPushNotifications(Request $request)
    {
        $pushNotificationsEntities = $this->repositoryPushNotification->findBy(array(), array(Constant::PUSH_NOTIFICATION_CREATE_AT => 'DESC'));

        if (count($pushNotificationsEntities) > 0) {

            $pushNotificationsArray = array();
            foreach ($pushNotificationsEntities as $notificationsEntity){

                $userAdminData = $this->container->get('api.AdminService')->getUserAdminData($notificationsEntity->getAdminUserId(), 2);
                $pushNotificationData = $this->getSinglePushData($notificationsEntity);
                $pushNotificationsArray[] = array(
                    Constant::JSON_ADMIN_USER_DATA => $userAdminData,
                    Constant::JSON_PUSH_NOTIFICATION_DATA => $pushNotificationData
                );
            }
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                Constant::JSON_PUSH_NOTIFICATIONS_ARRAY => $pushNotificationsArray
            );

        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_NO_PUSH_NOTIFICATIONS_FOUND,
                Constant::SPANISH_MESSAGE_RESPONSE_MESSAGE => Constant::SPANISH_MESSAGE_NO_PUSH_NOTIFICATIONS_FOUND
            );
        }
        return $jsonResponse;
    }

    public function createPushNotification(Request $request)
    {
        $requestData = array(
            Constant::JSON_ADMIN_USER_DATA => $request->request->get(Constant::JSON_ADMIN_USER_DATA),
            Constant::JSON_PUSH_NOTIFICATION_DATA => $request->request->get(Constant::JSON_PUSH_NOTIFICATION_DATA)
        );
        $jsonResponse = array();

        if (is_numeric($requestData[Constant::JSON_ADMIN_USER_DATA][Constant::JSON_ADMIN_USER_ID]) &&
            $requestData[Constant::JSON_PUSH_NOTIFICATION_DATA][Constant::JSON_PUSH_NOTIFICATION_TITLE] &&
            $requestData[Constant::JSON_PUSH_NOTIFICATION_DATA][Constant::PUSH_NOTIFICATION_DESCRIPTION]) {

            $adminUserEntity = $this->repositoryAdminUser->findOneBy(array(Constant::ID => $requestData[Constant::JSON_ADMIN_USER_DATA][Constant::JSON_ADMIN_USER_ID]));

            if (count($adminUserEntity) > 0) {

                $pushNotificationEntity = new PushNotification();

                $pushNotificationEntity->setTitle($requestData[Constant::JSON_PUSH_NOTIFICATION_DATA][Constant::JSON_PUSH_NOTIFICATION_TITLE]);
                $pushNotificationEntity->setDescription($requestData[Constant::JSON_PUSH_NOTIFICATION_DATA][Constant::PUSH_NOTIFICATION_DESCRIPTION]);
                $pushNotificationEntity->setCreateAt(new \DateTime());
                $pushNotificationEntity->setAdminUserId($adminUserEntity);

                $this->em->persist($pushNotificationEntity);
                $this->em->flush();

                $this->sendPushNotificationToAllUsers($requestData[Constant::JSON_PUSH_NOTIFICATION_DATA][Constant::JSON_PUSH_NOTIFICATION_TITLE],
                    $requestData[Constant::JSON_PUSH_NOTIFICATION_DATA][Constant::PUSH_NOTIFICATION_DESCRIPTION]);

                //$userAdminData = $this->container->get('api.AdminService')->getUserAdminData($pushNotificationEntity->getAdminUserId(), 2);
                //$pushNotificationData = $this->getSinglePushData($pushNotificationEntity);
                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                    Constant::JSON_ADMIN_USER_DATA => $this->container->get('api.AdminService')->getUserAdminData($pushNotificationEntity->getAdminUserId(), 2),
                    Constant::JSON_PUSH_NOTIFICATION_DATA => $this->getSinglePushData($pushNotificationEntity)
                );


            } else {
                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_USER_DOES_NOT_EXIST
                );
            }

        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_PARAMETERS
            );
        }
        return $jsonResponse;
    }

    public function getSinglePushData($pushNotificationId)
    {
        $pushNotificationEntity = $this->repositoryPushNotification->find($pushNotificationId);
        //$response = array();

        if (count($pushNotificationEntity) > 0) {
            $response = array(
                Constant::JSON_PUSH_NOTIFICATION_TITLE => $pushNotificationEntity->getTitle(),
                Constant::JSON_PUSH_NOTIFICATION_DESCRIPTION => $pushNotificationEntity->getDescription(),
                Constant::JSON_PUSH_NOTIFICATION_CREATE_AT => $pushNotificationEntity->getCreateAt()->format('Y-m-d H:i:s'),
            );
        } else {
            $response = new Obj();
        }
        return $response;
    }

    public function sendPushNotificationToAllUsers($pushNotificationTitle, $pushNotificationBody)
    {
        $appUsers = $this->repositoryAppUser->findBy(array());

        if(count($appUsers) > 0){
            foreach ($appUsers as $appUser){
                $pushNotifications = new PushNotificationsService($this->container);
                $pushNotifications->setDeviceToken($appUser->getDeviceToken());
                $pushNotifications->setDeviceType($appUser->getDeviceType());
                $pushNotifications->setTitle($pushNotificationTitle);
                $pushNotifications->setBody($pushNotificationBody);
                $pushNotifications->setAchievementCode(0);
                $pushNotifications->sendPushNotification();
            }
        }
    }
}