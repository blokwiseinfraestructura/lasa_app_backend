<?php
/**
 * Created by PhpStorm.
 * User: higom
 * Date: 20/12/2016
 * Time: 10:56 AM
 */

namespace AppBundle\Service;

use AppBundle\Entity\AdminUser;
use AppBundle\Entity\UserAdminModule;
use AppBundle\Utility\jsonWebToken;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;
use AppBundle\Constant\Constant;

class AdminService
{
    private $repositoryAdminModule;
    private $repositoryAdminUser;
    private $repositoryAnswer;
    private $repositoryAppUser;
    private $repositoryLevel;
    private $repositoryQuestion;
    private $repositoryTopic;
    private $repositoryUserAdminModule;
    private $container;
    private $em;

    public function __construct(EntityManager $entityManager, ContainerInterface $container)
    {
        $this->em = $entityManager;
        $this->container = $container;
        $this->repositoryAdminModule = $this->em->getRepository(Constant::ENTITY_ADMIN_MODULE);
        $this->repositoryAdminUser = $this->em->getRepository(Constant::ENTITY_ADMIN_USER);
        $this->repositoryAnswer = $this->em->getRepository(Constant::ENTITY_ANSWER);
        $this->repositoryAppUser = $this->em->getRepository(Constant::ENTITY_APP_USER);
        $this->repositoryLevel = $this->em->getRepository(Constant::ENTITY_LEVEL);
        $this->repositoryQuestion = $this->em->getRepository(Constant::ENTITY_QUESTION);
        $this->repositoryTopic = $this->em->getRepository(Constant::ENTITY_TOPIC);
        $this->repositoryUserAdminModule = $this->em->getRepository(Constant::ENTITY_USER_ADMIN_MODULE);
    }

    public function registerAdminUser(Request $request)
    {
        $requestData = array(
            Constant::JSON_ADMIN_USER_NAME => $request->request->get(Constant::JSON_ADMIN_USER_NAME),
            Constant::JSON_ADMIN_USER_LAST_NAME => $request->request->get(Constant::JSON_ADMIN_USER_LAST_NAME),
            Constant::JSON_ADMIN_USER_EMAIL => $request->request->get(Constant::JSON_ADMIN_USER_EMAIL),
            Constant::JSON_ADMIN_USER_ROLE => $request->request->get(Constant::JSON_ADMIN_USER_ROLE),
            Constant::JSON_ADMIN_USER_PASSWORD => $request->request->get(Constant::JSON_ADMIN_USER_PASSWORD),
            Constant::JSON_ADMIN_MODULE_MODULES_ARRAY => $request->request->get(Constant::JSON_ADMIN_MODULE_MODULES_ARRAY)
        );
        $jsonResponse = array();
        $serverDate = new \DateTime();

        if ($requestData[Constant::JSON_ADMIN_USER_NAME] && $requestData[Constant::JSON_ADMIN_USER_LAST_NAME] &&
            $requestData[Constant::JSON_ADMIN_USER_EMAIL] && $requestData[Constant::JSON_ADMIN_USER_ROLE] && $requestData[Constant::JSON_ADMIN_USER_PASSWORD] &&
            $requestData[Constant::JSON_ADMIN_MODULE_MODULES_ARRAY]) {

            $emailValidator = $this->emailVerification($requestData[Constant::JSON_ADMIN_USER_EMAIL]);

            if ($emailValidator == true) {

                $adminUserEntityValidator = $this->repositoryAdminUser->findOneBy(array(Constant::ADMIN_USER_EMAIL => $requestData[Constant::JSON_ADMIN_USER_EMAIL]));

                if (count($adminUserEntityValidator) == null) {

                    $adminUserEntity = new AdminUser();

                    $adminUserEntity->setName($requestData[Constant::JSON_ADMIN_USER_NAME]);
                    $adminUserEntity->setLastName($requestData[Constant::JSON_ADMIN_USER_LAST_NAME]);
                    $adminUserEntity->setEmail($requestData[Constant::JSON_ADMIN_USER_EMAIL]);
                    $adminUserEntity->setRole($requestData[Constant::JSON_ADMIN_USER_ROLE]);
                    $userPassWord = password_hash($requestData[Constant::JSON_ADMIN_USER_PASSWORD], PASSWORD_DEFAULT);
                    $adminUserEntity->setPassword($userPassWord);
                    $adminUserEntity->setCreateAt($serverDate);
                    $adminUserEntity->setUpdateAt($serverDate);
                    $this->em->persist($adminUserEntity);
                    $this->em->flush();

                    //Enviar Email al usuario
                    $transport = \Swift_SmtpTransport::newInstance()
                        ->setHost('smtp.office365.com')//exchange es el provedor del servicio de correo
                        ->setPort('587')
                        ->setEncryption('tls')
                        ->setUsername('sistemas@lasa.com.co')
                        ->setPassword('@ustr@li@.15*');

                    $mailer = \Swift_Mailer::newInstance($transport);

                    $message = \Swift_Message::newInstance()
                        ->setSubject('Acceso al gestor de contenido de LASA')
                        ->setFrom('noreply@lasa.com.co')
                        ->setTo($adminUserEntity->getEmail())
                        ->setContentType('text/html')
                        ->setBody($this->container->get('templating')->render('default/emailTemplate.html.twig', array('user' => $adminUserEntity->getEmail(), 'password' =>
                            $requestData[Constant::JSON_ADMIN_USER_PASSWORD])), 'text/html');

                    $mailer->send($message);

                    foreach ($requestData[Constant::JSON_ADMIN_MODULE_MODULES_ARRAY] as $module){

                        $adminModuleEntity = $this->repositoryAdminModule->findOneBy(array(Constant::ID => $module[Constant::JSON_ADMIN_MODULE_MODULE_ID]));

                        if (count($adminModuleEntity) > 0) {
                            $useAdminModuleEntity = new UserAdminModule();
                            $useAdminModuleEntity->setAdminUserId($adminUserEntity);
                            $useAdminModuleEntity->setAdminModuleId($adminModuleEntity);
                            $useAdminModuleEntity->setIsAvailable($module[Constant::JSON_USER_ADMIN_MODULE_IS_AVAILABLE]);
                            $this->em->persist($useAdminModuleEntity);
                        }
                    }
                    $this->em->flush();

                    $modulesArray = $this->getUserAdminModules($adminUserEntity);
                    $userAdminData = $this->getUserAdminData($adminUserEntity, 1);
                    $jsonResponse = array(
                        Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                        Constant::JSON_ADMIN_USER_DATA => $userAdminData,
                        Constant::JSON_ADMIN_MODULE_MODULES_ARRAY => $modulesArray
                    );

                } else {
                    $jsonResponse = array(
                        Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                        Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_EMAIL_IS_ALREADY_IN_USE,
                        Constant::SPANISH_MESSAGE_RESPONSE_MESSAGE => Constant::SPANISH_MESSAGE_EMAIL_IS_ALREADY_IN_USE
                    );
                }

            } else {
                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_INVALID_EMAIL_FORMAT
                );
            }

        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_PARAMETERS
            );
        }
        return $jsonResponse;
    }

    public function loginAdminUser(Request $request)
    {
        $requestData = array(
            Constant::JSON_ADMIN_USER_EMAIL => $request->request->get(Constant::JSON_ADMIN_USER_EMAIL),
            Constant::JSON_ADMIN_USER_PASSWORD => $request->request->get(Constant::JSON_ADMIN_USER_PASSWORD)
        );
        $jsonResponse = array();
        $serverDate = new \DateTime();

        if ($requestData[Constant::JSON_ADMIN_USER_EMAIL] && $requestData[Constant::JSON_ADMIN_USER_PASSWORD]) {

            $emailValidator = $this->emailVerification($requestData[Constant::JSON_ADMIN_USER_EMAIL]);
            if ($emailValidator == true) {
                $adminUserEntity = $this->repositoryAdminUser->findOneBy(array(Constant::ADMIN_USER_EMAIL => $requestData[Constant::JSON_ADMIN_USER_EMAIL]));

                if (count($adminUserEntity) > 0) {

                    if (password_verify($requestData[Constant::JSON_ADMIN_USER_PASSWORD], $adminUserEntity->getPassword())) {

                        if ($adminUserEntity->getAdminToken() == null) {

                            $jwt = new jsonWebToken();
                            $jwt->setUserId($adminUserEntity->getId());
                            $jwt->setStoreId($adminUserEntity->getEmail());
                            $jwt->setDate($serverDate->getTimestamp());
                            $token = $jwt->tokenEncrypt();
                            $adminUserEntity->setAdminToken($token);
                            $this->em->persist($adminUserEntity);
                            $this->em->flush();

                            $modulesArray = $this->getUserAdminModules($adminUserEntity);
                            $userAdminData = $this->getUserAdminData($adminUserEntity, 1);
                            $jsonResponse = array(
                                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                                Constant::JSON_ADMIN_USER_DATA => $userAdminData,
                                Constant::JSON_ADMIN_MODULE_MODULES_ARRAY => $modulesArray
                            );

                        } else {

                            $modulesArray = $this->getUserAdminModules($adminUserEntity);
                            $userAdminData = $this->getUserAdminData($adminUserEntity, 1);
                            $jsonResponse = array(
                                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                                Constant::JSON_ADMIN_USER_DATA => $userAdminData,
                                Constant::JSON_ADMIN_MODULE_MODULES_ARRAY => $modulesArray
                            );
                        }

                    } else {
                        $jsonResponse = array(
                            Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                            Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_EMAIL_OR_PASSWORD_DOES_NOT_MATCH
                        );
                    }

                } else {
                    $jsonResponse = array(
                        Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                        Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_EMAIL_OR_PASSWORD_DOES_NOT_MATCH
                    );
                }

            } else {
                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_INVALID_EMAIL_FORMAT
                );
            }

        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_PARAMETERS
            );
        }
        return $jsonResponse;
    }

    public function logoutAdminUser(request $request)
    {
        $severDate = new \DateTime();
        $jsonResponse = array();
        $apiAuthorization = $request->headers->get('Authorization');

        if ($apiAuthorization != null) {
            $jwt = new jsonWebToken();
            $jwt->setToken($apiAuthorization);

            if ($jwt->tokenDecrypt() > 0) {

                $userId = $jwt->tokenDecrypt();
                $userAdmin = $this->repositoryAdminUser->findOneBy(array(Constant::ID => $userId));

                if (count($userAdmin) > 0) {

                        $userAdmin->setAdminToken(null);
                        $this->em->persist($userAdmin);
                        $this->em->flush();

                        $jsonResponse = array(
                            Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                            Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_SUCCESSFUL
                        );

                } else {
                    $jsonResponse = array(
                        Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                        Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_USER_DOES_NOT_EXIST
                    );
                }

            } else {
                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_INVALID_PARAMETERS
                );
            }
        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_INVALID_PARAMETERS
            );
        }
        return $jsonResponse;
    }

    public function getAllAdminUsers(Request $request)
    {
        $jsonResponse = array();
        $adminUsers = $this->repositoryAdminUser->findBy(array(), array(Constant::ID => 'DESC'));

        if (count($adminUsers) > 0) {

            $userAdminsArray = array();
            foreach ($adminUsers as $adminUserEntity) {
                $modulesArray = $this->getUserAdminModules($adminUserEntity);
                $userAdminData = $this->getUserAdminData($adminUserEntity, 2);
                $userAdminsArray[] = array(
                    Constant::JSON_ADMIN_USER_DATA => $userAdminData,
                    Constant::JSON_ADMIN_MODULE_MODULES_ARRAY => $modulesArray
                );

            }
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                Constant::JSON_ADMIN_USERS_ARRAY => $userAdminsArray,
            );

        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_NO_USERS_FOUND
            );
        }
        return $jsonResponse;
    }

    public function editAdminUser(Request $request)
    {
        $requestData = array(
            Constant::JSON_USEFUL_IS_ADMIN_EDIT => $request->request->get(Constant::JSON_USEFUL_IS_ADMIN_EDIT),
            Constant::JSON_ADMIN_USER_DATA => $request->request->get(Constant::JSON_ADMIN_USER_DATA),
            Constant::JSON_ADMIN_MODULE_MODULES_ARRAY => $request->request->get(Constant::JSON_ADMIN_MODULE_MODULES_ARRAY)
        );
        $jsonResponse = array();
        $serverDate = new \DateTime();

        if ($requestData[Constant::JSON_ADMIN_USER_DATA] && is_numeric($requestData[Constant::JSON_ADMIN_USER_DATA][Constant::JSON_USER_ADMIN_MODULE_ADMIN_USER_ID]) &&
        $requestData[Constant::JSON_ADMIN_MODULE_MODULES_ARRAY]) {

            $adminUserEntity = $this->repositoryAdminUser->findOneBy(array(Constant::ID => $requestData[Constant::JSON_ADMIN_USER_DATA][Constant::JSON_ADMIN_USER_ID]));

            if (count($adminUserEntity) > 0) {

                $validEmail = $this->emailVerification($requestData[Constant::JSON_ADMIN_USER_DATA][Constant::JSON_ADMIN_USER_EMAIL]);

                if ($validEmail == true) {

                    if ($requestData[Constant::JSON_USEFUL_IS_ADMIN_EDIT] == true) {
                        $adminUserEntity->setName($requestData[Constant::JSON_ADMIN_USER_DATA][Constant::JSON_ADMIN_USER_NAME]);
                        $adminUserEntity->setLastName($requestData[Constant::JSON_ADMIN_USER_DATA][Constant::JSON_ADMIN_USER_LAST_NAME]);
                        $adminUserEntity->setEmail($requestData[Constant::JSON_ADMIN_USER_DATA][Constant::JSON_ADMIN_USER_EMAIL]);
                        $adminUserEntity->setRole($requestData[Constant::JSON_ADMIN_USER_DATA][Constant::JSON_ADMIN_USER_ROLE]);
                        $adminUserEntity->setUpdateAt($serverDate);
                        $this->em->persist($adminUserEntity);

                    } else {
                        $adminUserEntity->setName($requestData[Constant::JSON_ADMIN_USER_DATA][Constant::JSON_ADMIN_USER_NAME]);
                        $adminUserEntity->setLastName($requestData[Constant::JSON_ADMIN_USER_DATA][Constant::JSON_ADMIN_USER_LAST_NAME]);
                        $userPassWord = password_hash($requestData[Constant::JSON_ADMIN_USER_DATA][Constant::JSON_ADMIN_USER_PASSWORD], PASSWORD_DEFAULT);
                        $adminUserEntity->setPassword($userPassWord);
                        $adminUserEntity->setRole($requestData[Constant::JSON_ADMIN_USER_DATA][Constant::JSON_ADMIN_USER_ROLE]);
                        $adminUserEntity->setUpdateAt($serverDate);
                        $this->em->persist($adminUserEntity);

                    }
                    $this->em->flush();

                    foreach ($requestData[Constant::JSON_ADMIN_MODULE_MODULES_ARRAY] as $adminModuleObject){
                        $this->editUserAdminModule($adminUserEntity, $adminModuleObject);
                    }

                    $modulesArray = $this->getUserAdminModules($adminUserEntity);
                    $userAdminData = $this->getUserAdminData($adminUserEntity, 2);
                    $jsonResponse = array(
                        Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                        Constant::JSON_ADMIN_USER_DATA => $userAdminData,
                        Constant::JSON_ADMIN_MODULE_MODULES_ARRAY => $modulesArray
                    );


                } else {
                    $jsonResponse = array(
                        Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                        Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_INVALID_EMAIL_FORMAT
                    );
                }

            } else {
                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_USER_DOES_NOT_EXIST
                );
            }

        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_PARAMETERS
            );
        }
        return $jsonResponse;
    }

    public function editUserAdminModule($userAdmin, $adminModuleObject)
    {
        $userAdminEntity = $this->repositoryAdminUser->find($userAdmin);
        $adminModuleEntity = $this->repositoryAdminModule->find( $adminModuleObject[Constant::JSON_ADMIN_MODULE_MODULE_ID]);

        $response = null;

        if (count($adminModuleEntity) > 0) {
            $userAdminModuleEntity = $this->repositoryUserAdminModule->findOneBy(array(Constant::USER_ADMIN_MODULE_ADMIN_USER_ID => $userAdminEntity,
                Constant::USER_ADMIN_MODULE_ADMIN_MODULE_ID => $adminModuleEntity));

            if (count($userAdminModuleEntity) > 0) {

                $userAdminModuleEntity->setAdminModuleId($adminModuleEntity);
                $userAdminModuleEntity->setAdminUserId($userAdminEntity);
                $userAdminModuleEntity->setIsAvailable($adminModuleObject[Constant::JSON_USER_ADMIN_MODULE_IS_AVAILABLE]);
                $this->em->persist($userAdminModuleEntity);

            } else {
                $userAdminModuleEntity = new UserAdminModule();

                $userAdminModuleEntity->setAdminModuleId($adminModuleEntity);
                $userAdminModuleEntity->setAdminUserId($userAdminEntity);
                $userAdminModuleEntity->setIsAvailable($adminModuleObject[Constant::JSON_USER_ADMIN_MODULE_IS_AVAILABLE]);
                $this->em->persist($userAdminModuleEntity);
            }
            $this->em->flush();

            $response = $userAdminModuleEntity;
        }
        return $response;
    }

    public function deleteUserAdmin(Request $request)
    {
        $requestData = array(
            Constant::JSON_ADMIN_USER_DATA => $request->request->get(Constant::JSON_ADMIN_USER_DATA),
            Constant::JSON_ADMIN_MODULE_MODULES_ARRAY => $request->request->get(Constant::JSON_ADMIN_MODULE_MODULES_ARRAY)
        );
        $jsonResponse = array();

        if ($requestData[Constant::JSON_ADMIN_USER_DATA][ Constant::JSON_ADMIN_USER_ID]) {

            if (is_numeric($requestData[Constant::JSON_ADMIN_USER_DATA][ Constant::JSON_ADMIN_USER_ID])) {

                $adminUserEntity = $this->repositoryAdminUser->find($requestData[Constant::JSON_ADMIN_USER_DATA][ Constant::JSON_ADMIN_USER_ID]);

                if (count($adminUserEntity) > 0) {

                    $this->em->remove($adminUserEntity);
                    $this->em->flush();

                    $jsonResponse = $jsonResponse = array(
                        Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                        Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_USER_WAS_DELETED_CORRECTLY
                    );

                } else {
                    $jsonResponse = array(
                        Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                        Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_USER_DOES_NOT_EXIST
                    );
                }

            } else {
                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_ADMIN_USER_ID_MUST_BE_A_NUMBER
                );
            }

        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_PARAMETERS
            );
        }
        return $jsonResponse;
    }

    /*
     * Devuelve los datos del admin.
     * la variable @dataRequirement hace referncia a los datos que seran devueltos
     * dataRequirement = 1 si es de login y registro 2 si es para listar la info
     * dataRequirement = 2 si es para listar y editar
     * */
    public function getUserAdminData($userEntity, $dataRequirement)
    {
        $jsonResponse = array();
        $adminUser = $this->repositoryAdminUser->find($userEntity);

        if (count($adminUser) > 0) {

            if ($dataRequirement == 1) {
                $jsonResponse = array(
                    Constant::JSON_ADMIN_USER_ID => $adminUser->getId(),
                    Constant::JSON_ADMIN_USER_NAME => $adminUser->getName(),
                    Constant::JSON_ADMIN_USER_LAST_NAME => $adminUser->getLastName(),
                    Constant::JSON_ADMIN_USER_ROLE => $adminUser->getRole(),
                    Constant::JSON_ADMIN_USER_EMAIL => $adminUser->getEmail(),
                    Constant::JSON_ADMIN_USER_ADMIN_TOKEN => $adminUser->getAdminToken(),
                );
            } elseif ($dataRequirement == 2) {
                $jsonResponse = array(
                    Constant::JSON_ADMIN_USER_ID => $adminUser->getId(),
                    Constant::JSON_ADMIN_USER_NAME => $adminUser->getName(),
                    Constant::JSON_ADMIN_USER_LAST_NAME => $adminUser->getLastName(),
                    Constant::JSON_ADMIN_USER_ROLE => $adminUser->getRole(),
                    Constant::JSON_ADMIN_USER_EMAIL => $adminUser->getEmail(),
                    Constant::JSON_ADMIN_USER_ADMIN_TOKEN => null,
                );
            }

        }
        return $jsonResponse;
    }

    public function getUserAdminModules($userEntity)
    {
        $jsonResponse = array();
        $adminUser = $this->repositoryAdminUser->find($userEntity);
        $userAdminModulesEntities = $this->repositoryUserAdminModule->findBy(array(Constant::USER_ADMIN_MODULE_ADMIN_USER_ID => $adminUser));

        if (count($userAdminModulesEntities) > 0) {
            $adminModulesArray = array();

            foreach ($userAdminModulesEntities as $adminModulesEntity){

                $adminModulesArray[] = array(
                    Constant::JSON_ADMIN_MODULE_MODULE_ID => $adminModulesEntity->getAdminModuleId()->getId(),
                    Constant::JSON_ADMIN_MODULE_MODULE_NAME => $adminModulesEntity->getAdminModuleId()->getModuleName(),
                    Constant::JSON_ADMIN_MODULE_MODULE_CODE => $adminModulesEntity->getAdminModuleId()->getModuleCode(),
                    Constant::JSON_USER_ADMIN_MODULE_IS_AVAILABLE => $adminModulesEntity->getIsAvailable(),
                );
            }
            $jsonResponse = $adminModulesArray;
        }
        return $jsonResponse;
    }

    public function recoveryUserToken(Request $request){
        $jsonResponse = array();
        $requestData = array(
            Constant::JSON_ADMIN_USER_EMAIL => $request->request->get(Constant::JSON_ADMIN_USER_EMAIL)
        );

        $emailValidator = $this->emailVerification($requestData[Constant::JSON_ADMIN_USER_EMAIL]);

        if ($emailValidator == true) {

            $dataUser = $this->repositoryAdminUser->findOneBy(array(Constant::ADMIN_USER_EMAIL => $requestData[Constant::JSON_ADMIN_USER_EMAIL]));

            if(count($dataUser) > 0){
                $token = bin2hex(random_bytes(16));
                $updateDate = new \DateTime();

                $dataUser->setRecoverPasswordToken($token);
                $dataUser->setRecoverPasswordTokenExpiresAt($updateDate->modify('+ 1 day'));
                $dataUser->setUpdateAt(new \DateTime());
                $this->em->persist($dataUser);
                $this->em->flush();

                //Enviar Email al usuario
                $transport = \Swift_SmtpTransport::newInstance()
                    ->setHost('smtp.gmail.com')//gmail es el provedor del servicio de correo
                    ->setPort('587')
                    ->setEncryption('tls')
                    ->setUsername('bwdeveloperserver@gmail.com')
                    ->setPassword('Blokwise10');

                $mailer = \Swift_Mailer::newInstance($transport);

                $message = \Swift_Message::newInstance()
                    ->setSubject('Recuperar contraseña Lasa Capacitación Virtual')
                    ->setFrom('noreply@lasa.com.co')
                    ->setTo($dataUser->getEmail())
                    ->setContentType('text/html')
                    ->setBody($this->container->get('templating')->render('default/emailTemplate.html.twig', array('userToken' => $token,'isAdmin' => true, 'typeEmail' => 1)), 'text/html');

                $mailer->send($message);

                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_EMAIL_SENT_SUCCESSFULY
                );
            }else{
                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_USER_DOES_NOT_EXIST
                );
            }
        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_INVALID_EMAIL_FORMAT
            );
        }

        return $jsonResponse;
    }

    public function updatePasswordFromRecoverMode(Request $request)
    {
        $dataRequest = array(
            Constant::JSON_IS_ADMIN => $request->request->get(Constant::JSON_IS_ADMIN),
            Constant::JSON_RECOVER_TOKEN => $request->request->get(Constant::JSON_RECOVER_TOKEN),
            Constant::JSON_NEW_PASSWORD => $request->request->get(Constant::JSON_NEW_PASSWORD)
        );
        $jsonResponse = array();

        if ($dataRequest[Constant::JSON_RECOVER_TOKEN] && $dataRequest[Constant::JSON_NEW_PASSWORD] && is_bool($dataRequest[Constant::JSON_IS_ADMIN])) {

            $currentDate = new \DateTime();
            $token = bin2hex(random_bytes(16));

            if ($dataRequest[Constant::JSON_IS_ADMIN] == 1 || $dataRequest[Constant::JSON_IS_ADMIN] == true) {

                $userAdminEntity = $this->repositoryAdminUser->findOneBy(array(Constant::ADMIN_USER_RECOVER_PASSWORD_TOKEN => $dataRequest[Constant::JSON_RECOVER_TOKEN]));

                if (count($userAdminEntity) > 0) {

                    if ($currentDate <= $userAdminEntity->getRecoverPasswordTokenExpiresAt()) {


                        $password = password_hash($dataRequest[Constant::JSON_NEW_PASSWORD], PASSWORD_DEFAULT);

                        $userAdminEntity->setRecoverPasswordToken($token);
                        $userAdminEntity->setRecoverPasswordTokenExpiresAt($currentDate->modify('- 1 day'));
                        $userAdminEntity->setPassword($password);
                        $userAdminEntity->setUpdateAt($currentDate);
                        $this->em->persist($userAdminEntity);
                        $this->em->flush();

                        //Enviar Email al usuario
                        $transport = \Swift_SmtpTransport::newInstance()
                            ->setHost('smtp.office365.com')//exchange es el provedor del servicio de correo
                            ->setPort('587')
                            ->setEncryption('tls')
                            ->setUsername('sistemas@lasa.com.co')
                            ->setPassword('@ustr@li@.15*');

                        $mailer = \Swift_Mailer::newInstance($transport);

                        $message = \Swift_Message::newInstance()
                            ->setSubject('Confirmación Cambio de Contraseña')
                            ->setFrom('no-reply@lasa.com.co')
                            ->setTo($userAdminEntity->getEmail())
                            ->setContentType('text/html')
                            ->setBody($this->container->get('templating')->render('default/emailTemplate.html.twig', array('typeEmail' => 2)), 'text/html');

                        $mailer->send($message);

                        $jsonResponse = array(
                            Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                            Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_EMAIL_SENT_SUCCESSFULY,
                        );

                    } else {
                        $jsonResponse = array(
                            Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                            Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_TOKEN_HAS_EXPIRED
                        );
                    }

                } else {
                    $jsonResponse = array(
                        Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                        Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_INVALID_TOKEN
                    );
                }


            } else if ($dataRequest[Constant::JSON_IS_ADMIN] == 0 || $dataRequest[Constant::JSON_IS_ADMIN] == false) {

                $appUserEntity = $this->repositoryAppUser->findOneBy(array(Constant::APP_USER_RECOVER_PASSWORD_TOKEN => $dataRequest[Constant::JSON_RECOVER_TOKEN]));

                if (count($appUserEntity) > 0) {

                    if ($currentDate <= $appUserEntity->getRecoverPasswordTokenExpiresAt()) {

                        $password = password_hash($dataRequest[Constant::JSON_NEW_PASSWORD], PASSWORD_DEFAULT);

                        $appUserEntity->setRecoverPasswordToken($token);
                        $appUserEntity->setRecoverPasswordTokenExpiresAt($currentDate->modify('- 1 day'));
                        $appUserEntity->setPassword($password);
                        $this->em->persist($appUserEntity);
                        $this->em->flush();

                        //Enviar Email al usuario
                        $transport = \Swift_SmtpTransport::newInstance()
                            ->setHost('smtp.office365.com')//exchange es el provedor del servicio de correo
                            ->setPort('587')
                            ->setEncryption('tls')
                            ->setUsername('sistemas@lasa.com.co')
                            ->setPassword('@ustr@li@.15*');

                        $mailer = \Swift_Mailer::newInstance($transport);

                        $message = \Swift_Message::newInstance()
                            ->setSubject('Confirmación Cambio de Contraseña')
                            ->setFrom('no-reply@lasa.com.co')
                            ->setTo($appUserEntity->getEmail())
                            ->setContentType('text/html')
                            ->setBody($this->container->get('templating')->render('default/emailTemplate.html.twig', array('typeEmail' => 2)), 'text/html');

                        $mailer->send($message);

                        $jsonResponse = array(
                            Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                            Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_EMAIL_SENT_SUCCESSFULY,
                        );

                    } else {
                        $jsonResponse = array(
                            Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                            Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_TOKEN_HAS_EXPIRED
                        );
                    }

                } else {
                    $jsonResponse = array(
                        Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                        Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_INVALID_TOKEN
                    );
                }

            } else {
                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_IS_ADMIN_MUST_BE_A_BOOL
                );
            }

        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_PARAMETERS
            );
        }
        return $jsonResponse;
    }

    public function passwordMatcher(Request $request)
    {
        $requestData = array(
            Constant::JSON_ADMIN_USER_ID => $request->request->get(Constant::JSON_ADMIN_USER_ID),
            Constant::JSON_ADMIN_USER_PASSWORD => $request->request->get(Constant::JSON_ADMIN_USER_PASSWORD)
        );
        $jsonResponse = array();

        if ($requestData[Constant::JSON_ADMIN_USER_ID] && $requestData[Constant::JSON_ADMIN_USER_PASSWORD]) {

            $userAdminEntity = $this->repositoryAdminUser->findOneBy(array(Constant::ID => $requestData[Constant::JSON_ADMIN_USER_ID]));

            if (count($userAdminEntity) > 0) {

                if (password_verify($requestData[Constant::JSON_ADMIN_USER_PASSWORD], $userAdminEntity->getPassword())) {

                    $jsonResponse = array(
                        Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                        Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_PASSWORD_MATCH
                    );

                } else {
                    $jsonResponse = array(
                        Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                        Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_PASSWORD_MISMATCH
                    );
                }

            } else {
                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_USER_DOES_NOT_EXIST
                );
            }

        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_PARAMETERS
            );
        }
        return $jsonResponse;
    }

    public function emailVerification($mail)
    {
        $syntax='#^[\w.-]+@[\w.-]+\.[a-zA-Z]{2,6}$#';
        if(preg_match($syntax,$mail)){
            return true;
        } else {
            return false;
        }
    }
}