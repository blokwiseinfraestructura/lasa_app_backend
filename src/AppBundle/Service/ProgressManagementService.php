<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 7/02/17
 * Time: 9:50 AM
 */

namespace AppBundle\Service;

use AppBundle\Entity\AppUser;
use AppBundle\Entity\AppUserModule;
use AppBundle\Entity\AppUserStatistics;
use AppBundle\Utility\jsonWebToken;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;
use AppBundle\Constant\Constant;
use Symfony\Component\Validator\Tests\Fixtures\ConstraintA;

class ProgressManagementService
{
    private $repositoryAdminModule;
    private $repositoryAdminUser;
    private $repositoryAnswer;
    private $repositoryAppConstant;
    private $repositoryAppModule;
    private $repositoryAppUser;
    private $repositoryAppUserModule;
    private $repositoryAppUserStatistics;
    private $repositoryCity;
    private $repositoryLevel;
    private $repositoryQuestion;
    private $repositoryTopic;
    private $repositoryUserAdminModule;
    private $repositoryWorkArea;
    private $container;
    private $em;

    public function __construct(EntityManager $entityManager, ContainerInterface $container)
    {
        $this->em = $entityManager;
        $this->container = $container;
        $this->repositoryAdminModule = $this->em->getRepository(Constant::ENTITY_ADMIN_MODULE);
        $this->repositoryAdminUser = $this->em->getRepository(Constant::ENTITY_ADMIN_USER);
        $this->repositoryAnswer = $this->em->getRepository(Constant::ENTITY_ANSWER);
        $this->repositoryAppConstant = $this->em->getRepository(Constant::ENTITY_APP_CONSTANT);
        $this->repositoryAppModule = $this->em->getRepository(Constant::ENTITY_APP_MODULE);
        $this->repositoryAppUser = $this->em->getRepository(Constant::ENTITY_APP_USER);
        $this->repositoryAppUserModule = $this->em->getRepository(Constant::ENTITY_APP_USER_MODULE);
        $this->repositoryAppUserStatistics = $this->em->getRepository(Constant::ENTITY_APP_USER_STATISTICS);
        $this->repositoryCity = $this->em->getRepository(Constant::ENTITY_CITY);
        $this->repositoryLevel = $this->em->getRepository(Constant::ENTITY_LEVEL);
        $this->repositoryQuestion = $this->em->getRepository(Constant::ENTITY_QUESTION);
        $this->repositoryTopic = $this->em->getRepository(Constant::ENTITY_TOPIC);
        $this->repositoryUserAdminModule = $this->em->getRepository(Constant::ENTITY_USER_ADMIN_MODULE);
        $this->repositoryWorkArea = $this->em->getRepository(Constant::ENTITY_WORK_AREA);
    }

    public function decreaseProgress2($appUserId)
    {
        $appUserEntity = $this->repositoryAppUser->find($appUserId);

        $date = new \DateTime();

        if(count($appUserEntity) > 0){

            $limitToCompare = $this->repositoryAppConstant->findOneBy(array(Constant::APP_CONSTANT_APP_CONSTANT_CODE => 3));
            $appUserModules = $this->repositoryAppUserModule->findBy(array(Constant::APP_USER_MODULE_APP_USER_ID => $appUserEntity));

            foreach ($appUserModules as $appUserModule){

                if((($date->getTimestamp() - $appUserEntity->getAppUserStatisticsId()->getLastUpdateDecreaseProgressAt() ) > $limitToCompare->getAppConstantValue()) && (
                    ($date->getTimestamp() - $appUserEntity->getAppUserStatisticsId()->getLastPracticeAt() ) > $limitToCompare->getAppConstantValue())){

                    if($appUserModule->getAppModuleId()->getLevelId()->getLevelCode() != 9){

                        if ($appUserModule->getScore() - 5 >= 0) {
                            $appUserModule->setScore($appUserModule->getScore() - 5);
                        } else {
                            $appUserModule->setScore(0);
                        }
                        $this->em->persist($appUserModule);
                    }
                }
            }
            $appUserStatisticsEntity = $appUserEntity->getAppUserStatisticsId();

            $appUserStatisticsEntity->setLastUpdateDecreaseProgressAt($date->getTimestamp());
            $this->em->persist($appUserStatisticsEntity);

            $this->em->flush();
        }
    }

    public function decreaseProgress($appUserId)
    {
        $appUserEntity = $this->repositoryAppUser->find($appUserId);

        $date = new \DateTime();

        if(count($appUserEntity) > 0){

            $limitToCompare = $this->repositoryAppConstant->findOneBy(array(Constant::APP_CONSTANT_APP_CONSTANT_CODE => 3));
            $percentForDecreaseProgress = $this->repositoryAppConstant->findOneBy(array(Constant::APP_CONSTANT_APP_CONSTANT_CODE => 4));
            $appUserModules = $this->repositoryAppUserModule->findBy(array(Constant::APP_USER_MODULE_APP_USER_ID => $appUserEntity));

            if(($date->getTimestamp() - $appUserEntity->getAppUserStatisticsId()->getLastUpdateDecreaseProgressAt() ) > $limitToCompare->getAppConstantValue()){

                $datesDifferencePractice = $date->getTimestamp() - $appUserEntity->getAppUserStatisticsId()->getLastPracticeAt();
                $datesDifferenceDecrease = $date->getTimestamp() - $appUserEntity->getAppUserStatisticsId()->getLastUpdateDecreaseProgressAt();

                $totalWeeksToDecreasePoints = intval($datesDifferencePractice / $limitToCompare->getAppConstantValue());
                //$totalWeeksToDecrease = intval($datesDifferenceDecrease / $limitToCompare->getAppConstantValue());

                if(intval($datesDifferenceDecrease / $limitToCompare->getAppConstantValue()) == 1){

                    foreach ($appUserModules as $appUserModule){

                        if(($appUserModule->getAppModuleId()->getLevelId()->getLevelCode() != 9 ) && ($appUserModule->getAppModuleId()->getAppModuleName() != 'minijuego') ){

                            if ($appUserModule->getScore() - ($percentForDecreaseProgress->getAppConstantValue()) >= 0) {
                                $appUserModule->setScore($appUserModule->getScore() - ($percentForDecreaseProgress->getAppConstantValue()));
                            } else {
                                $appUserModule->setScore(0);
                            }

                            $this->em->persist($appUserModule);
                        }

                    }


                } elseif (intval($datesDifferenceDecrease / $limitToCompare->getAppConstantValue()) > 1) {

                    foreach ($appUserModules as $appUserModule){

                        if(($appUserModule->getAppModuleId()->getLevelId()->getLevelCode() != 9 ) && ($appUserModule->getAppModuleId()->getAppModuleName() != 'minijuego') ){

                            if ($appUserModule->getScore() - ($percentForDecreaseProgress->getAppConstantValue() * $totalWeeksToDecreasePoints) >= 0) {
                                $appUserModule->setScore($appUserModule->getScore() - ($percentForDecreaseProgress->getAppConstantValue() * $totalWeeksToDecreasePoints));
                            } else {
                                $appUserModule->setScore(0);
                            }

                            $this->em->persist($appUserModule);
                        }

                    }
                }

                $appUserStatisticsEntity = $appUserEntity->getAppUserStatisticsId();

                $appUserStatisticsEntity->setLastUpdateDecreaseProgressAt($date->getTimestamp());
                $this->em->persist($appUserStatisticsEntity);

                $this->em->flush();
            }

        }
    }

    public function cronJobTaskDecreaseProgress(Request $request)
    {
        $appUserEntities = $this->repositoryAppUser->findBy(array());

        $date = new \DateTime();

        if(count($appUserEntities) > 0){

            $limitToCompare = $this->repositoryAppConstant->findOneBy(array(Constant::APP_CONSTANT_APP_CONSTANT_CODE => 3));
            $percentForDecreaseProgress = $this->repositoryAppConstant->findOneBy(array(Constant::APP_CONSTANT_APP_CONSTANT_CODE => 4));


            foreach ($appUserEntities as $appUserEntity){

                if(($date->getTimestamp() - $appUserEntity->getAppUserStatisticsId()->getLastPracticeAt()) > $limitToCompare->getAppConstantValue()){

                    $appUserModules = $this->repositoryAppUserModule->findBy(array(Constant::APP_USER_MODULE_APP_USER_ID => $appUserEntity));

                    foreach ($appUserModules as $appUserModule){

                        if(($appUserModule->getAppModuleId()->getLevelId()->getLevelCode() != 9 ) && ($appUserModule->getAppModuleId()->getAppModuleName() != 'minijuego') ){

                            if ($appUserModule->getScore() - $percentForDecreaseProgress->getAppConstantValue() >= 0 ) {
                                $appUserModule->setScore($appUserModule->getScore() - $percentForDecreaseProgress->getAppConstantValue());
                            } else {
                                $appUserModule->setScore(0);
                            }

                            $appUserStatisticsEntity = $appUserEntity->getAppUserStatisticsId();

                            $appUserStatisticsEntity->setLastUpdateDecreaseProgressAt($date->getTimestamp());

                            $this->em->persist($appUserModule);
                            $this->em->persist($appUserStatisticsEntity);
                        }
                    }
                    $this->em->flush();
                }
            }

        }

        return  array(
            Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
            Constant::MESSAGE_RESPONSE_MESSAGE => 'execution date: '.$date->format('Y-m-d H:i:s'),
        );
    }

}