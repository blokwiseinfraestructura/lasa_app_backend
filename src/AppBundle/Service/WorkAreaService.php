<?php
/**
 * Created by PhpStorm.
 * User: higom
 * Date: 30/12/2016
 * Time: 10:58 PM
 */

namespace AppBundle\Service;

use AppBundle\Entity\AppUser;
use AppBundle\Entity\AppUserModule;
use AppBundle\Entity\AppUserStatistics;
use AppBundle\Utility\jsonWebToken;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;
use AppBundle\Constant\Constant;

class WorkAreaService
{
    private $repositoryAdminModule;
    private $repositoryAdminUser;
    private $repositoryAnswer;
    private $repositoryAppModule;
    private $repositoryAppUser;
    private $repositoryAppUserStatistics;
    private $repositoryCity;
    private $repositoryLevel;
    private $repositoryQuestion;
    private $repositoryTopic;
    private $repositoryUserAdminModule;
    private $repositoryWorkArea;
    private $container;
    private $em;

    public function __construct(EntityManager $entityManager, ContainerInterface $container)
    {
        $this->em = $entityManager;
        $this->container = $container;
        $this->repositoryAdminModule = $this->em->getRepository(Constant::ENTITY_ADMIN_MODULE);
        $this->repositoryAdminUser = $this->em->getRepository(Constant::ENTITY_ADMIN_USER);
        $this->repositoryAnswer = $this->em->getRepository(Constant::ENTITY_ANSWER);
        $this->repositoryAppModule = $this->em->getRepository(Constant::ENTITY_APP_MODULE);
        $this->repositoryAppUser = $this->em->getRepository(Constant::ENTITY_APP_USER);
        $this->repositoryAppUserStatistics = $this->em->getRepository(Constant::ENTITY_APP_USER_STATISTICS);
        $this->repositoryCity = $this->em->getRepository(Constant::ENTITY_CITY);
        $this->repositoryLevel = $this->em->getRepository(Constant::ENTITY_LEVEL);
        $this->repositoryQuestion = $this->em->getRepository(Constant::ENTITY_QUESTION);
        $this->repositoryTopic = $this->em->getRepository(Constant::ENTITY_TOPIC);
        $this->repositoryUserAdminModule = $this->em->getRepository(Constant::ENTITY_USER_ADMIN_MODULE);
        $this->repositoryWorkArea = $this->em->getRepository(Constant::ENTITY_WORK_AREA);
    }

    public function getAllWorkAreas()
    {
        $workAreas = $this->repositoryWorkArea->findBy(array(), array(Constant::ID => 'ASC'));

        $workAreasArray = array();
        foreach ($workAreas as $workArea){
            $workAreasArray[] = array(
                Constant::JSON_WORK_AREA_ID => $workArea->getId(),
                Constant::JSON_WORK_AREA_WORK_AREA_NAME => $workArea->getWorkAreaName()
            );
        }
        return $workAreasArray;
    }
}