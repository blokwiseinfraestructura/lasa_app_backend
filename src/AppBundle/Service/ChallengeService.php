<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 18/01/17
 * Time: 3:37 PM
 */

namespace AppBundle\Service;

use AppBundle\Entity\AppUserChallenge;
use AppBundle\Entity\AppUserStatistics;
use AppBundle\Entity\Challenge;
use AppBundle\Repository\AppUserRepository;
use AppBundle\Utility\jsonWebToken;
use AppBundle\Utility\PushNotificationsService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;
use AppBundle\Constant\Constant;
use RMS\PushNotificationsBundle\Message\AndroidMessage;
use Symfony\Component\Validator\Constraints\Count;


class ChallengeService
{
    private $repositoryAdminModule;
    private $repositoryAdminUser;
    private $repositoryAnswer;
    private $repositoryAppModule;
    private $repositoryAppUser;
    private $repositoryAppUserChallenge;
    private $repositoryAppUserStatistics;
    private $repositoryChallenge;
    private $repositoryAppUserModule;
    private $repositoryCity;
    private $repositoryLevel;
    private $repositoryQuestion;
    private $repositoryTopic;
    private $repositoryUserAdminModule;
    private $repositoryWorkArea;
    private $container;
    private $em;

    public function __construct(EntityManager $entityManager, ContainerInterface $container)
    {
        $this->em = $entityManager;
        $this->container = $container;
        $this->repositoryAdminModule = $this->em->getRepository(Constant::ENTITY_ADMIN_MODULE);
        $this->repositoryAdminUser = $this->em->getRepository(Constant::ENTITY_ADMIN_USER);
        $this->repositoryAnswer = $this->em->getRepository(Constant::ENTITY_ANSWER);
        $this->repositoryAppModule = $this->em->getRepository(Constant::ENTITY_APP_MODULE);
        $this->repositoryAppUser = $this->em->getRepository(Constant::ENTITY_APP_USER);
        $this->repositoryAppUserChallenge = $this->em->getRepository(Constant::ENTITY_APP_USER_CHALLENGE);
        $this->repositoryAppUserStatistics = $this->em->getRepository(Constant::ENTITY_APP_USER_STATISTICS);
        $this->repositoryChallenge = $this->em->getRepository(Constant::ENTITY_CHALLENGE);
        $this->repositoryAppUserModule = $this->em->getRepository(Constant::ENTITY_APP_USER_MODULE);
        $this->repositoryCity = $this->em->getRepository(Constant::ENTITY_CITY);
        $this->repositoryLevel = $this->em->getRepository(Constant::ENTITY_LEVEL);
        $this->repositoryQuestion = $this->em->getRepository(Constant::ENTITY_QUESTION);
        $this->repositoryTopic = $this->em->getRepository(Constant::ENTITY_TOPIC);
        $this->repositoryUserAdminModule = $this->em->getRepository(Constant::ENTITY_USER_ADMIN_MODULE);
        $this->repositoryWorkArea = $this->em->getRepository(Constant::ENTITY_WORK_AREA);
    }

    /*
     * Metodo en pruebas y esperando definicion final
     * */
    public function createChallenge(Request $request)
    {
        $requestData = array(
            Constant::JSON_APP_USER_MY_SCORE => $request->request->get(Constant::JSON_APP_USER_MY_SCORE),
            Constant::JSON_CHALLENGE_DATA => $request->request->get(Constant::JSON_CHALLENGE_DATA),
        );

        $apiAuthorization = $request->headers->get('Authorization');

        if ($apiAuthorization != null ) {

            $jwt = new jsonWebToken();
            $jwt->setToken($apiAuthorization);

            if ($jwt->tokenDecrypt() > 0) {

                $userId = $jwt->tokenDecrypt();
                $appUserEntity = $this->repositoryAppUser->findOneBy(array(Constant::ID => $userId));

                if (count($appUserEntity) > 0) {


                    $possibleContenders = $this->repositoryAppUser->findBy(array());

                    if(count($possibleContenders) > 0) {

                        $contendersArray = array();
                        foreach ($possibleContenders as $contender){
                            if ($contender->getId() != $appUserEntity->getId()){
                                $available = $this->confirmChallengeAvailment($contender->getId());

                                if($available == true){
                                    $contendersArray[] = array(
                                        $contender
                                    );
                                }
                            }
                        }

                        shuffle($contendersArray);
                        $c = count($contendersArray);
                        if($contendersArray != null){
                            $selected = array_rand($contendersArray, 1);
                            $id = $contendersArray[$selected];
                        } else{
                            $id = 0;
                        }

                        if($id > 0){

                            $serveDate = new \DateTime();

                            $challengeData = $this->registerChallenge();

                            $this->registerAppUserChallenge($challengeData, $appUserEntity, $requestData[Constant::JSON_APP_USER_CHALLENGE_SCORE], true);
                            $this->registerAppUserChallenge($challengeData, $id[0], 0, false);

                            $opponentData = $this->getOpponentData($id[0]);
                            $challengeData = $this->getChallengeData($challengeData);
                            /*$message = new AndroidMessage();
                            $message->setGCM(true);
                            $message->setGCMOptions(
                                array('notification' => array('body' => $appUserEntity->getName().' Te ha retado!',
                                    'title' => 'Titulo',
                                    'icon' => 'myicon',
                                    'sound' => 'default')), array('data' => array('dato' => 'dato1'))
                            ); // SENDS MESSAGE OF FORM
                            $message->setDeviceIdentifier('dS9Sn6eRyvY:APA91bHagrC30gXm_7u35eKUBsOmzTpycR_gBUgRqpNqCZ1LgjXVwxsp0fXZxfV96hi7BIVUsl7iYHWxzU_RhH8T1noGx9HuBiu_RKiccyGXSNP0GyW34OuqEiqQJEJPaB3yunFCsiq9');
                            $this->container->get('rms_push_notifications')->send($message);*/

                            //$avaliable = $this->getActiveChallenges($id);
                            //$pending = $this->pendingChallengesForMe($appUserEntity);

                            $jsonResponse = array(
                                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                                Constant::JSON_DATE_SERVER_CURRENT_DATE => $serveDate->getTimestamp(),
                                Constant::JSON_CHALLENGE_DATA => $challengeData,
                                Constant::JSON_OPPONENT_DATA => $opponentData,
                                /*'idselected' => $id,
                                //'available' => $avaliable,*/
                                'count' => $c,
                                //'pending' => $pending,
                                /*//'array' => $contendersArray,
                                'opponetdata' => $opponentData*/
                            );

                        } else{
                            $jsonResponse = array(
                                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_THERE_ARE_NOT_USERS_TO_CHALLENGE
                            );
                        }

                    } else {
                        $jsonResponse = array(
                            Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                            Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_NO_USERS_FOUND
                        );
                    }

                } else {
                    $jsonResponse = array(
                        Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                        Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_USER_DOES_NOT_EXIST
                    );
                }

            } else {
                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_INVALID_AUTHORIZATION
                );
            }

        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_AUTHORIZATION
            );
        }
        return $jsonResponse;
    }

    public function createChallengeV2(Request $request)
    {
        $requestData = array(
            Constant::JSON_APP_USER_MY_SCORE => $request->request->get(Constant::JSON_APP_USER_MY_SCORE),
            Constant::JSON_CHALLENGE_DATA => $request->request->get(Constant::JSON_CHALLENGE_DATA),
        );

        $apiAuthorization = $request->headers->get('Authorization');

        if ($apiAuthorization != null ) {

            $jwt = new jsonWebToken();
            $jwt->setToken($apiAuthorization);

            if ($jwt->tokenDecrypt() > 0) {

                $userId = $jwt->tokenDecrypt();
                $appUserEntity = $this->repositoryAppUser->findOneBy(array(Constant::ID => $userId));

                if (count($appUserEntity) > 0) {


                    switch ($requestData){

                        case (is_numeric($requestData[Constant::JSON_APP_USER_MY_SCORE]) && ($requestData[Constant::JSON_CHALLENGE_DATA] != null)):


                            if (is_numeric($requestData[Constant::JSON_CHALLENGE_DATA][Constant::JSON_APP_USER_CHALLENGE_ID])) {

                                $appUserChallengeEntity = $this->repositoryAppUserChallenge->findOneBy(array(Constant::APP_USER_CHALLENGE_CHALLENGE_ID =>
                                    $requestData[Constant::JSON_CHALLENGE_DATA][Constant::JSON_APP_USER_CHALLENGE_ID], Constant::APP_USER_CHALLENGE_APP_USER_ID => $appUserEntity));

                                if(count($appUserChallengeEntity) > 0){

                                    if($appUserChallengeEntity->getChallengeId()->getIsFinished() != true){

                                        $serverDate = new \DateTime();

                                        $appUserChallengeEntity->setScore($requestData[Constant::JSON_APP_USER_MY_SCORE]);

                                        $challengeEntity = $appUserChallengeEntity->getChallengeId();

                                        $challengeEntity->setChallengeFinishedAt($serverDate);
                                        $challengeEntity->setIsFinished(true);
                                        $this->em->persist($challengeEntity, $challengeEntity);
                                        $this->em->flush();

                                        $this->setChallengeWinner($challengeEntity);


                                        $pendingChallengesArray = $this->pendingChallengesForMe($appUserEntity);
                                        $challengesHistory = $this->getChallengeHistory($appUserEntity);
                                        $serveDate = new \DateTime();
                                        $pendingChallengesForOther = $this->pendingChallengesForOther($appUserEntity);
                                        $appUserAchievements = $this->container->get('api.AchievementService')->getAppUserAchievements($appUserEntity);
                                        $appUserStatics = $this->container->get('api.AppUserService')->getAppUserStatistics($appUserEntity);

                                        $jsonResponse = array(
                                            Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                                            Constant::JSON_DATE_SERVER_CURRENT_DATE => $serveDate->getTimestamp(),
                                            Constant::JSON_PENDING_CHALLENGES_FOR_ME_ARRAY => ($pendingChallengesArray == null ? array() : $pendingChallengesArray),
                                            Constant::JSON_PENDING_CHALLENGES_FOR_OTHERS_ARRAY => ($pendingChallengesForOther == null ? array() : $pendingChallengesForOther),
                                            Constant::JSON_CHALLENGES_HISTORY_ARRAY => ($challengesHistory == null ? array() : $challengesHistory),
                                            Constant::JSON_APP_USER_STATISTICS => $appUserStatics,
                                            Constant::JSON_APP_USER_ACHIEVEMENTS_ARRAY => $appUserAchievements,
                                        );


                                    } else {
                                        $jsonResponse = array(
                                            Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                                            Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_THE_CHALLENGE_IS_OVER
                                        );
                                    }


                                } else {
                                    $jsonResponse = array(
                                        Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                                        Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_CHALLENGE_DOES_NOT_EXIST
                                    );
                                }

                            } else {
                                $jsonResponse = array(
                                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_PARAMETERS
                                );
                            }

                            break;

                        case (is_numeric($requestData[Constant::JSON_APP_USER_MY_SCORE]) && ($requestData[Constant::JSON_CHALLENGE_DATA] == null)):

                            $possibleContenders = $this->findByNot($appUserEntity->getId());

                            if(count($possibleContenders) > 0) {

                                $usersArrayF1 = $this->usersForChallengeFilterNumberOne($appUserEntity, $possibleContenders);
                                $usersArrayF2 = $this->usersForChallengeFilterNumberTwo($appUserEntity, $usersArrayF1);
                                $usersArrayF3 = $this->usersForChallengeFilterNumberThree($appUserEntity, $usersArrayF2);


                                $selectedOpponent = $this->repositoryAppUser->find($usersArrayF3[0][Constant::JSON_APP_USER_ID]);

                                $challengeData = $this->registerChallenge();
                                $this->registerAppUserChallenge($challengeData, $appUserEntity, $requestData[Constant::JSON_APP_USER_MY_SCORE], true);
                                $this->registerAppUserChallenge($challengeData, $selectedOpponent, 0, false);



                                $pendingChallengesArray = $this->pendingChallengesForMe($appUserEntity);
                                $challengesHistory = $this->getChallengeHistory($appUserEntity);
                                $serveDate = new \DateTime();
                                $pendingChallengesForOther = $this->pendingChallengesForOther($appUserEntity);
                                $appUserAchievements = $this->container->get('api.AchievementService')->getAppUserAchievements($appUserEntity);
                                $appUserStatics = $this->container->get('api.AppUserService')->getAppUserStatistics($appUserEntity);
                                $jsonResponse = array(
                                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                                    Constant::JSON_DATE_SERVER_CURRENT_DATE => $serveDate->getTimestamp(),
                                    Constant::JSON_PENDING_CHALLENGES_FOR_ME_ARRAY => ($pendingChallengesArray == null ? array() : $pendingChallengesArray),
                                    Constant::JSON_PENDING_CHALLENGES_FOR_OTHERS_ARRAY => ($pendingChallengesForOther == null ? array() : $pendingChallengesForOther),
                                    Constant::JSON_CHALLENGES_HISTORY_ARRAY => ($challengesHistory == null ? array() : $challengesHistory),
                                    Constant::JSON_APP_USER_STATISTICS => $appUserStatics,
                                    Constant::JSON_APP_USER_ACHIEVEMENTS_ARRAY => $appUserAchievements,
                                );

                                $pushNotifications = new PushNotificationsService($this->container);
                                $pushNotifications->setDeviceToken($selectedOpponent->getDeviceToken());
                                $pushNotifications->setDeviceType($selectedOpponent->getDeviceType());
                                $pushNotifications->setTitle('tienes un nuevo duelo');
                                $pushNotifications->setBody($appUserEntity->getName().' te ha retado a un duelo');
                                $pushNotifications->setAchievementCode(0);
                                $pushNotifications->sendPushNotification();

                            } else {
                                $jsonResponse = array(
                                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_NO_USERS_FOUND
                                );
                            }

                            break;

                        default:
                            $jsonResponse = array(
                                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_PARAMETERS
                            );
                    }

                } else {
                    $jsonResponse = array(
                        Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                        Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_USER_DOES_NOT_EXIST
                    );
                }

            } else {
                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_INVALID_AUTHORIZATION
                );
            }

        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_AUTHORIZATION
            );
        }
        return $jsonResponse;
    }

    /*
     * Metodo para declinar o eliminar reto
     * */
    public function declineChallenge(Request $request)
    {
        $requestData = array(
            Constant::JSON_CHALLENGE_DATA => $request->request->get(Constant::JSON_CHALLENGE_DATA)
        );

        $apiAuthorization = $request->headers->get('Authorization');

        if ($apiAuthorization != null ) {

            $jwt = new jsonWebToken();
            $jwt->setToken($apiAuthorization);

            if ($jwt->tokenDecrypt() > 0) {

                $userId = $jwt->tokenDecrypt();
                $appUserEntity = $this->repositoryAppUser->findOneBy(array(Constant::ID => $userId));

                if (count($appUserEntity) > 0) {

                    if(is_numeric($requestData[Constant::JSON_CHALLENGE_DATA][Constant::JSON_APP_USER_CHALLENGE_ID]) &&
                        $requestData[Constant::JSON_CHALLENGE_DATA][Constant::JSON_CHALLENGE_IDENTIFIER]){

                        $challengeEntity = $this->repositoryChallenge->findOneBy(array(Constant::ID => $requestData[Constant::JSON_CHALLENGE_DATA][Constant::JSON_APP_USER_CHALLENGE_ID],
                            Constant::CHALLENGE_IDENTIFIER => $requestData[Constant::JSON_CHALLENGE_DATA][Constant::JSON_CHALLENGE_IDENTIFIER]));

                        if (count($challengeEntity) > 0) {

                            if($challengeEntity->getIsFinished() != true){

                                $challengeComplements = $challengeEntity->getChallengesAppUser();

                                foreach ($challengeComplements as $challengeComplement){

                                    if($challengeComplement->getIsChallenger() == true){
                                        $pushNotifications = new PushNotificationsService($this->container);
                                        $pushNotifications->setDeviceToken($challengeComplement->getAppUserId()->getDeviceToken());
                                        $pushNotifications->setDeviceType($challengeComplement->getAppUserId()->getDeviceType());
                                        $pushNotifications->setTitle('Reto Cancelado');
                                        $pushNotifications->setBody('alguien de los que retaste desistió de enfrentarte!');
                                        $pushNotifications->setAchievementCode(0);
                                        $pushNotifications->sendPushNotification();
                                    }
                                }

                                $this->em->remove($challengeEntity);
                                $this->em->flush();

                                $pendingChallengesArray = $this->pendingChallengesForMe($appUserEntity);
                                $challengesHistory = $this->getChallengeHistory($appUserEntity);
                                $serveDate = new \DateTime();
                                $pendingChallengesForOther = $this->pendingChallengesForOther($appUserEntity);
                                $appUserAchievements = $this->container->get('api.AchievementService')->getAppUserAchievements($appUserEntity);
                                $appUserStatics = $this->container->get('api.AppUserService')->getAppUserStatistics($appUserEntity);


                                $jsonResponse = array(
                                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_CHALLENGE_WAS_DELETED_CORRECTLY,
                                    Constant::JSON_DATE_SERVER_CURRENT_DATE => $serveDate->getTimestamp(),
                                    Constant::JSON_PENDING_CHALLENGES_FOR_ME_ARRAY => ($pendingChallengesArray == null ? array() : $pendingChallengesArray),
                                    Constant::JSON_PENDING_CHALLENGES_FOR_OTHERS_ARRAY => ($pendingChallengesForOther == null ? array() : $pendingChallengesForOther),
                                    Constant::JSON_CHALLENGES_HISTORY_ARRAY => ($challengesHistory == null ? array() : $challengesHistory),
                                    Constant::JSON_APP_USER_STATISTICS => $appUserStatics,
                                    Constant::JSON_APP_USER_ACHIEVEMENTS_ARRAY => $appUserAchievements,
                                );

                            } else {
                                $jsonResponse = array(
                                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_CHALLENGE_CANNOT_BE_DELETED
                                );
                            }

                        } else {
                            $jsonResponse = array(
                                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_CHALLENGE_DOES_NOT_EXIST
                            );
                        }

                    } else {
                        $jsonResponse = array(
                            Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                            Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_PARAMETERS
                        );
                    }

                } else {
                    $jsonResponse = array(
                        Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                        Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_USER_DOES_NOT_EXIST
                    );
                }

            } else {
                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_INVALID_AUTHORIZATION
                );
            }

        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_AUTHORIZATION
            );
        }
        return $jsonResponse;

    }

    /*
     * Filtro de oponentes 1
     * */
    public function usersForChallengeFilterNumberOne($appUserEntity, $possibleContenders)
    {
        $totalUser = count($possibleContenders);
        shuffle($possibleContenders);
        $usersArray = $possibleContenders;

        $restricterCounter = 0;
        for ($i = 0; $i < count($possibleContenders); $i++){
            $info = $this->getOpponentData($possibleContenders[$i]);
            if ($info[Constant::JSON_APP_USER_ID] != $appUserEntity->getId()){
                $available = $this->confirmChallengeAvailment($possibleContenders[$i]->getId());

                if($available == false){
                    unset($usersArray[$i]);
                    $restricterCounter++;
                }

                if($restricterCounter >= ($totalUser/2)){
                    break;
                }
            }
        }
        $contendersArray = array();
        foreach ($usersArray as $user){
            $contendersArray[] = $this->getOpponentData($user);
        }
        return $contendersArray;
    }

    /*
     * Filtro de oponentes 2
     * */
    public function usersForChallengeFilterNumberTwo($appUserEntity, $usersArray)
    {
        $restricterCounter = 0;
        $totalUsers  = count($usersArray);

        for ($i = 0; $i < count($usersArray); $i++){

            if ($usersArray[$i][Constant::JSON_APP_USER_IS_LASA_WORKER] != $appUserEntity->getIsLasaWorker()){

                unset($usersArray[$i]);
                $restricterCounter++;

                if($restricterCounter >= ($totalUsers * 0.7)){
                    break;
                }
            }
        }
        $contendersArray = array();
        foreach ($usersArray as $user){
            $contendersArray[] = $this->getOpponentData($user[Constant::JSON_APP_USER_ID]);
        }
        return $contendersArray;
    }

    /*
     * Filtro de oponentes 3
     * */
    public function usersForChallengeFilterNumberThree($appUserEntity, $usersArray)
    {
        $restricterCounter = 0;
        $totalUsers = count($usersArray);

        for ($i = 0; $i < count($usersArray); $i++){

            if($usersArray[$i][Constant::JSON_APP_USER_CITY_ID] != $appUserEntity->getCityId()->getId()){
                unset($usersArray[$i]);
                $restricterCounter++;
            }

            if($restricterCounter >= ($totalUsers * 0.7)){
                break;
            }
        }

        $contendersArray = array();
        foreach ($usersArray as $user){
            $contendersArray[] = $this->getOpponentData($user[Constant::JSON_APP_USER_ID]);
        }
        return $contendersArray;
    }

    /*
     * Metodo para verificar que dsiponibilidad par los retos
     * */
    public function confirmChallengeAvailment($appUser)
    {
        $challenges = $this->repositoryAppUserChallenge->findBy(array(Constant::JSON_APP_USER_CHALLENGE_APP_USER_ID => $appUser), array(Constant::ID => 'DESC'), 5);

        $available = true;
        if (count($challenges) > 0) {

            $counter = 0;
            foreach ($challenges as $challenge){
                $verificatorOne = $challenge->getChallengeId()->getIsFinished();
                if($verificatorOne == false){
                    $counter ++;
                }
            }

            if($counter >= 5){
                $available = false;
            }

        }
        return $available;
    }

    /*
     * esta funcion arma un arreglo con los datos relevantes de los oponentes
     * */
    public function getOpponentData($opponentId)
    {
        $appUserEntity = $this->repositoryAppUser->find($opponentId);

        if (count($appUserEntity) > 0) {
            $request = $this->container->get('request');
            $userImage = ($appUserEntity->getUserImage() == null
                ? null
                : ($request->getScheme() . '://' . $request->getHttpHost().$request->getBasePath().'/'.$appUserEntity->getUserImage()->getWebPath()));

                $jsonResponse = array(
                    Constant::JSON_APP_USER_ID => $appUserEntity->getId(),
                    Constant::JSON_APP_USER_NAME => $appUserEntity->getName(),
                    Constant::JSON_APP_USER_LAST_NAME => $appUserEntity->getLastName(),
                    Constant::JSON_APP_USER_IMAGE => $userImage,
                    Constant::JSON_APP_USER_IS_LASA_WORKER => $appUserEntity->getIsLasaWorker(),
                    Constant::JSON_CITY_ID => $appUserEntity->getCityId()->getId()
                );

        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_USER_DOES_NOT_EXIST
            );
        }
        return $jsonResponse;
    }

    /*
     * Metodo para registrar reto
     * */
    public function registerChallenge()
    {
        $challengeEntity = new Challenge();

        $challengeEntity->setChallengeCreateAt(new \DateTime());
        $challengeEntity->setChallengeIdentifier(md5($challengeEntity->getChallengeCreateAt()->format('Y-m-d-H:i:s')));
        $challengeEntity->setIsFinished(false);
        $this->em->persist($challengeEntity);
        $this->em->flush();

        return $challengeEntity;
    }

    /*
     * Metodo para obtener datos del reto
     * */
    public function getChallengeData($challengeId)
    {
        $challengeEntity = $this->repositoryChallenge->find($challengeId);

        if (count($challengeId) > 0) {
            $jsonResponse = array(
                Constant::JSON_APP_USER_CHALLENGE_ID => $challengeEntity->getId(),
                Constant::JSON_CHALLENGE_IDENTIFIER => $challengeEntity->getChallengeIdentifier(),
                Constant::JSON_CHALLENGE_CHALLENGE_CREATE_AT => $challengeEntity->getChallengeCreateAt()->format('Y-m-d H:i:s'),
                Constant::JSON_CHALLENGE_CHALLENGE_FINISHED_AT => $challengeEntity->getChallengeFinishedAt() == null ? null : $challengeEntity->getChallengeFinishedAt()
                ->format('Y-m-d H:i:s'),
                Constant::JSON_CHALLENGE_IS_FINISHED => $challengeEntity->getIsFinished()
            );
        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_CHALLENGE_DOES_NOT_EXIST
            );
        }
        return $jsonResponse;
    }

    /*
     * Metodo para registra los datos complementarios de reto (tabla intermedia AppUser-Challenge)
     * */
    public function registerAppUserChallenge($challengeId, $appUser, $score, $isChallenger)
    {
            $appUserChallengeEntity = new AppUserChallenge();

            $appUserChallengeEntity->setAppUserId($appUser);
            $appUserChallengeEntity->setChallengeId($challengeId);
            $appUserChallengeEntity->setIsChallenger($isChallenger);
            $appUserChallengeEntity->setScore($score);
            $appUserChallengeEntity->setIsWinner(false);

            $this->em->persist($appUserChallengeEntity);
            $this->em->flush();
    }

    /*
     * Metodo para obtener los retos a los que fue desafiado un usuario
     * */
    public function pendingChallengesForMe($appUserId)
    {
        $appUserEntity = $this->repositoryAppUser->find($appUserId);

        $pendingChallengesForMeArray = null;
        if(count($appUserEntity) > 0){

            $appUserChallengeEntities = $this->repositoryAppUserChallenge->findBy(array(Constant::APP_USER_CHALLENGE_APP_USER_ID => $appUserEntity,
                Constant::APP_USER_CHALLENGE_IS_CHALLENGER => false), array());

            foreach ($appUserChallengeEntities as $appUserChallengeEntity) {

                $challengeData = $this->getChallengeData($appUserChallengeEntity->getChallengeId());

                $challengeUsers = $appUserChallengeEntity->getChallengeId()->getChallengesAppUser();

                $opponentData = null;

                foreach ($challengeUsers as $challengeUser){
                    if ($challengeUser->getAppUserId() != $appUserEntity) {
                        $opponentData = $this->getOpponentDataFromOldChallenge($challengeUser);
                    }
                }

                if ($appUserChallengeEntity->getChallengeId()->getIsFinished() == false) {
                    $pendingChallengesForMeArray[] = array(
                        Constant::JSON_CHALLENGE_DATA => $challengeData,
                        Constant::JSON_OPPONENT_DATA => $opponentData,
                        Constant::JSON_APP_USER_MY_SCORE => $appUserChallengeEntity->getScore()
                    );
                }
            }

        }
        return $pendingChallengesForMeArray;
    }

    /*
     * Metodo para obtener los retos que ha creado un usuario
     * */
    public function pendingChallengesForOther($appUserId)
    {
        $appUserEntity = $this->repositoryAppUser->find($appUserId);

        $pendingChallengesForOtherArray = null;
        if(count($appUserEntity) > 0){

            $appUserChallengeEntities = $this->repositoryAppUserChallenge->findBy(array(Constant::APP_USER_CHALLENGE_APP_USER_ID => $appUserEntity,
                Constant::APP_USER_CHALLENGE_IS_CHALLENGER => true), array());

            foreach ($appUserChallengeEntities as $appUserChallengeEntity) {

                $challengeData = $this->getChallengeData($appUserChallengeEntity->getChallengeId());

                $challengeUsers = $this->findByNotSameUserChallenge($appUserChallengeEntity->getAppUserId(), $appUserChallengeEntity->getChallengeId()->getId());

                foreach ($challengeUsers as $challengeUser){
                        $opponentData = $this->getOpponentDataFromOldChallenge($challengeUser);
                }

                if ($appUserChallengeEntity->getChallengeId()->getIsFinished() == false) {
                    $pendingChallengesForOtherArray[] = array(
                        Constant::JSON_CHALLENGE_DATA => $challengeData,
                        Constant::JSON_OPPONENT_DATA => $opponentData,
                        Constant::JSON_APP_USER_MY_SCORE => $appUserChallengeEntity->getScore()
                    );
                }
            }

        }
        return $pendingChallengesForOtherArray;
    }

    /*
     * Metodo en pruebas y esperando definicion final DEPRCADO
     * */
    public function updateChallengeScore(Request $request)
    {
        $requestData = array(
            Constant::JSON_APP_USER_CHALLENGE_ID => $request->request->get(Constant::JSON_APP_USER_CHALLENGE_ID),
            Constant::JSON_CHALLENGE_IDENTIFIER => $request->request->get(Constant::JSON_CHALLENGE_IDENTIFIER),
            Constant::JSON_CHALLENGE_CHALLENGE_CREATE_AT => $request->request->get(Constant::JSON_CHALLENGE_CHALLENGE_CREATE_AT),
            Constant::JSON_APP_USER_CHALLENGE_SCORE => $request->request->get(Constant::JSON_APP_USER_CHALLENGE_SCORE)
        );

        $apiAuthorization = $request->headers->get('Authorization');

        if ($apiAuthorization != null ) {

            $jwt = new jsonWebToken();
            $jwt->setToken($apiAuthorization);

            if ($jwt->tokenDecrypt() > 0) {

                $userId = $jwt->tokenDecrypt();
                $appUserEntity = $this->repositoryAppUser->findOneBy(array(Constant::ID => $userId));

                if (count($appUserEntity) > 0) {

                    if (is_numeric($requestData[Constant::JSON_APP_USER_CHALLENGE_ID]) && $requestData[Constant::JSON_CHALLENGE_IDENTIFIER] &&
                    $requestData[Constant::JSON_CHALLENGE_CHALLENGE_CREATE_AT] && is_numeric($requestData[Constant::JSON_APP_USER_CHALLENGE_SCORE])) {

                        $appUserChallengeEntity = $this->repositoryAppUserChallenge->findOneBy(array(Constant::APP_USER_CHALLENGE_APP_USER_ID => $appUserEntity,
                            Constant::APP_USER_CHALLENGE_CHALLENGE_ID => $requestData[Constant::JSON_APP_USER_CHALLENGE_CHALLENGE_ID]));

                        if (count($appUserChallengeEntity) > 0) {



                            $serveDate = new \DateTime();
                            $jsonResponse = array(
                                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                                Constant::JSON_DATE_SERVER_CURRENT_DATE => $serveDate->getTimestamp(),
                            );

                        } else {
                            $jsonResponse = array(
                                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_CHALLENGE_DOES_NOT_EXIST
                            );
                        }

                    } else {
                        $jsonResponse = array(
                            Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                            Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_PARAMETERS
                        );
                    }

                } else {
                    $jsonResponse = array(
                        Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                        Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_USER_DOES_NOT_EXIST
                    );
                }

            } else {
                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_INVALID_AUTHORIZATION
                );
            }

        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_AUTHORIZATION
            );
        }
        return $jsonResponse;
    }

    /*
     * Esta función se encagará de ejecutar todos los métodos necesarios para obtener todos los datos del modulo de duelos de un usuario;
     * Duelos pendientes, Historial de duelos, etc.
     * */
    public function getChallengeModule(Request $request)
    {
        $apiAuthorization = $request->headers->get('Authorization');

        if ($apiAuthorization != null ) {

            $jwt = new jsonWebToken();
            $jwt->setToken($apiAuthorization);

            if ($jwt->tokenDecrypt() > 0) {

                $userId = $jwt->tokenDecrypt();
                $appUserEntity = $this->repositoryAppUser->findOneBy(array(Constant::ID => $userId));

                if (count($appUserEntity) > 0) {

                    $pendingChallengesArray = $this->pendingChallengesForMe($appUserEntity);
                    $pendingChallengesForOther = $this->pendingChallengesForOther($appUserEntity);
                    $challengesHistory = $this->getChallengeHistory($appUserEntity);
                    $serveDate = new \DateTime();
                    $jsonResponse = array(
                        Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                        Constant::JSON_DATE_SERVER_CURRENT_DATE => $serveDate->getTimestamp(),
                        Constant::JSON_PENDING_CHALLENGES_FOR_ME_ARRAY => ($pendingChallengesArray == null ? array() : $pendingChallengesArray),
                        Constant::JSON_PENDING_CHALLENGES_FOR_OTHERS_ARRAY => ($pendingChallengesForOther == null ? array() : $pendingChallengesForOther),
                        Constant::JSON_CHALLENGES_HISTORY_ARRAY => ($challengesHistory == null ? array() : $challengesHistory),
                    );

                } else {
                    $jsonResponse = array(
                        Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                        Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_USER_DOES_NOT_EXIST
                    );
                }

            } else {
                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_INVALID_AUTHORIZATION
                );
            }

        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_AUTHORIZATION
            );
        }
        return $jsonResponse;
    }

    /*
     * Esta funcion realiza una consulta de todos los usuarios del app excluyendo a quien consulta
     * */
    public function findByNot($userId){
        $qb = $this->repositoryAppUser->createQueryBuilder('u');
        $qb->where('u.id != :id')
            ->setParameter('id', $userId);

        return $qb->getQuery()
            ->getResult();
    }

    public function findByNotSameUserChallenge($userId, $challengeId){
        $qb = $this->repositoryAppUserChallenge->createQueryBuilder('u')
            //->select('u.userId')
            ->where('u.appUserId != :idUser')
            ->andWhere('u.challengeId = :challengeId')
            ->setParameter('idUser', $userId)
            ->setParameter('challengeId', $challengeId);

        return $qb->getQuery()
            ->getResult();
    }

    /*
     * Metodo para obtener los datos de un usuario para un reto ya creado
     * */
    public function getOpponentDataFromOldChallenge($appUserChallenge)
    {
        $appUserChallengeEntity = $this->repositoryAppUserChallenge->find($appUserChallenge);

        if (count($appUserChallengeEntity) > 0) {
            $request = $this->container->get('request');
            $userImage = ($appUserChallengeEntity->getAppUserId()->getUserImage() == null
                ? null
                : ($request->getScheme() . '://' . $request->getHttpHost().$request->getBasePath().'/'.$appUserChallengeEntity->getAppUserId()->getUserImage()->getWebPath()));

            $jsonResponse = array(
                Constant::JSON_APP_USER_ID => $appUserChallengeEntity->getAppUserId()->getId(),
                Constant::JSON_APP_USER_NAME => $appUserChallengeEntity->getAppUserId()->getName(),
                Constant::JSON_APP_USER_LAST_NAME => $appUserChallengeEntity->getAppUserId()->getLastName(),
                Constant::JSON_APP_USER_IMAGE => $userImage,
                Constant::JSON_APP_USER_CHALLENGE_IS_WINNER => $appUserChallengeEntity->getIsWinner(),
                Constant::JSON_APP_USER_CHALLENGE_IS_CHALLENGER => $appUserChallengeEntity->getIsChallenger(),
                Constant::JSON_LEVEL => [
                    Constant::JSON_LEVEL_LEVEL_ID => $appUserChallengeEntity->getAppUserId()->getLevelId()->getId(),
                    Constant::JSON_LEVEL_LEVEL_DESCRIPTION => $appUserChallengeEntity->getAppUserId()->getLevelId()->getLevelDescription(),
                    Constant::JSON_LEVEL_LEVEL_CODE => $appUserChallengeEntity->getAppUserId()->getLevelId()->getLevelCode(),
                ],
                Constant::APP_USER_CHALLENGE_SCORE => $appUserChallengeEntity->getScore()
            );

            $this->em->flush($appUserChallengeEntity);

        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_USER_DOES_NOT_EXIST
            );
        }
        return $jsonResponse;
    }


    /*
     * Esta funcion se encarga de determinar el ganador del challenge
     * */
    public function setChallengeWinner($challengeId)
    {
        $challengeEntity = $this->repositoryChallenge->find($challengeId);

        if(count($challengeEntity) > 0){

            $challengeComplements = $challengeEntity->getChallengesAppUser();

            $challengeComplementIdArray = array();
            foreach ($challengeComplements as $challengeComplement){
                $challengeComplementIdArray[] = $challengeComplement->getId();
            }

            $challengeComplementOne = $this->repositoryAppUserChallenge->find($challengeComplementIdArray[0]);

            $challengeComplementTwo = $this->repositoryAppUserChallenge->find($challengeComplementIdArray[1]);

            if($challengeComplementOne->getScore() > $challengeComplementTwo->getScore()){

                $challengeComplementOne->setIsWinner(true);
                $this->em->persist($challengeComplementOne);

                $this->updateAppUserStatisticsAfterChallenge($challengeComplementOne->getAppUserId()->getAppUserStatisticsId(), true);
                $this->updateAppUserStatisticsAfterChallenge($challengeComplementTwo->getAppUserId()->getAppUserStatisticsId(), false);
                $this->container->get('api.AchievementService')->winAchievement($challengeComplementOne->getAppUserId());
                $winner = 1;

                $pushNotifications = new PushNotificationsService($this->container);
                $pushNotifications->setDeviceToken($challengeComplementOne->getAppUserId()->getDeviceToken());
                $pushNotifications->setDeviceType($challengeComplementOne->getAppUserId()->getDeviceType());
                $pushNotifications->setTitle('Reto finalizado');
                $pushNotifications->setBody('Has ganado el reto contra '.$challengeComplementTwo->getAppUserId()->getName().' '.$challengeComplementTwo->getAppUserId()->getLastName());
                $pushNotifications->setAchievementCode(0);
                $pushNotifications->sendPushNotification();

                $pushNotifications1 = new PushNotificationsService($this->container);
                $pushNotifications1->setDeviceToken($challengeComplementTwo->getAppUserId()->getDeviceToken());
                $pushNotifications1->setDeviceType($challengeComplementTwo->getAppUserId()->getDeviceType());
                $pushNotifications1->setTitle('Reto finalizado');
                $pushNotifications1->setBody('Has perdido el reto contra '.$challengeComplementOne->getAppUserId()->getName().' '.$challengeComplementOne->getAppUserId()->getLastName());
                $pushNotifications1->setAchievementCode(0);
                $pushNotifications1->sendPushNotification();

            } elseif ($challengeComplementOne->getScore() < $challengeComplementTwo->getScore()) {
                $challengeComplementTwo->setIsWinner(true);
                $this->em->persist($challengeComplementTwo);

                $this->updateAppUserStatisticsAfterChallenge($challengeComplementTwo->getAppUserId()->getAppUserStatisticsId(), true);
                $this->updateAppUserStatisticsAfterChallenge($challengeComplementOne->getAppUserId()->getAppUserStatisticsId(), false);
                $this->container->get('api.AchievementService')->winAchievement($challengeComplementTwo->getAppUserId());
                $winner = 2;

                $pushNotifications = new PushNotificationsService($this->container);
                $pushNotifications->setDeviceToken($challengeComplementOne->getAppUserId()->getDeviceToken());
                $pushNotifications->setDeviceType($challengeComplementOne->getAppUserId()->getDeviceType());
                $pushNotifications->setTitle('Reto finalizado');
                $pushNotifications->setBody('Has ganado el reto contra '.$challengeComplementOne->getAppUserId()->getName().' '.$challengeComplementOne->getAppUserId()->getLastName());
                $pushNotifications->setAchievementCode(0);
                $pushNotifications->sendPushNotification();

                $pushNotifications1 = new PushNotificationsService($this->container);
                $pushNotifications1->setDeviceToken($challengeComplementTwo->getAppUserId()->getDeviceToken());
                $pushNotifications1->setDeviceType($challengeComplementTwo->getAppUserId()->getDeviceType());
                $pushNotifications1->setTitle('Reto finalizado');
                $pushNotifications1->setBody('Has perdido el reto contra '.$challengeComplementTwo->getAppUserId()->getName().' '.$challengeComplementTwo->getAppUserId()->getLastName());
                $pushNotifications1->setAchievementCode(0);
                $pushNotifications1->sendPushNotification();

            } else {

                $this->updateAppUserStatisticsAfterChallenge($challengeComplementTwo->getAppUserId()->getAppUserStatisticsId(), 35);
                $this->updateAppUserStatisticsAfterChallenge($challengeComplementOne->getAppUserId()->getAppUserStatisticsId(), 35);

                $pushNotifications = new PushNotificationsService($this->container);
                $pushNotifications->setDeviceToken($challengeComplementOne->getAppUserId()->getDeviceToken());
                $pushNotifications->setDeviceType($challengeComplementOne->getAppUserId()->getDeviceType());
                $pushNotifications->setTitle('Reto finalizado');
                $pushNotifications->setBody('Has empatado el reto contra '.$challengeComplementOne->getAppUserId()->getName().' '.$challengeComplementOne->getAppUserId()->getLastName());
                $pushNotifications->setAchievementCode(0);
                $pushNotifications->sendPushNotification();

                $pushNotifications1 = new PushNotificationsService($this->container);
                $pushNotifications1->setDeviceToken($challengeComplementTwo->getAppUserId()->getDeviceToken());
                $pushNotifications1->setDeviceType($challengeComplementTwo->getAppUserId()->getDeviceType());
                $pushNotifications1->setTitle('Reto finalizado');
                $pushNotifications1->setBody('Has empatado el reto contra '.$challengeComplementTwo->getAppUserId()->getName().' '.$challengeComplementTwo->getAppUserId()->getLastName());
                $pushNotifications1->setAchievementCode(0);
                $pushNotifications1->sendPushNotification();
            }
            $this->em->flush();

        } else{

            $winner = 0;
        }
        //return $winner;
    }

    /*
     * Metodo para obtener los retos que ha jugado un usuario y que ademas estén finalizados sin importar el resultado
     * */
    public function getChallengeHistory($appUserId)
    {
        $appUserEntity = $this->repositoryAppUser->find($appUserId);

        $challengeHistoryArray = null;
        if(count($appUserEntity) > 0){

            $appUserChallengeEntities = $this->repositoryAppUserChallenge->findBy(array(Constant::APP_USER_CHALLENGE_APP_USER_ID => $appUserEntity), array(Constant::ID => 'DESC'), 20);

            $counter = 0;
            foreach ($appUserChallengeEntities as $appUserChallengeEntity) {

                if($appUserChallengeEntity->getChallengeId()->getIsFinished() == true){

                    $counter ++;

                    $challengeData = $this->getChallengeData($appUserChallengeEntity->getChallengeId());
                    $challengeUsers = $appUserChallengeEntity->getChallengeId()->getChallengesAppUser();

                    $opponentData = null;

                    foreach ($challengeUsers as $challengeUser){
                        if ($challengeUser->getAppUserId() != $appUserEntity) {
                            $opponentData = $this->getOpponentDataFromOldChallenge($challengeUser);
                        }
                    }

                    $challengeHistoryArray[] = array(
                        Constant::JSON_CHALLENGE_DATA => $challengeData,
                        Constant::JSON_OPPONENT_DATA => $opponentData,
                        Constant::JSON_APP_USER_MY_SCORE => $appUserChallengeEntity->getScore()
                    );

                    if($counter == 10){
                        break;
                    }

                }
            }

        }
        return $challengeHistoryArray;
    }

    public function updateAppUserStatisticsAfterChallenge($appUserStatisticsId, $isWinner)
    {
        $appUserStatisticsEntity = $this->repositoryAppUserStatistics->find($appUserStatisticsId);

        if(count($appUserStatisticsEntity) > 0){

            $serverDate = new \DateTime();

            if($isWinner == 1){

                $appUserStatisticsEntity->setChallengesWon($appUserStatisticsEntity->getChallengesWon() + 1);
                $appUserStatisticsEntity->setWinStreak($appUserStatisticsEntity->getWinStreak() +1);
                $appUserStatisticsEntity->setLoseStreak(0);
                $appUserStatisticsEntity->setTotalChallengesPlayed($appUserStatisticsEntity->getTotalChallengesPlayed() + 1);
                $appUserStatisticsEntity->setLastChallengeAt($serverDate->getTimestamp());

            } elseif ($isWinner == 0) {
                $appUserStatisticsEntity->setChallengesLost($appUserStatisticsEntity->getChallengesLost() + 1);
                $appUserStatisticsEntity->setWinStreak(0);
                $appUserStatisticsEntity->setLoseStreak($appUserStatisticsEntity->getLoseStreak() +1);
                $appUserStatisticsEntity->setTotalChallengesPlayed($appUserStatisticsEntity->getTotalChallengesPlayed() + 1);
                $appUserStatisticsEntity->setLastChallengeAt($serverDate->getTimestamp());

            } else {
                $appUserStatisticsEntity->setWinStreak(0);
                $appUserStatisticsEntity->setLoseStreak(0);
                $appUserStatisticsEntity->setTotalChallengesPlayed($appUserStatisticsEntity->getTotalChallengesPlayed() + 1);
                $appUserStatisticsEntity->setLastChallengeAt($serverDate->getTimestamp());
            }

            $this->em->persist($appUserStatisticsEntity);
            $this->em->flush();
        }
    }

}