<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 16/01/17
 * Time: 9:03 AM
 */

namespace AppBundle\Service;

use AppBundle\Entity\News;
use AppBundle\Utility\jsonWebToken;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;
use AppBundle\Constant\Constant;

class NewsService
{
    private $repositoryAdminModule;
    private $repositoryAdminUser;
    private $repositoryAnswer;
    private $repositoryAppModule;
    private $repositoryAppUser;
    private $repositoryAppUserStatistics;
    private $repositoryAppUserModule;
    private $repositoryCity;
    private $repositoryLevel;
    private $repositoryNews;
    private $repositoryQuestion;
    private $repositoryTopic;
    private $repositoryUserAdminModule;
    private $repositoryWorkArea;
    private $container;
    private $em;

    public function __construct(EntityManager $entityManager, ContainerInterface $container)
    {
        $this->em = $entityManager;
        $this->container = $container;
        $this->repositoryAdminModule = $this->em->getRepository(Constant::ENTITY_ADMIN_MODULE);
        $this->repositoryAdminUser = $this->em->getRepository(Constant::ENTITY_ADMIN_USER);
        $this->repositoryAnswer = $this->em->getRepository(Constant::ENTITY_ANSWER);
        $this->repositoryAppModule = $this->em->getRepository(Constant::ENTITY_APP_MODULE);
        $this->repositoryAppUser = $this->em->getRepository(Constant::ENTITY_APP_USER);
        $this->repositoryAppUserStatistics = $this->em->getRepository(Constant::ENTITY_APP_USER_STATISTICS);
        $this->repositoryAppUserModule = $this->em->getRepository(Constant::ENTITY_APP_USER_MODULE);
        $this->repositoryCity = $this->em->getRepository(Constant::ENTITY_CITY);
        $this->repositoryLevel = $this->em->getRepository(Constant::ENTITY_LEVEL);
        $this->repositoryNews = $this->em->getRepository(Constant::ENTITY_NEWS);
        $this->repositoryQuestion = $this->em->getRepository(Constant::ENTITY_QUESTION);
        $this->repositoryTopic = $this->em->getRepository(Constant::ENTITY_TOPIC);
        $this->repositoryUserAdminModule = $this->em->getRepository(Constant::ENTITY_USER_ADMIN_MODULE);
        $this->repositoryWorkArea = $this->em->getRepository(Constant::ENTITY_WORK_AREA);
    }

    public function getAllNews(Request $request)
    {
        $jsonResponse = array();
        $hostName = $_SERVER['HTTP_HOST'];
        $protocol = isset($_SERVER["HTTPS"]) ? 'https' : 'http';
        $allNews = $this->repositoryNews->findBy(array(), array(Constant::JSON_NEWS_CREATE_AT => 'DESC'));

        if (count($allNews) > 0) {
            $newsArray = array();
            foreach ($allNews as $newsEntity){
                $newsArray[] = array(
                    Constant::JSON_NEWS_NEWS_ID => $newsEntity->getId(),
                    Constant::JSON_NEWS_NEWS_TITLE => $newsEntity->getNewsTitle(),
                    Constant::JSON_NEWS_NEWS_DESCRIPTION => $newsEntity->getNewsDescription(),
                    Constant::JSON_NEWS_NEWS_IMAGE_PATH => $protocol.'://'.$hostName.Constant::JSON_USEFUL_DEFAULT_NEWS_IMAGES_DIRECTORY.$newsEntity->getNewsImageName(),
                    Constant::JSON_NEWS_CREATE_AT => $newsEntity->getCreateAt()->format('Y-m-d H:i:s'),
                );
            }
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                Constant::JSON_NEWS_NEWS_ARRAY => $newsArray
            );
        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_NO_NEWS_FOUND,
                Constant::SPANISH_MESSAGE_RESPONSE_MESSAGE => Constant::SPANISH_MESSAGE_NO_NEWS_FOUND
            );
        }
        return $jsonResponse;
    }

    /**
     * @param Request $request
     * @return array
     */
    public function createNews(Request $request)
    {
        $requestData = array(
            Constant::JSON_NEWS_NEWS_TITLE => $request->request->get(Constant::JSON_NEWS_NEWS_TITLE),
            Constant::JSON_NEWS_NEWS_DESCRIPTION => $request->request->get(Constant::JSON_NEWS_NEWS_DESCRIPTION),
            Constant::JSON_NEWS_NEWS_IMAGE_FILE => $request->files->get(Constant::JSON_NEWS_NEWS_IMAGE_FILE)
        );
        $jsonResponse = array();
        $hostName = $_SERVER['HTTP_HOST'];
        $protocol = isset($_SERVER["HTTPS"]) ? 'https' : 'http';

        if ($requestData[Constant::JSON_NEWS_NEWS_TITLE] != null && $requestData[Constant::JSON_NEWS_NEWS_DESCRIPTION] != null
            && $requestData[Constant::JSON_NEWS_NEWS_IMAGE_FILE] != null) {

            $newsEntity = new News();

            $newsEntity->setNewsTitle($requestData[Constant::JSON_NEWS_NEWS_TITLE]);
            $newsEntity->setNewsDescription($requestData[Constant::JSON_NEWS_NEWS_DESCRIPTION]);

            /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $file = $requestData[Constant::JSON_NEWS_NEWS_IMAGE_FILE];

            $fileName = md5(uniqid()) . '.' . $file->guessExtension();

            $file->move(
                $this->container->getParameter('image_news_directory'), $fileName
            );
            $dateNow = new \DateTime();
            $newsEntity->setNewsImageName($fileName);
            $newsEntity->setCreateAt($dateNow);
            $newsEntity->setUpdateAt($dateNow);

            $this->em->persist($newsEntity);
            $this->em->flush();

            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                Constant::JSON_NEWS_NEWS => [
                    Constant::JSON_NEWS_NEWS_ID => $newsEntity->getId(),
                    Constant::JSON_NEWS_NEWS_TITLE => $newsEntity->getNewsTitle(),
                    Constant::JSON_NEWS_NEWS_DESCRIPTION => $newsEntity->getNewsDescription(),
                    Constant::JSON_NEWS_NEWS_IMAGE_PATH => $protocol.'://'.$hostName.Constant::JSON_USEFUL_DEFAULT_NEWS_IMAGES_DIRECTORY.$newsEntity->getNewsImageName(),
                    Constant::JSON_NEWS_CREATE_AT => $newsEntity->getCreateAt()->format('Y-m-d H:i:s')
                ]
            );


        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_PARAMETERS
            );
        }
        return $jsonResponse;
    }

    public function updateNews(Request $request)
    {
        $requestData = array(
            Constant::JSON_NEWS_NEWS_ID => $request->request->get(Constant::JSON_NEWS_NEWS_ID),
            Constant::JSON_NEWS_NEWS_TITLE => $request->request->get(Constant::JSON_NEWS_NEWS_TITLE),
            Constant::JSON_NEWS_NEWS_DESCRIPTION => $request->request->get(Constant::JSON_NEWS_NEWS_DESCRIPTION),
            Constant::JSON_NEWS_NEWS_IMAGE_FILE => $request->files->get(Constant::JSON_NEWS_NEWS_IMAGE_FILE)
        );
        $jsonResponse = array();
        $hostName = $_SERVER['HTTP_HOST'];
        $protocol = isset($_SERVER["HTTPS"]) ? 'https' : 'http';

        if (is_numeric($requestData[Constant::JSON_NEWS_NEWS_ID])) {

            $newsEntity = $this->repositoryNews->findOneBy(array(Constant::ID => $requestData[Constant::JSON_NEWS_NEWS_ID]));

            if (count($newsEntity) > 0) {

                switch ($requestData){

                    case ($requestData[Constant::JSON_NEWS_NEWS_TITLE] != null && $requestData[Constant::JSON_NEWS_NEWS_DESCRIPTION] != null &&
                        $requestData[Constant::JSON_NEWS_NEWS_IMAGE_FILE] != null):

                        $oldFile =  '%kernel.root_dir%/../web/uploads/news'.$newsEntity->getNewsImageName();

                        /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
                        $file = $requestData[Constant::JSON_NEWS_NEWS_IMAGE_FILE];

                        $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                        $file->move(
                            $this->container->getParameter('image_news_directory'), $fileName
                        );
                        $dateNow = new \DateTime();
                        $newsEntity->setNewsTitle($requestData[Constant::JSON_NEWS_NEWS_TITLE]);
                        $newsEntity->setNewsDescription($requestData[Constant::JSON_NEWS_NEWS_DESCRIPTION]);
                        $newsEntity->setNewsImageName($fileName);
                        $newsEntity->setUpdateAt($dateNow);

                        $this->em->persist($newsEntity);
                        $this->em->flush();

                        break;

                    case ($requestData[Constant::JSON_NEWS_NEWS_TITLE] != null && $requestData[Constant::JSON_NEWS_NEWS_DESCRIPTION] != null):

                        $dateNow = new \DateTime();
                        $newsEntity->setNewsTitle($requestData[Constant::JSON_NEWS_NEWS_TITLE]);
                        $newsEntity->setNewsDescription($requestData[Constant::JSON_NEWS_NEWS_DESCRIPTION]);
                        $newsEntity->setUpdateAt($dateNow);

                        $this->em->persist($newsEntity);
                        $this->em->flush();

                        break;

                    case ($requestData[Constant::JSON_NEWS_NEWS_TITLE] != null && $requestData[Constant::JSON_NEWS_NEWS_IMAGE_FILE] != null):

                        $oldFile =  '%kernel.root_dir%/../web/uploads/news'.$newsEntity->getNewsImageName();

                        /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
                        $file = $requestData[Constant::JSON_NEWS_NEWS_IMAGE_FILE];

                        $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                        $file->move(
                            $this->container->getParameter('image_news_directory'), $fileName
                        );
                        $dateNow = new \DateTime();
                        $newsEntity->setNewsTitle($requestData[Constant::JSON_NEWS_NEWS_TITLE]);
                        $newsEntity->setNewsImageName($fileName);
                        $newsEntity->setUpdateAt($dateNow);

                        $this->em->persist($newsEntity);
                        $this->em->flush();

                        break;

                    case ($requestData[Constant::JSON_NEWS_NEWS_DESCRIPTION] != null && $requestData[Constant::JSON_NEWS_NEWS_IMAGE_FILE] != null):

                        $oldFile =  '%kernel.root_dir%/../web/uploads/news'.$newsEntity->getNewsImageName();

                        /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
                        $file = $requestData[Constant::JSON_NEWS_NEWS_IMAGE_FILE];

                        $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                        $file->move(
                            $this->container->getParameter('image_news_directory'), $fileName
                        );
                        $dateNow = new \DateTime();
                        $newsEntity->setNewsDescription($requestData[Constant::JSON_NEWS_NEWS_DESCRIPTION]);
                        $newsEntity->setNewsImageName($fileName);
                        $newsEntity->setUpdateAt($dateNow);

                        $this->em->persist($newsEntity);
                        $this->em->flush();

                        break;

                    case ($requestData[Constant::JSON_NEWS_NEWS_TITLE] != null):

                        $dateNow = new \DateTime();
                        $newsEntity->setNewsTitle($requestData[Constant::JSON_NEWS_NEWS_TITLE]);
                        $newsEntity->setUpdateAt($dateNow);

                        $this->em->persist($newsEntity);
                        $this->em->flush();

                        break;

                    case ($requestData[Constant::JSON_NEWS_NEWS_DESCRIPTION] != null):

                        $dateNow = new \DateTime();
                        $newsEntity->setNewsDescription($requestData[Constant::JSON_NEWS_NEWS_DESCRIPTION]);
                        $newsEntity->setUpdateAt($dateNow);

                        $this->em->persist($newsEntity);
                        $this->em->flush();

                        break;

                    case ($requestData[Constant::JSON_NEWS_NEWS_IMAGE_FILE] != null):

                        $oldFile =  '%kernel.root_dir%/../web/uploads/news'.$newsEntity->getNewsImageName();

                        /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
                        $file = $requestData[Constant::JSON_NEWS_NEWS_IMAGE_FILE];

                        $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                        $file->move(
                            $this->container->getParameter('image_news_directory'), $fileName
                        );
                        $dateNow = new \DateTime();
                        $newsEntity->setNewsImageName($fileName);
                        $newsEntity->setUpdateAt($dateNow);

                        $this->em->persist($newsEntity);
                        $this->em->flush();

                        break;
                }

                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                    Constant::JSON_NEWS_NEWS => [
                        Constant::JSON_NEWS_NEWS_ID => $newsEntity->getId(),
                        Constant::JSON_NEWS_NEWS_TITLE => $newsEntity->getNewsTitle(),
                        Constant::JSON_NEWS_NEWS_DESCRIPTION => $newsEntity->getNewsDescription(),
                        Constant::JSON_NEWS_NEWS_IMAGE_PATH => $protocol.'://'.$hostName.Constant::JSON_USEFUL_DEFAULT_NEWS_IMAGES_DIRECTORY.$newsEntity->getNewsImageName(),
                        Constant::JSON_NEWS_CREATE_AT => $newsEntity->getCreateAt()->format('Y-m-d H:i:s')
                    ]
                );

            } else {
                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_REQUESTED_NEWS_NOT_FOUND
                );
            }

        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_PARAMETERS
            );
        }
        return $jsonResponse;
    }

    public function deleteNews(Request $request)
    {
        $requestData = array(
            Constant::JSON_NEWS_NEWS => $request->request->get(Constant::JSON_NEWS_NEWS),
        );
        $jsonResponse = array();

        if (is_numeric($requestData[Constant::JSON_NEWS_NEWS][Constant::JSON_NEWS_NEWS_ID])) {

            $newsEntity = $this->repositoryNews->findOneBy(array(Constant::ID => $requestData[Constant::JSON_NEWS_NEWS][Constant::JSON_NEWS_NEWS_ID]));

            if (count($newsEntity) > 0) {

                $this->em->remove($newsEntity);
                $this->em->flush();

                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_NEWS_WAS_DELETED_CORRECTLY
                );

            } else {
                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_REQUESTED_NEWS_NOT_FOUND
                );
            }

        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_PARAMETERS
            );
        }
        return $jsonResponse;
    }

    public function getAllNewsForApp()
    {
        $jsonResponse = array();
        $hostName = $_SERVER['HTTP_HOST'];
        $protocol = isset($_SERVER["HTTPS"]) ? 'https' : 'http';
        $allNews = $this->repositoryNews->findBy(array(), array(Constant::JSON_NEWS_CREATE_AT => 'DESC'), 5);

        if (count($allNews) > 0) {
            $newsArray = array();
            foreach ($allNews as $newsEntity){
                $newsArray[] = array(
                    Constant::JSON_NEWS_NEWS_TITLE => $newsEntity->getNewsTitle(),
                    Constant::JSON_NEWS_NEWS_DESCRIPTION => $newsEntity->getNewsDescription(),
                    Constant::JSON_NEWS_NEWS_IMAGE_PATH => $protocol.'://'.$hostName.Constant::JSON_USEFUL_DEFAULT_NEWS_IMAGES_DIRECTORY.$newsEntity->getNewsImageName(),
                    Constant::JSON_NEWS_CREATE_AT => $newsEntity->getCreateAt()->format('Y-m-d H:i:s'),
                );
            }
            $jsonResponse =  $newsArray;

        }

        return $jsonResponse;
    }
}