<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Service;

use AppBundle\Entity\AppUserAchievement;
use AppBundle\Utility\jsonWebToken;
use AppBundle\Utility\PushNotificationsService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;
use AppBundle\Constant\Constant;
use Symfony\Component\Validator\Tests\Fixtures\ConstraintA;

class AchievementService {

    private $repositoryAdminModule;
    private $repositoryAdminUser;
    private $repositoryAnswer;
    private $repositoryAppModule;
    private $repositoryAppUser;
    private $repositoryAppUserStatistics;
    private $repositoryAppUserModule;
    private $repositoryCity;
    private $repositoryLevel;
    private $repositoryQuestion;
    private $repositoryTopic;
    private $repositoryUserAdminModule;
    private $repositoryWorkArea;
    private $repositoryUserImage;
    private $repositoryAppUserAchievement;
    private $repositoryAchievement;
    private $container;
    private $em;

    public function __construct(EntityManager $entityManager, ContainerInterface $container) {
        $this->em = $entityManager;
        $this->container = $container;
        $this->repositoryAdminModule = $this->em->getRepository(Constant::ENTITY_ADMIN_MODULE);
        $this->repositoryAdminUser = $this->em->getRepository(Constant::ENTITY_ADMIN_USER);
        $this->repositoryAnswer = $this->em->getRepository(Constant::ENTITY_ANSWER);
        $this->repositoryAppModule = $this->em->getRepository(Constant::ENTITY_APP_MODULE);
        $this->repositoryAppUser = $this->em->getRepository(Constant::ENTITY_APP_USER);
        $this->repositoryAppUserStatistics = $this->em->getRepository(Constant::ENTITY_APP_USER_STATISTICS);
        $this->repositoryAppUserModule = $this->em->getRepository(Constant::ENTITY_APP_USER_MODULE);
        $this->repositoryCity = $this->em->getRepository(Constant::ENTITY_CITY);
        $this->repositoryLevel = $this->em->getRepository(Constant::ENTITY_LEVEL);
        $this->repositoryQuestion = $this->em->getRepository(Constant::ENTITY_QUESTION);
        $this->repositoryTopic = $this->em->getRepository(Constant::ENTITY_TOPIC);
        $this->repositoryUserAdminModule = $this->em->getRepository(Constant::ENTITY_USER_ADMIN_MODULE);
        $this->repositoryWorkArea = $this->em->getRepository(Constant::ENTITY_WORK_AREA);
        $this->repositoryUserImage = $this->em->getRepository(Constant::ENTITY_USER_IMAGE);
        $this->repositoryAchievement = $this->em->getRepository(Constant::ENTITY_ACHIEVEMENT);
        $this->repositoryAppUserAchievement = $this->em->getRepository(Constant::ENTITY_USER_ACHIEVEMENT);
    }

    public function registerAchievement($achievements, $user) {
        $response = array();
        $userData = $this->repositoryAppUser->find($user);
        if (count($userData) > 0) {
            foreach ($achievements as $achievement) {
                $dataAchievement = $this->repositoryAchievement->find($achievement[Constant::JSON_APP_USER_ACHIEVEMENT_ID]);
                if (count($dataAchievement) > 0) {
                    $dataAppUserAchievement = $this->repositoryAppUserAchievement->findOneBy(array(Constant::ACHIEVEMENT_NAME_ID => $dataAchievement->getId(), Constant::JSON_APP_USER_MODULE_APP_USER_ID => $userData->getId()));
                    if (!count($dataAppUserAchievement) > 0) {
                        $appUserAchievement = new AppUserAchievement();
                        $appUserAchievement->setAppUserId($userData);
                        $appUserAchievement->setAchievementId($dataAchievement);
                        $this->em->persist($appUserAchievement);
                    }
                } else {
                    $response = array();
                }
            }
            $this->em->flush();
            $this->updateAchievements($userData);
            foreach ($userData->getAppUserAchievements() as $achievement) {
                array_push($response, array(
                    Constant::JSON_APP_ACHIEVEMENT_NAME_ID => $achievement->getAchievementId()->getId(),
                    Constant::JSON_APP_ACHIEVEMENT_NAME => $achievement->getAchievementId()->getAchievementName(),
                    Constant::JSON_APP_ACHIEVEMENT_CODE => $achievement->getAchievementId()->getAchievementCode(),
                    Constant::JSON_APP_ACHIEVEMENT_DETAIL => $achievement->getAchievementId()->getAchievementDetail(),
                ));
            }
        } else {
            $response = array();
        }
        return $response;
    }

    public function winAchievement($user) {
        $response = array();
        $dataUser = $this->repositoryAppUser->find($user);
        if (count($dataUser) > 0) {
            switch ($dataUser->getAppUserStatisticsId()->getChallengesWon()) {
                case Constant::NUMBER_CONDITION_ACHIEVEMENT_21:
                    $dataAchievement = $this->repositoryAchievement->findOneBy(array(Constant::ACHIEVEMENT_CODE => Constant::ACHIEVEMENT_CODE_21));
                    if (count($dataAchievement) > 0) {
                        $dataAppUserAchievement = $this->repositoryAppUserAchievement->findOneBy(array(
                            Constant::ACHIEVEMENT_NAME_ID => $dataAchievement->getId(),
                            Constant::JSON_APP_USER_MODULE_APP_USER_ID => $dataUser->getId()));
                        if (!count($dataAppUserAchievement) > 0) {
                            $appUserAchievement = new AppUserAchievement();
                            $appUserAchievement->setAchievementId($dataAchievement);
                            $appUserAchievement->setAppUserId($dataUser);
                            $this->em->persist($appUserAchievement);

                            $pushNotifications = new PushNotificationsService($this->container);
                            $pushNotifications->setDeviceToken($dataUser->getDeviceToken());
                            $pushNotifications->setDeviceType($dataUser->getDeviceType());
                            $pushNotifications->setTitle($dataAchievement->getAchievementName());
                            $pushNotifications->setBody($dataAchievement->getAchievementDetail());
                            $pushNotifications->setAchievementCode($dataAchievement->getAchievementCode());
                            $pushNotifications->sendPushNotification();

                            $this->em->flush();

                            $this->updateAchievements($dataUser);

                            $response = array(
                                Constant::JSON_APP_ACHIEVEMENT_NAME_ID => $dataAchievement->getId(),
                                Constant::JSON_APP_ACHIEVEMENT_NAME => $dataAchievement->getAchievementName(),
                                Constant::JSON_APP_ACHIEVEMENT_CODE => $dataAchievement->getAchievementCode(),
                                Constant::JSON_APP_ACHIEVEMENT_DETAIL => $dataAchievement->getAchievementDetail(),
                            );
                        } else {
                            $response = array();
                        }
                    } else {
                        $response = array();
                    }
                    break;
                case Constant::NUMBER_CONDITION_ACHIEVEMENT_22:
                    $dataAchievement = $this->repositoryAchievement->findOneBy(array(Constant::ACHIEVEMENT_CODE => Constant::ACHIEVEMENT_CODE_22));
                    if (count($dataAchievement) > 0) {
                        $dataAppUserAchievement = $this->repositoryAppUserAchievement->findOneBy(array(
                            Constant::ACHIEVEMENT_NAME_ID => $dataAchievement->getId(),
                            Constant::JSON_APP_USER_MODULE_APP_USER_ID => $dataUser->getId()));
                        if (!count($dataAppUserAchievement) > 0) {
                            $appUserAchievement = new AppUserAchievement();
                            $appUserAchievement->setAchievementId($dataAchievement);
                            $appUserAchievement->setAppUserId($dataUser);
                            $this->em->persist($appUserAchievement);

                            $pushNotifications = new PushNotificationsService($this->container);
                            $pushNotifications->setDeviceToken($dataUser->getDeviceToken());
                            $pushNotifications->setDeviceType($dataUser->getDeviceType());
                            $pushNotifications->setTitle($dataAchievement->getAchievementName());
                            $pushNotifications->setBody($dataAchievement->getAchievementDetail());
                            $pushNotifications->setAchievementCode($dataAchievement->getAchievementCode());
                            $pushNotifications->sendPushNotification();

                            $this->em->flush();
                            
                            $this->updateAchievements($dataUser);

                            $response = array(
                                Constant::JSON_APP_ACHIEVEMENT_NAME_ID => $dataAchievement->getId(),
                                Constant::JSON_APP_ACHIEVEMENT_NAME => $dataAchievement->getAchievementName(),
                                Constant::JSON_APP_ACHIEVEMENT_CODE => $dataAchievement->getAchievementCode(),
                                Constant::JSON_APP_ACHIEVEMENT_DETAIL => $dataAchievement->getAchievementDetail(),
                            );
                        } else {
                            $response = array();
                        }
                    } else {
                        $response = array();
                    }
                    break;
                case Constant::NUMBER_CONDITION_ACHIEVEMENT_23:
                    $dataAchievement = $this->repositoryAchievement->findOneBy(array(Constant::ACHIEVEMENT_CODE => Constant::ACHIEVEMENT_CODE_23));
                    if (count($dataAchievement) > 0) {
                        $dataAppUserAchievement = $this->repositoryAppUserAchievement->findOneBy(array(
                            Constant::ACHIEVEMENT_NAME_ID => $dataAchievement->getId(),
                            Constant::JSON_APP_USER_MODULE_APP_USER_ID => $dataUser->getId()));
                        if (!count($dataAppUserAchievement) > 0) {
                            $appUserAchievement = new AppUserAchievement();
                            $appUserAchievement->setAchievementId($dataAchievement);
                            $appUserAchievement->setAppUserId($dataUser);
                            $this->em->persist($appUserAchievement);

                            $pushNotifications = new PushNotificationsService($this->container);
                            $pushNotifications->setDeviceToken($dataUser->getDeviceToken());
                            $pushNotifications->setDeviceType($dataUser->getDeviceType());
                            $pushNotifications->setTitle($dataAchievement->getAchievementName());
                            $pushNotifications->setBody($dataAchievement->getAchievementDetail());
                            $pushNotifications->setAchievementCode($dataAchievement->getAchievementCode());
                            $pushNotifications->sendPushNotification();

                            $this->em->flush();
                            
                            $this->updateAchievements($dataUser);

                            $response = array(
                                Constant::JSON_APP_ACHIEVEMENT_NAME_ID => $dataAchievement->getId(),
                                Constant::JSON_APP_ACHIEVEMENT_NAME => $dataAchievement->getAchievementName(),
                                Constant::JSON_APP_ACHIEVEMENT_CODE => $dataAchievement->getAchievementCode(),
                                Constant::JSON_APP_ACHIEVEMENT_DETAIL => $dataAchievement->getAchievementDetail(),
                            );
                        } else {
                            $response = array();
                        }
                    } else {
                        $response = array();
                    }
                    break;
            }
        } else {
            $response = array();
        }
        return $response;
    }

    public function getAllAchievements() {
        $achievements = $this->repositoryAchievement->findBy(array(), array(Constant::ACHIEVEMENT_CODE => 'ASC'));

        $achievementsArray = array();
        if (count($achievements) > 0) {
            foreach ($achievements as $achievement) {
                $achievementsArray[] = array(
                    Constant::JSON_APP_ACHIEVEMENT_NAME_ID => $achievement->getId(),
                    Constant::JSON_APP_ACHIEVEMENT_NAME => $achievement->getAchievementName(),
                    Constant::JSON_APP_ACHIEVEMENT_CODE => $achievement->getAchievementCode(),
                    Constant::JSON_APP_ACHIEVEMENT_DETAIL => $achievement->getAchievementDetail(),
                );
            }
        }

        return $achievementsArray;
    }


    public function getAppUserAchievements($userId)
    {
        $appUserEntity = $this->repositoryAppUser->find($userId);

        $appUserAchievements = $this->repositoryAppUserAchievement->findBy(array(Constant::JSON_APP_USER_ACHIEVEMENT_APP_USER_ID => $appUserEntity));

        $appUserAchievementsArray = array();
        if (count($appUserAchievements) > 0) {

            foreach ($appUserAchievements as $appUserAchievement) {
                $appUserAchievementsArray[] = array(
                    Constant::JSON_APP_ACHIEVEMENT_NAME_ID => $appUserAchievement->getAchievementId()->getId(),
                    Constant::JSON_APP_ACHIEVEMENT_NAME => $appUserAchievement->getAchievementId()->getAchievementName(),
                    Constant::JSON_APP_ACHIEVEMENT_CODE => $appUserAchievement->getAchievementId()->getAchievementCode(),
                    Constant::JSON_APP_ACHIEVEMENT_DETAIL => $appUserAchievement->getAchievementId()->getAchievementDetail(),
                );
            }
        }

        return $appUserAchievementsArray;
    }

    public function updateAchievements($userData) {
        
        $userData->getAppUserStatisticsId()->setAchievementsUnlocked(count($userData->getAppUserAchievements()));
        $this->em->persist($userData->getAppUserStatisticsId());
        $this->em->flush();
        
        return $userData;
    }

}
