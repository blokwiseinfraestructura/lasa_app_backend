<?php

/**
 * Created by PhpStorm.
 * User: BW_HG
 * Date: 19/12/16
 * Time: 11:03 AM
 */
namespace AppBundle\Service;

use AppBundle\Entity\Answer;
use AppBundle\Entity\Question;
use AppBundle\Utility\jsonWebToken;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;
use AppBundle\Constant\Constant;
class QuestionService
{
    private $repositoryAnswer;
    private $repositoryLevel;
    private $repositoryQuestion;
    private $repositoryTopic;
    private $container;
    private $em;

    public function __construct(EntityManager $entityManager, ContainerInterface $container)
    {
        $this->em = $entityManager;
        $this->container = $container;
        $this->repositoryAnswer = $this->em->getRepository(Constant::ENTITY_ANSWER);
        $this->repositoryLevel = $this->em->getRepository(Constant::ENTITY_LEVEL);
        $this->repositoryQuestion = $this->em->getRepository(Constant::ENTITY_QUESTION);
        $this->repositoryTopic = $this->em->getRepository(Constant::ENTITY_TOPIC);
    }

    public function getAllTopics()
    {
        $jsonResponse = array();
        $topics = $this->repositoryTopic->findBy(array());

        if (count($topics) > 0) {

            $topicsArray = array();
            foreach ($topics as $topic) {
                $topicsArray[] = array(
                    Constant::JSON_TOPIC_TOPIC_ID => $topic->getId(),
                    Constant::JSON_TOPIC_TOPIC_DESCRIPTION => $topic->getTopicDescription(),
                    Constant::JSON_TOPIC_TOPIC_CODE => $topic->getTopicCode()
                );
            }
            $jsonResponse = $topicsArray;

        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_NO_TOPICS_FOUND
            );
        }
        return $jsonResponse;
    }

    /*
     * Para listar los niveles epera un parametro que tipo booleano qeu si es = 'true' quiere decir que se listan para el CMS sino para el APP
     * */
    public function getAllLevels($requestType)
    {
        $jsonResponse = array();
        $levels = $this->repositoryLevel->findBy(array(), array(Constant::LEVEL_LEVEL_CODE => 'ASC'));

        if (count($levels) > 0) {


            $levelsArray = array();
            if($requestType == true){
                foreach ($levels as $level) {

                    if ($level->getLevelCode() != 9){
                        $levelsArray[] = array(
                            Constant::JSON_LEVEL_LEVEL_ID => $level->getId(),
                            Constant::JSON_LEVEL_LEVEL_DESCRIPTION => $level->getLevelDescription(),
                            Constant::JSON_LEVEL_LEVEL_CODE => $level->getLevelCode()
                        );
                    }
                }
            } elseif ($requestType == false) {

                foreach ($levels as $level) {
                    $levelsArray[] = array(
                        Constant::JSON_LEVEL_LEVEL_ID => $level->getId(),
                        Constant::JSON_LEVEL_LEVEL_DESCRIPTION => $level->getLevelDescription(),
                        Constant::JSON_LEVEL_LEVEL_CODE => $level->getLevelCode()
                    );
                }
            }

            $jsonResponse = $levelsArray;

        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_NO_LEVELS_FOUND
            );
        }
        return $jsonResponse;
    }

    /*
     * Funcion para obtener las preguntas de un tópico, recibe como parametro una intancia de la entidad Topic
     * */
    public function getAllTopicQuestions($topic)
    {
        $jsonResponse = array();
        $questions = $this->repositoryQuestion->findBy(array(Constant::QUESTION_QUESTION_TOPIC_ID => $topic), array
        (Constant::QUESTION_QUESTION_LEVEL_ID => 'DESC'));

        if (count($questions) > 0) {

            $questionsArray = array();
            foreach ($questions as $question){
                $questionsArray[] = array(
                    Constant::JSON_QUESTION_QUESTION_ID =>$question->getId(),
                    Constant::JSON_QUESTION_QUESTION_DESCRIPTION => $question->getQuestionDescription(),
                    Constant::JSON_ANSWER_ANSWERS_ARRAY => $this->getAllQuestionAnswers($question)
                );
            }
            $jsonResponse = $questionsArray;

        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_NO_QUESTIONS_FOUND
            );
        }
        return$jsonResponse;
    }

    /*
     * Funcion para obtener las respuestas de una pregunta, recibe como parametro una intancia de la entidad Question
     * */
    public function getAllQuestionAnswers($question)
    {
        $jsoResponse = array();
        $answers = $this->repositoryAnswer->findBy(array(Constant::ANSWER_ANSWER_QUESTION_ID => $question), array
        (Constant::ANSWER_ANSWER_IS_CORRECT => 'DESC'));

        if(count($answers) > 0){

            $answersArray = array();
            foreach ($answers as $answer){
                $answersArray[] = array(
                    Constant::JSON_ANSWER_ANSWER_ID => $answer->getId(),
                    Constant::ANSWER_ANSWER_DESCRIPTION => $answer->getAnswerDescription(),
                    Constant::ANSWER_ANSWER_IS_CORRECT => $answer->getIsCorrect(),
                );
            }
            $jsonResponse = $answersArray;

        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_NO_ANSWERS_FOUND
            );
        }
        return $jsonResponse;
    }

    public function createQuestion(Request $request)
    {
        $requestData = array(
            Constant::JSON_TOPIC => $request->request->get(Constant::JSON_TOPIC),
            Constant::JSON_LEVEL => $request->request->get(Constant::JSON_LEVEL),
            Constant::JSON_QUESTION_QUESTION_DESCRIPTION => $request->request->get(Constant::JSON_QUESTION_QUESTION_DESCRIPTION),
            Constant::JSON_ANSWER_ANSWERS_ARRAY =>$request->request->get(Constant::JSON_ANSWER_ANSWERS_ARRAY)
            );
        $jsonResponse = array();
        if ($requestData[Constant::JSON_TOPIC] && $requestData[Constant::JSON_LEVEL] && $requestData[Constant::JSON_QUESTION_QUESTION_DESCRIPTION]
            && $requestData[Constant::JSON_ANSWER_ANSWERS_ARRAY]) {

            if (is_numeric($requestData[Constant::JSON_TOPIC][Constant::JSON_TOPIC_TOPIC_ID]) && is_numeric
                ($requestData[Constant::JSON_LEVEL][Constant::JSON_LEVEL_LEVEL_ID])) {

                $topicEntity = $this->repositoryTopic->findOneBy(array(Constant::ID => $requestData[Constant::JSON_TOPIC][Constant::JSON_TOPIC_TOPIC_ID]));
                if (count($topicEntity) > 0) {

                    $topicObject = array(
                        Constant::JSON_TOPIC_TOPIC_ID => $topicEntity->getId(),
                        Constant::JSON_TOPIC_TOPIC_DESCRIPTION => $topicEntity->getTopicDescription(),
                        Constant::JSON_TOPIC_TOPIC_CODE => $topicEntity->getTopicCode()
                    );

                    $levelEntity = $this->repositoryLevel->findOneBy(array(Constant::ID => $requestData[Constant::JSON_LEVEL][Constant::JSON_LEVEL_LEVEL_ID]));
                    if (count($levelEntity) > 0) {

                        $levelObject = array(
                            Constant::JSON_LEVEL_LEVEL_ID => $levelEntity->getId(),
                            Constant::JSON_LEVEL_LEVEL_DESCRIPTION => $levelEntity->getLevelDescription(),
                            Constant::JSON_LEVEL_LEVEL_CODE => $levelEntity->getLevelCode()
                        );

                        if (count($requestData[Constant::JSON_ANSWER_ANSWERS_ARRAY]) == 4) {
                            $questionEntity = new Question();

                            $questionEntity->setLevelId($levelEntity);
                            $questionEntity->setTopicId($topicEntity);
                            $questionEntity->setQuestionDescription($requestData[Constant::JSON_QUESTION_QUESTION_DESCRIPTION]);
                            $this->em->persist($questionEntity);
                            $this->em->flush();

                            $answersArray = array();
                            foreach ($requestData[Constant::JSON_ANSWER_ANSWERS_ARRAY] as $answerObject) {
                                $answer = $this->createAnswer($questionEntity, $answerObject);
                                $answersArray[] = array(
                                    Constant::JSON_ANSWER_ANSWER_ID => $answer->getId(),
                                    Constant::JSON_ANSWER_ANSWER_DESCRIPTION => $answer->getAnswerDescription(),
                                    Constant::JSON_ANSWER_ANSWER_IS_CORRECT => $answer->getIsCorrect()
                                );
                            }

                            $jsonResponse = array(
                                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                                Constant::JSON_QUESTION =>[
                                    Constant::JSON_TOPIC => $topicObject,
                                    Constant::JSON_LEVEL => $levelObject,
                                    Constant::JSON_QUESTION_QUESTION_ID => $questionEntity->getId(),
                                    Constant::JSON_QUESTION_QUESTION_DESCRIPTION => $questionEntity->getQuestionDescription(),
                                    Constant::JSON_ANSWER_ANSWERS_ARRAY => $answersArray
                                ]
                            );

                        } else {
                            $jsonResponse = array(
                                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_FOUR_ANSWERS_EXACTLY_IS_MANDATORY
                            );
                        }

                    } else {
                        $jsonResponse = array(
                            Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                            Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_LEVEL_DOES_NOT_EXIST_OR_NOT_FOUND
                        );
                    }

                } else {
                    $jsonResponse = array(
                        Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                        Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_TOPIC_DOES_NOT_EXIST_OR_NOT_FOUND
                    );
                }

            } else {
                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_INVALID_PARAMETERS
                );
            }

            } else{
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_PARAMETERS
            );
        }
        return $jsonResponse;
    }

    public function editQuestion(Request $request)
    {
        $requestData = array(
            Constant::JSON_TOPIC => $request->request->get(Constant::JSON_TOPIC),
            Constant::JSON_LEVEL => $request->request->get(Constant::JSON_LEVEL),
            Constant::JSON_QUESTION_QUESTION_ID => $request->request->get(Constant::JSON_QUESTION_QUESTION_ID),
            Constant::JSON_QUESTION_QUESTION_DESCRIPTION => $request->request->get(Constant::JSON_QUESTION_QUESTION_DESCRIPTION),
            Constant::JSON_ANSWER_ANSWERS_ARRAY =>$request->request->get(Constant::JSON_ANSWER_ANSWERS_ARRAY)
        );
        $jsonResponse = array();

        if (is_numeric($requestData[Constant::JSON_QUESTION_QUESTION_ID])) {
            $questionEntity = $this->repositoryQuestion->find($requestData[Constant::JSON_QUESTION_QUESTION_ID]);
            if (count($questionEntity) >0) {

                if ($requestData[Constant::JSON_TOPIC] && $requestData[Constant::JSON_LEVEL] && $requestData[Constant::JSON_QUESTION_QUESTION_DESCRIPTION]
                    && $requestData[Constant::JSON_ANSWER_ANSWERS_ARRAY]) {

                    if (is_numeric($requestData[Constant::JSON_TOPIC][Constant::JSON_TOPIC_TOPIC_ID]) && is_numeric
                        ($requestData[Constant::JSON_LEVEL][Constant::JSON_LEVEL_LEVEL_ID])) {

                        $topicEntity = $this->repositoryTopic->findOneBy(array(Constant::ID => $requestData[Constant::JSON_TOPIC][Constant::JSON_TOPIC_TOPIC_ID]));
                        if (count($topicEntity) > 0) {

                            $topicObject = array(
                                Constant::JSON_TOPIC_TOPIC_ID => $topicEntity->getId(),
                                Constant::JSON_TOPIC_TOPIC_DESCRIPTION => $topicEntity->getTopicDescription(),
                                Constant::JSON_TOPIC_TOPIC_CODE => $topicEntity->getTopicCode()
                            );

                            $levelEntity = $this->repositoryLevel->findOneBy(array(Constant::ID => $requestData[Constant::JSON_LEVEL][Constant::JSON_LEVEL_LEVEL_ID]));
                            if (count($levelEntity) > 0) {

                                $levelObject = array(
                                    Constant::JSON_LEVEL_LEVEL_ID => $levelEntity->getId(),
                                    Constant::JSON_LEVEL_LEVEL_DESCRIPTION => $levelEntity->getLevelDescription(),
                                    Constant::JSON_LEVEL_LEVEL_CODE => $levelEntity->getLevelCode()
                                );

                                if (count($requestData[Constant::JSON_ANSWER_ANSWERS_ARRAY]) == 4) {
                                    $questionEntity = $this->repositoryQuestion->find($requestData[Constant::JSON_QUESTION_QUESTION_ID]);

                                    $questionEntity->setLevelId($levelEntity);
                                    $questionEntity->setTopicId($topicEntity);
                                    $questionEntity->setQuestionDescription($requestData[Constant::JSON_QUESTION_QUESTION_DESCRIPTION]);
                                    $this->em->persist($questionEntity);
                                    $this->em->flush();

                                    $answersArray = array();
                                    foreach ($requestData[Constant::JSON_ANSWER_ANSWERS_ARRAY] as $answerObject) {
                                        $answer = $this->editAnswer($answerObject);
                                        $answersArray[] = array(
                                            Constant::JSON_ANSWER_ANSWER_ID => $answer->getId(),
                                            Constant::JSON_ANSWER_ANSWER_DESCRIPTION => $answer->getAnswerDescription(),
                                            Constant::JSON_ANSWER_ANSWER_IS_CORRECT => $answer->getIsCorrect()
                                        );
                                    }

                                    $jsonResponse = array(
                                        Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                                        Constant::JSON_QUESTION =>[
                                            Constant::JSON_TOPIC => $topicObject,
                                            Constant::JSON_LEVEL => $levelObject,
                                            Constant::JSON_QUESTION_QUESTION_ID => $questionEntity->getId(),
                                            Constant::JSON_QUESTION_QUESTION_DESCRIPTION => $questionEntity->getQuestionDescription(),
                                            Constant::JSON_ANSWER_ANSWERS_ARRAY => $answersArray
                                        ]
                                    );

                                } else {
                                    $jsonResponse = array(
                                        Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                                        Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_FOUR_ANSWERS_EXACTLY_IS_MANDATORY
                                    );
                                }

                            } else {
                                $jsonResponse = array(
                                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_LEVEL_DOES_NOT_EXIST_OR_NOT_FOUND
                                );
                            }

                        } else {
                            $jsonResponse = array(
                                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_TOPIC_DOES_NOT_EXIST_OR_NOT_FOUND
                            );
                        }

                    } else {
                        $jsonResponse = array(
                            Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                            Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_INVALID_PARAMETERS
                        );
                    }


                } else{
                    $jsonResponse = array(
                        Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                        Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_PARAMETERS
                    );
                }

            } else{
                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_REQUESTED_QUESTION_NOT_FOUND
                );
            }
        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_QUESTION_ID_MUST_BE_A_NUMBER
            );
        }
        return $jsonResponse;
    }

    public function deleteQuestion(Request $request)
    {

        $requestData = array(
            Constant::JSON_TOPIC => $request->request->get(Constant::JSON_TOPIC),
            Constant::JSON_LEVEL => $request->request->get(Constant::JSON_LEVEL),
            Constant::JSON_QUESTION_QUESTION_ID => $request->request->get(Constant::JSON_QUESTION_QUESTION_ID),
            Constant::JSON_QUESTION_QUESTION_DESCRIPTION => $request->request->get(Constant::JSON_QUESTION_QUESTION_DESCRIPTION),
            Constant::JSON_ANSWER_ANSWERS_ARRAY =>$request->request->get(Constant::JSON_ANSWER_ANSWERS_ARRAY)
        );

        if ($requestData[Constant::JSON_QUESTION_QUESTION_ID]) {

            if (is_numeric($requestData[Constant::JSON_QUESTION_QUESTION_ID])) {
                $questionEntity = $this->repositoryQuestion->find($requestData[Constant::JSON_QUESTION_QUESTION_ID]);

                if(count($questionEntity) > 0){

                    $this->em->remove($questionEntity);
                    $this->em->flush();

                    $jsonResponse = $jsonResponse = array(
                        Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                        Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_QUESTION_WAS_DELETED_CORRECTLY
                    );


                } else{
                    $jsonResponse = array(
                        Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                        Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_REQUESTED_QUESTION_NOT_FOUND
                    );
                }
            } else {
                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_QUESTION_ID_MUST_BE_A_NUMBER
                );
            }

        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_PARAMETERS
            );
        }
        return $jsonResponse;
    }

    public function createAnswer($questionEntity, $answerObject)
    {
        $answerEntity = new Answer();
        $answerEntity->setAnswerDescription($answerObject[Constant::JSON_ANSWER_ANSWER_DESCRIPTION]);
        $answerEntity->setIsCorrect($answerObject[Constant::JSON_ANSWER_ANSWER_IS_CORRECT]);
        $answerEntity->setQuestionId($questionEntity);
        $this->em->persist($answerEntity);
        $this->em->flush();

        return $answerEntity;
    }

    public function editAnswer($answerObject)
    {
        $answerEntity = $this->repositoryAnswer->find($answerObject[Constant::JSON_ANSWER_ANSWER_ID]);

        if (count($answerEntity) > 0) {
            $answerEntity->setAnswerDescription($answerObject[Constant::JSON_ANSWER_ANSWER_DESCRIPTION]);
            $answerEntity->setIsCorrect($answerObject[Constant::JSON_ANSWER_ANSWER_IS_CORRECT]);
            $this->em->persist($answerEntity);
            $this->em->flush();
        }
        return $answerEntity;
    }

    public function getAllQuestions(Request $request)
    {
        $jsonResponse = array();
        $questionsEntities = $this->repositoryQuestion->findBy(array(), array(Constant::ID => 'DESC'));

        if (count($questionsEntities) > 0) {

            $questionsArray = array();
            foreach ($questionsEntities as $questionEntity){
                $questionsArray[] = array(
                    Constant::JSON_TOPIC => array(
                        Constant::JSON_TOPIC_TOPIC_ID => $questionEntity->getTopicId()->getId(),
                        Constant::JSON_TOPIC_TOPIC_DESCRIPTION => $questionEntity->getTopicId()->getTopicDescription(),
                        Constant::JSON_TOPIC_TOPIC_CODE => $questionEntity->getTopicId()->getTopicCode()
                    ),
                    Constant::JSON_LEVEL => array(
                        Constant::JSON_LEVEL_LEVEL_ID => $questionEntity->getLevelId()->getId(),
                        Constant::JSON_LEVEL_LEVEL_DESCRIPTION => $questionEntity->getLevelId()->getLevelDescription(),
                        Constant::JSON_LEVEL_LEVEL_CODE => $questionEntity->getLevelId()->getLevelCode()
                    ),
                    Constant::JSON_QUESTION_QUESTION_ID => $questionEntity->getId(),
                    Constant::JSON_QUESTION_QUESTION_DESCRIPTION => $questionEntity->getQuestionDescription(),
                    Constant::JSON_ANSWER_ANSWERS_ARRAY => $this->getAllQuestionAnswers($questionEntity)
                );
            }

            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                Constant::JSON_TOPIC_TOPICS_ARRAY => $this->getAllTopics(),
                Constant::JSON_LEVEL_LEVELS_ARRAY => $this->getAllLevels(false),
                Constant::JSON_LEVEL_LEVEL_QUESTIONS_ARRAY =>$questionsArray
            );

        } else {
            $questionsArray = array();
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                Constant::JSON_TOPIC_TOPICS_ARRAY => $this->getAllTopics(),
                Constant::JSON_LEVEL_LEVELS_ARRAY => $this->getAllLevels(false),
                Constant::JSON_LEVEL_LEVEL_QUESTIONS_ARRAY => $questionsArray
            );
        }
        return $jsonResponse;
    }

    public function getAllQuestionsForApp()
    {
        $jsonResponse = array();
        $questionsEntities = $this->repositoryQuestion->findBy(array());

        if (count($questionsEntities) > 0) {

            $questionsArray = array();
            foreach ($questionsEntities as $questionEntity){
                $questionsArray[] = array(
                    Constant::JSON_TOPIC => array(
                        Constant::JSON_TOPIC_TOPIC_ID => $questionEntity->getTopicId()->getId(),
                        Constant::JSON_TOPIC_TOPIC_DESCRIPTION => $questionEntity->getTopicId()->getTopicDescription(),
                        Constant::JSON_TOPIC_TOPIC_CODE => $questionEntity->getTopicId()->getTopicCode()
                    ),
                    Constant::JSON_LEVEL => array(
                        Constant::JSON_LEVEL_LEVEL_ID => $questionEntity->getLevelId()->getId(),
                        Constant::JSON_LEVEL_LEVEL_DESCRIPTION => $questionEntity->getLevelId()->getLevelDescription(),
                        Constant::JSON_LEVEL_LEVEL_CODE => $questionEntity->getLevelId()->getLevelCode()
                    ),
                    Constant::JSON_QUESTION_QUESTION_ID => $questionEntity->getId(),
                    Constant::JSON_QUESTION_QUESTION_DESCRIPTION => $questionEntity->getQuestionDescription(),
                    Constant::JSON_ANSWER_ANSWERS_ARRAY => $this->getAllQuestionAnswers($questionEntity)
                );
            }
            $jsonResponse = $questionsArray;

        }
        return $jsonResponse;
    }
}