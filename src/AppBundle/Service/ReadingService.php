<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 24/01/17
 * Time: 8:12 PM
 */

namespace AppBundle\Service;

use AppBundle\Entity\Answer;
use AppBundle\Entity\Question;
use AppBundle\Entity\Reading;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;
use AppBundle\Constant\Constant;
class ReadingService
{
    private $repositoryReading;
    private $repositoryLevel;
    private $repositoryTopic;
    private $container;
    private $em;

    public function __construct(EntityManager $entityManager, ContainerInterface $container)
    {
        $this->em = $entityManager;
        $this->container = $container;
        $this->repositoryLevel = $this->em->getRepository(Constant::ENTITY_LEVEL);
        $this->repositoryTopic = $this->em->getRepository(Constant::ENTITY_TOPIC);
        $this->repositoryReading = $this->em->getRepository(Constant::ENTITY_READING);
    }

    public function createReading(Request $request)
    {
        $requestData = array(
            Constant::JSON_READING_READING_DESCRIPTION => $request->request->get(Constant::JSON_READING_READING_DESCRIPTION),
            Constant::JSON_READING_READING_FILE => $request->files->get(Constant::JSON_READING_READING_FILE),
            Constant::JSON_LEVEL_LEVEL_ID => $request->request->get(Constant::JSON_LEVEL_LEVEL_ID),
            Constant::JSON_TOPIC_TOPIC_ID => $request->request->get(Constant::JSON_TOPIC_TOPIC_ID),
        );
        $hostName = $_SERVER['HTTP_HOST'];
        $protocol = isset($_SERVER["HTTPS"]) ? 'https' : 'http';
        $jsonResponse = array();
        $serverDate = new \DateTime();

        if ($requestData[Constant::JSON_READING_READING_DESCRIPTION] && $requestData[Constant::JSON_READING_READING_FILE] && is_numeric($requestData[Constant::JSON_LEVEL_LEVEL_ID]) &&
            is_numeric($requestData[Constant::JSON_TOPIC_TOPIC_ID])) {

            $levelEntity = $this->repositoryLevel->findOneBy(array(Constant::ID => $requestData[Constant::JSON_LEVEL_LEVEL_ID]));
            if (count($levelEntity) > 0) {

                $levelObject = array(
                    Constant::JSON_LEVEL_LEVEL_ID => $levelEntity->getId(),
                    Constant::JSON_LEVEL_LEVEL_DESCRIPTION => $levelEntity->getLevelDescription(),
                    Constant::JSON_LEVEL_LEVEL_CODE => $levelEntity->getLevelCode()
                );

                $topicEntity = $this->repositoryTopic->findOneBy(array(Constant::ID => $requestData[Constant::JSON_TOPIC_TOPIC_ID]));
                if (count($topicEntity) > 0) {

                    $topicObject = array(
                        Constant::JSON_TOPIC_TOPIC_ID => $topicEntity->getId(),
                        Constant::JSON_TOPIC_TOPIC_DESCRIPTION => $topicEntity->getTopicDescription(),
                        Constant::JSON_TOPIC_TOPIC_CODE => $topicEntity->getTopicCode()
                    );

                    $readingEntity = new Reading();
                    $readingEntity->setReadingDescription($requestData[Constant::JSON_READING_READING_DESCRIPTION]);
                    $readingEntity->setLevelId($levelEntity);
                    $readingEntity->setTopicId($topicEntity);

                    /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
                    $file = $requestData[Constant::JSON_READING_READING_FILE];

                    $fileName = md5(uniqid()) . $serverDate->getTimestamp() . '.' . $file->guessExtension();

                    $file->move(
                        $this->container->getParameter('image_readings_directory'), $fileName
                    );

                    $readingEntity->setReadingImageName($fileName);

                    $this->em->persist($readingEntity);
                    $this->em->flush();


                    $jsonResponse = array(
                        Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                        Constant::JSON_READING_READING =>[
                            Constant::JSON_TOPIC => $topicObject,
                            Constant::JSON_LEVEL => $levelObject,
                            Constant::JSON_READING_READING_ID => $readingEntity->getId(),
                            Constant::JSON_READING_READING_DESCRIPTION => $readingEntity->getReadingDescription(),
                            Constant::JSON_READING_READING_IMAGE_PATH => $protocol.'://'.$hostName.Constant::JSON_USEFUL_DEFAULT_READINGS_IMAGES_DIRECTORY.$readingEntity->getReadingImageName()
                        ]
                    );

                } else {
                    $jsonResponse = array(
                        Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                        Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_TOPIC_DOES_NOT_EXIST_OR_NOT_FOUND
                    );
                }

            } else{
                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_LEVEL_DOES_NOT_EXIST_OR_NOT_FOUND
                );
            }

        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_PARAMETERS
            );
        }
        return $jsonResponse;
    }

    public function getAllReadings(Request $request)
    {
        $readingEntities = $this->repositoryReading->findBy(array(), array(Constant::ID => 'DESC'));

        $hostName = $_SERVER['HTTP_HOST'];
        $protocol = isset($_SERVER["HTTPS"]) ? 'https' : 'http';

        if (count($readingEntities) > 0) {
            $readingsArray = array();
            foreach ($readingEntities as $readingEntity) {
                $readingsArray[] = array(
                    Constant::JSON_TOPIC => array(
                        Constant::JSON_TOPIC_TOPIC_ID => $readingEntity->getTopicId()->getId(),
                        Constant::JSON_TOPIC_TOPIC_DESCRIPTION => $readingEntity->getTopicId()->getTopicDescription(),
                        Constant::JSON_TOPIC_TOPIC_CODE => $readingEntity->getTopicId()->getTopicCode()
                    ),
                    Constant::JSON_LEVEL => array(
                        Constant::JSON_LEVEL_LEVEL_ID => $readingEntity->getLevelId()->getId(),
                        Constant::JSON_LEVEL_LEVEL_DESCRIPTION => $readingEntity->getLevelId()->getLevelDescription(),
                        Constant::JSON_LEVEL_LEVEL_CODE => $readingEntity->getLevelId()->getLevelCode(),
                    ),
                    Constant::JSON_READING_READING_ID => $readingEntity->getId(),
                    Constant::JSON_READING_READING_DESCRIPTION => $readingEntity->getReadingDescription(),
                    Constant::JSON_READING_READING_IMAGE_PATH => $protocol.'://'.$hostName.Constant::JSON_USEFUL_DEFAULT_READINGS_IMAGES_DIRECTORY.$readingEntity->getReadingImageName()
                );
            }

            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                Constant::JSON_TOPIC_TOPICS_ARRAY => $this->container->get('api.QuestionService')->getAllTopics(),
                Constant::JSON_LEVEL_LEVELS_ARRAY => $this->container->get('api.QuestionService')->getAllLevels(true),
                Constant::JSON_READING_READINGS_ARRAY => $readingsArray
            );
        } else {
            $readingsArray = array();
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                Constant::JSON_TOPIC_TOPICS_ARRAY => $this->container->get('api.QuestionService')->getAllTopics(),
                Constant::JSON_LEVEL_LEVELS_ARRAY => $this->container->get('api.QuestionService')->getAllLevels(true),
                Constant::JSON_READING_READINGS_ARRAY => $readingsArray
            );
        }

        return $jsonResponse;

    }

    public function updateReading(Request $request)
    {
        $requestData = array(
            Constant::JSON_READING_READING_ID => $request->request->get(Constant::JSON_READING_READING_ID),
            Constant::JSON_READING_READING_DESCRIPTION => $request->request->get(Constant::JSON_READING_READING_DESCRIPTION),
            Constant::JSON_READING_READING_FILE => $request->files->get(Constant::JSON_READING_READING_FILE),
            Constant::JSON_LEVEL_LEVEL_ID => $request->request->get(Constant::JSON_LEVEL_LEVEL_ID),
            Constant::JSON_TOPIC_TOPIC_ID => $request->request->get(Constant::JSON_TOPIC_TOPIC_ID),
        );
        $hostName = $_SERVER['HTTP_HOST'];
        $protocol = isset($_SERVER["HTTPS"]) ? 'https' : 'http';
        $serverDate = new \DateTime();
        $jsonResponse = array();

        if (is_numeric($requestData[Constant::JSON_READING_READING_ID])) {

            $readingEntity = $this->repositoryReading->findOneBy(array(Constant::ID => $requestData[Constant::JSON_READING_READING_ID]));

            if (count($readingEntity) > 0) {

                switch ($requestData){

                    case($requestData[Constant::JSON_READING_READING_DESCRIPTION] != null && $requestData[Constant::JSON_READING_READING_FILE] != null &&
                        $requestData[Constant::JSON_LEVEL_LEVEL_ID] != null && $requestData[Constant::JSON_TOPIC_TOPIC_ID] != null):

                        $readingEntity->setReadingDescription($requestData[Constant::JSON_READING_READING_DESCRIPTION]);

                        $levelEntity = $this->repositoryLevel->findOneBy(array(Constant::ID => $requestData[Constant::JSON_LEVEL_LEVEL_ID]));
                        $topicEntity = $this->repositoryTopic->findOneBy(array(Constant::ID => $requestData[Constant::JSON_TOPIC_TOPIC_ID]));
                        if (count($topicEntity) > 0) {
                            $readingEntity->setTopicId($topicEntity);
                        }
                        if (count($levelEntity) > 0) {
                            $readingEntity->setLevelId($levelEntity);
                        }



                        /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
                        $file = $requestData[Constant::JSON_READING_READING_FILE];

                        $fileName = md5(uniqid()) . $serverDate->getTimestamp() .'.' . $file->guessExtension();

                        $file->move(
                            $this->container->getParameter('image_readings_directory'), $fileName
                        );

                        $readingEntity->setReadingImageName($fileName);

                        $this->em->persist($readingEntity);
                        $this->em->flush();

                        break;

                    case ($requestData[Constant::JSON_READING_READING_DESCRIPTION]!= null && $requestData[Constant::JSON_READING_READING_FILE] != null &&
                        $requestData[Constant::JSON_LEVEL_LEVEL_ID] != null):

                        $readingEntity->setReadingDescription($requestData[Constant::JSON_READING_READING_DESCRIPTION]);

                        $levelEntity = $this->repositoryLevel->findOneBy(array(Constant::ID => $requestData[Constant::JSON_LEVEL_LEVEL_ID]));

                        if (count($levelEntity) > 0) {
                            $readingEntity->setLevelId($levelEntity);
                        }

                        /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
                        $file = $requestData[Constant::JSON_READING_READING_FILE];

                        $fileName = md5(uniqid()) . $serverDate->getTimestamp() .'.' . $file->guessExtension();

                        $file->move(
                            $this->container->getParameter('image_readings_directory'), $fileName
                        );

                        $readingEntity->setReadingImageName($fileName);

                        $this->em->persist($readingEntity);
                        $this->em->flush();

                        break;

                    case($requestData[Constant::JSON_READING_READING_DESCRIPTION] != null && $requestData[Constant::JSON_READING_READING_FILE] != null &&
                        $requestData[Constant::JSON_TOPIC_TOPIC_ID] != null):

                        $readingEntity->setReadingDescription($requestData[Constant::JSON_READING_READING_DESCRIPTION]);
                        $topicEntity = $this->repositoryTopic->findOneBy(array(Constant::ID => $requestData[Constant::JSON_TOPIC_TOPIC_ID]));
                        if (count($topicEntity) > 0) {
                            $readingEntity->setTopicId($topicEntity);
                        }

                        /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
                        $file = $requestData[Constant::JSON_READING_READING_FILE];

                        $fileName = md5(uniqid()) . $serverDate->getTimestamp() . '.' . $file->guessExtension();

                        $file->move(
                            $this->container->getParameter('image_readings_directory'), $fileName
                        );

                        $readingEntity->setReadingImageName($fileName);

                        $this->em->persist($readingEntity);
                        $this->em->flush();

                        break;

                    case ($requestData[Constant::JSON_READING_READING_DESCRIPTION] != null && $requestData[Constant::JSON_LEVEL_LEVEL_ID] != null &&
                        $requestData[Constant::JSON_TOPIC_TOPIC_ID] != null):

                        $readingEntity->setReadingDescription($requestData[Constant::JSON_READING_READING_DESCRIPTION]);
                        $levelEntity = $this->repositoryLevel->findOneBy(array(Constant::ID => $requestData[Constant::JSON_LEVEL_LEVEL_ID]));
                        $topicEntity = $this->repositoryTopic->findOneBy(array(Constant::ID => $requestData[Constant::JSON_TOPIC_TOPIC_ID]));
                        if (count($topicEntity) > 0) {
                            $readingEntity->setTopicId($topicEntity);
                        }
                        if (count($levelEntity) > 0) {
                            $readingEntity->setLevelId($levelEntity);
                        }

                        $this->em->persist($readingEntity);
                        $this->em->flush();

                        break;

                    case($requestData[Constant::JSON_READING_READING_FILE] != null && $requestData[Constant::JSON_LEVEL_LEVEL_ID] != null &&
                        $requestData[Constant::JSON_TOPIC_TOPIC_ID] != null):

                        $levelEntity = $this->repositoryLevel->findOneBy(array(Constant::ID => $requestData[Constant::JSON_LEVEL_LEVEL_ID]));
                        $topicEntity = $this->repositoryTopic->findOneBy(array(Constant::ID => $requestData[Constant::JSON_TOPIC_TOPIC_ID]));
                        if (count($topicEntity) > 0) {
                            $readingEntity->setTopicId($topicEntity);
                        }
                        if (count($levelEntity) > 0) {
                            $readingEntity->setLevelId($levelEntity);
                        }

                        /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
                        $file = $requestData[Constant::JSON_READING_READING_FILE];

                        $fileName = md5(uniqid()) . $serverDate->getTimestamp() . '.' . $file->guessExtension();

                        $file->move(
                            $this->container->getParameter('image_readings_directory'), $fileName
                        );

                        $readingEntity->setReadingImageName($fileName);

                        $this->em->persist($readingEntity);
                        $this->em->flush();

                        break;

                    case($requestData[Constant::JSON_READING_READING_DESCRIPTION] != null && $requestData[Constant::JSON_READING_READING_FILE] != null):

                        $readingEntity->setReadingDescription($requestData[Constant::JSON_READING_READING_DESCRIPTION]);

                        /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
                        $file = $requestData[Constant::JSON_READING_READING_FILE];

                        $fileName = md5(uniqid()) . $serverDate->getTimestamp() . '.' . $file->guessExtension();

                        $file->move(
                            $this->container->getParameter('image_readings_directory'), $fileName
                        );

                        $readingEntity->setReadingImageName($fileName);

                        $this->em->persist($readingEntity);
                        $this->em->flush();

                        break;

                    case ($requestData[Constant::JSON_READING_READING_DESCRIPTION] != null && $requestData[Constant::JSON_LEVEL_LEVEL_ID] != null):

                        $readingEntity->setReadingDescription($requestData[Constant::JSON_READING_READING_DESCRIPTION]);
                        $levelEntity = $this->repositoryLevel->findOneBy(array(Constant::ID => $requestData[Constant::JSON_LEVEL_LEVEL_ID]));
                        if (count($levelEntity) > 0) {
                            $readingEntity->setLevelId($levelEntity);
                        }

                        $this->em->persist($readingEntity);
                        $this->em->flush();

                        break;

                    case($requestData[Constant::JSON_READING_READING_DESCRIPTION] != null && $requestData[Constant::JSON_TOPIC_TOPIC_ID] != null):

                        $readingEntity->setReadingDescription($requestData[Constant::JSON_READING_READING_DESCRIPTION]);
                        $topicEntity = $this->repositoryTopic->findOneBy(array(Constant::ID => $requestData[Constant::JSON_TOPIC_TOPIC_ID]));
                        if (count($topicEntity) > 0) {
                            $readingEntity->setTopicId($topicEntity);
                        }

                        $this->em->persist($readingEntity);
                        $this->em->flush();

                        break;

                    case($requestData[Constant::JSON_READING_READING_FILE] != null && $requestData[Constant::JSON_LEVEL_LEVEL_ID] != null):

                        $levelEntity = $this->repositoryLevel->findOneBy(array(Constant::ID => $requestData[Constant::JSON_LEVEL_LEVEL_ID]));
                        if (count($levelEntity) > 0) {
                            $readingEntity->setLevelId($levelEntity);
                        }

                        /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
                        $file = $requestData[Constant::JSON_READING_READING_FILE];

                        $fileName = md5(uniqid()) . $serverDate->getTimestamp() . '.' . $file->guessExtension();

                        $file->move(
                            $this->container->getParameter('image_readings_directory'), $fileName
                        );

                        $readingEntity->setReadingImageName($fileName);

                        $this->em->persist($readingEntity);
                        $this->em->flush();

                        break;

                    case ($requestData[Constant::JSON_READING_READING_FILE] != null && $requestData[Constant::JSON_TOPIC_TOPIC_ID] != null):

                        $topicEntity = $this->repositoryTopic->findOneBy(array(Constant::ID => $requestData[Constant::JSON_TOPIC_TOPIC_ID]));
                        if (count($topicEntity) > 0) {
                            $readingEntity->setTopicId($topicEntity);
                        }

                        /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
                        $file = $requestData[Constant::JSON_READING_READING_FILE];

                        $fileName = md5(uniqid()) . $serverDate->getTimestamp() . '.' . $file->guessExtension();

                        $file->move(
                            $this->container->getParameter('image_readings_directory'), $fileName
                        );

                        $readingEntity->setReadingImageName($fileName);

                        $this->em->persist($readingEntity);
                        $this->em->flush();

                        break;

                    case ($requestData[Constant::JSON_LEVEL_LEVEL_ID]  != null && $requestData[Constant::JSON_TOPIC_TOPIC_ID] != null):

                        $levelEntity = $this->repositoryLevel->findOneBy(array(Constant::ID => $requestData[Constant::JSON_LEVEL_LEVEL_ID]));
                        $topicEntity = $this->repositoryTopic->findOneBy(array(Constant::ID => $requestData[Constant::JSON_TOPIC_TOPIC_ID]));
                        if (count($topicEntity) > 0) {
                            $readingEntity->setTopicId($topicEntity);
                        }
                        if (count($levelEntity) > 0) {
                            $readingEntity->setLevelId($levelEntity);
                        }

                        $this->em->persist($readingEntity);
                        $this->em->flush();

                        break;

                    case($requestData[Constant::JSON_READING_READING_DESCRIPTION] != null):

                        $readingEntity->setReadingDescription($requestData[Constant::JSON_READING_READING_DESCRIPTION]);

                        $this->em->persist($readingEntity);
                        $this->em->flush();

                        break;

                    case ($requestData[Constant::JSON_READING_READING_FILE] != null):

                        /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
                        $file = $requestData[Constant::JSON_READING_READING_FILE];

                        $fileName = md5(uniqid()) . $serverDate->getTimestamp() . '.' . $file->guessExtension();

                        $file->move(
                            $this->container->getParameter('image_readings_directory'), $fileName
                        );

                        $readingEntity->setReadingImageName($fileName);

                        $this->em->persist($readingEntity);
                        $this->em->flush();

                        break;

                    case($requestData[Constant::JSON_LEVEL_LEVEL_ID] != null):

                        $levelEntity = $this->repositoryLevel->findOneBy(array(Constant::ID => $requestData[Constant::JSON_LEVEL_LEVEL_ID]));

                        if (count($levelEntity) > 0) {
                            $readingEntity->setLevelId($levelEntity);
                        }

                        $this->em->persist($readingEntity);
                        $this->em->flush();

                        break;

                    case ($requestData[Constant::JSON_TOPIC_TOPIC_ID] != null):

                        $topicEntity = $this->repositoryTopic->findOneBy(array(Constant::ID => $requestData[Constant::JSON_TOPIC_TOPIC_ID]));
                        if (count($topicEntity) > 0) {
                            $readingEntity->setTopicId($topicEntity);
                        }

                        $this->em->persist($readingEntity);
                        $this->em->flush();

                        break;
                }

                $topicObject = array(
                    Constant::JSON_TOPIC_TOPIC_ID => $readingEntity->getTopicId()->getId(),
                    Constant::JSON_TOPIC_TOPIC_DESCRIPTION => $readingEntity->getTopicId()->getTopicDescription(),
                    Constant::JSON_TOPIC_TOPIC_CODE => $readingEntity->getTopicId()->getTopicCode()
                );

                $levelObject = array(
                    Constant::JSON_LEVEL_LEVEL_ID => $readingEntity->getLevelId()->getId(),
                    Constant::JSON_LEVEL_LEVEL_DESCRIPTION => $readingEntity->getLevelId()->getLevelDescription(),
                    Constant::JSON_LEVEL_LEVEL_CODE => $readingEntity->getLevelId()->getLevelCode()
                );

                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                    Constant::JSON_READING_READING =>[
                        Constant::JSON_TOPIC => $topicObject,
                        Constant::JSON_LEVEL => $levelObject,
                        Constant::JSON_READING_READING_ID => $readingEntity->getId(),
                        Constant::JSON_READING_READING_DESCRIPTION => $readingEntity->getReadingDescription(),
                        Constant::JSON_READING_READING_IMAGE_PATH => $protocol.'://'.$hostName.Constant::JSON_USEFUL_DEFAULT_READINGS_IMAGES_DIRECTORY.$readingEntity->getReadingImageName()
                    ]
                );

            } else {
                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_REQUESTED_READING_NOT_FOUND
                );
            }

        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_PARAMETERS
            );
        }
        return $jsonResponse;
    }

    public function deleteReading(Request $request)
    {
        $requestData = array(
            Constant::JSON_READING_READING => $request->request->get(Constant::JSON_READING_READING),
        );
        $jsonResponse = array();

        if (is_numeric($requestData[Constant::JSON_READING_READING][Constant::JSON_READING_READING_ID])) {

            $readingEntity = $this->repositoryReading->findOneBy(array(Constant::ID => $requestData[Constant::JSON_READING_READING][Constant::JSON_READING_READING_ID]));

            if (count($readingEntity) > 0 ) {

                $this->em->remove($readingEntity);
                $this->em->flush();

                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_READING_WAS_DELETED_CORRECTLY
                );

            } else {
                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_REQUESTED_READING_NOT_FOUND
                );
            }

        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_PARAMETERS
            );
        }

        return $jsonResponse;
    }

    public function getAllReadingsForApp()
    {
        $jsonResponse = array();
        $hostName = $_SERVER['HTTP_HOST'];
        $protocol = isset($_SERVER["HTTPS"]) ? 'https' : 'http';
        $readingEntities = $this->repositoryReading->findBy(array());

        if (count($readingEntities) > 0) {

            $readingsArray = array();
            foreach ($readingEntities as $readingEntity) {
                $readingsArray[] = array(
                    Constant::JSON_TOPIC => array(
                        Constant::JSON_TOPIC_TOPIC_ID => $readingEntity->getTopicId()->getId(),
                        Constant::JSON_TOPIC_TOPIC_DESCRIPTION => $readingEntity->getTopicId()->getTopicDescription(),
                        Constant::JSON_TOPIC_TOPIC_CODE => $readingEntity->getTopicId()->getTopicCode()
                    ),
                    Constant::JSON_LEVEL => array(
                        Constant::JSON_LEVEL_LEVEL_ID => $readingEntity->getLevelId()->getId(),
                        Constant::JSON_LEVEL_LEVEL_DESCRIPTION => $readingEntity->getLevelId()->getLevelDescription(),
                        Constant::JSON_LEVEL_LEVEL_CODE => $readingEntity->getLevelId()->getLevelCode(),
                    ),
                    Constant::JSON_READING_READING_ID => $readingEntity->getId(),
                    Constant::JSON_READING_READING_DESCRIPTION => $readingEntity->getReadingDescription(),
                    Constant::JSON_READING_READING_IMAGE_PATH => $protocol.'://'.$hostName.Constant::JSON_USEFUL_DEFAULT_READINGS_IMAGES_DIRECTORY.$readingEntity->getReadingImageName()
                );
            }
            $jsonResponse = $readingsArray;
        }
        return $jsonResponse;
    }
}