<?php
/**
 * Created by PhpStorm.
 * User: higom
 * Date: 26/12/2016
 * Time: 8:28 AM
 */

namespace AppBundle\Service;

use AppBundle\Entity\AppUser;
use AppBundle\Entity\AppUserModule;
use AppBundle\Entity\UserImage;
use AppBundle\Entity\AppUserStatistics;
use AppBundle\Utility\jsonWebToken;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;
use AppBundle\Constant\Constant;
use Symfony\Component\Validator\Tests\Fixtures\ConstraintA;

class AppUserService
{
    private $repositoryAdminModule;
    private $repositoryAdminUser;
    private $repositoryAnswer;
    private $repositoryAppModule;
    private $repositoryAppUser;
    private $repositoryAppUserStatistics;
    private $repositoryAppUserModule;
    private $repositoryCity;
    private $repositoryLevel;
    private $repositoryQuestion;
    private $repositoryTopic;
    private $repositoryUserAdminModule;
    private $repositoryWorkArea;
    private $repositoryUserImage;
    private $container;
    private $em;

    public function __construct(EntityManager $entityManager, ContainerInterface $container)
    {
        $this->em = $entityManager;
        $this->container = $container;
        $this->repositoryAdminModule = $this->em->getRepository(Constant::ENTITY_ADMIN_MODULE);
        $this->repositoryAdminUser = $this->em->getRepository(Constant::ENTITY_ADMIN_USER);
        $this->repositoryAnswer = $this->em->getRepository(Constant::ENTITY_ANSWER);
        $this->repositoryAppModule = $this->em->getRepository(Constant::ENTITY_APP_MODULE);
        $this->repositoryAppUser = $this->em->getRepository(Constant::ENTITY_APP_USER);
        $this->repositoryAppUserStatistics = $this->em->getRepository(Constant::ENTITY_APP_USER_STATISTICS);
        $this->repositoryAppUserModule = $this->em->getRepository(Constant::ENTITY_APP_USER_MODULE);
        $this->repositoryCity = $this->em->getRepository(Constant::ENTITY_CITY);
        $this->repositoryLevel = $this->em->getRepository(Constant::ENTITY_LEVEL);
        $this->repositoryQuestion = $this->em->getRepository(Constant::ENTITY_QUESTION);
        $this->repositoryTopic = $this->em->getRepository(Constant::ENTITY_TOPIC);
        $this->repositoryUserAdminModule = $this->em->getRepository(Constant::ENTITY_USER_ADMIN_MODULE);
        $this->repositoryWorkArea = $this->em->getRepository(Constant::ENTITY_WORK_AREA);
        $this->repositoryUserImage = $this->em->getRepository(Constant::ENTITY_USER_IMAGE);
    }

    public function getAppGeneralParameters(Request $request)
    {
        $serverDate = new \DateTime();
        $jsonResponse = array();

        $levels = $this->container->get('api.QuestionService')->getAllLevels(false);
        $topics = $this->container->get('api.QuestionService')->getAllTopics();
        $topicQuestions = $this->container->get('api.QuestionService')->getAllQuestionsForApp();
        $appModules = $this->container->get('api.AppModuleService')->getAllAppModules();
        $appCities = $this->container->get('api.CityService')->getAllCities();
        $workAreas = $this->container->get('api.WorkAreaService')->getAllWorkAreas();
        $news = $this->container->get('api.NewsService')->getAllNewsForApp();
        $readings = $this->container->get('api.ReadingService')->getAllReadingsForApp();
        $appHelpfulConstant = $this->container->get('api.AppConstantService')->getAllAppHelpfulConstant();
        $appAchievements = $this->container->get('api.AchievementService')->getAllAchievements();

        $jsonResponse = array(
            Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
            Constant::JSON_DATE_SERVER_CURRENT_DATE => $serverDate->getTimestamp(),
            Constant::JSON_APP_GENERAL_PARAMETERS => [
                Constant::JSON_APP_CONSTANT_APP_CONSTANTS_ARRAY => $appHelpfulConstant,
                Constant::JSON_NEWS_NEWS_ARRAY => $news,
                Constant::JSON_WORK_AREAS => $workAreas,
                Constant::JSON_CITIES_ARRAY => $appCities,
                Constant::JSON_LEVEL_APP_LEVELS_ARRAY => $levels,
                Constant::JSON_APP_MODULE_APP_MODULES => $appModules,
                Constant::JSON_TOPIC_APP_TOPICS_ARRAY => $topics,
                Constant::JSON_APP_ACHIEVEMENTS_ARRAY => $appAchievements,
                Constant::JSON_READING_APP_READINGS_ARRAY => $readings,
                Constant::JSON_QUESTION_APP_QUESTIONS_ARRAY => $topicQuestions,
            ]
        );

        return $jsonResponse;
    }

    public function registerAppUser(Request $request)
    {
        $requestData = array(
            Constant::JSON_APP_USER_NAME => $request->request->get(Constant::JSON_APP_USER_NAME),
            Constant::JSON_APP_USER_LAST_NAME => $request->request->get(Constant::JSON_APP_USER_LAST_NAME),
            Constant::JSON_APP_USER_EMAIL => $request->request->get(Constant::JSON_APP_USER_EMAIL),
            Constant::JSON_APP_USER_IDENTIFICATION_NUMBER => $request->request->get(Constant::JSON_APP_USER_IDENTIFICATION_NUMBER),
            Constant::JSON_APP_USER_PASSWORD => $request->request->get(Constant::JSON_APP_USER_PASSWORD),
            Constant::JSON_APP_USER_DEVICE_TYPE => $request->request->get(Constant::JSON_APP_USER_DEVICE_TYPE),
            Constant::JSON_APP_USER_DEVICE_TOKEN => $request->request->get(Constant::JSON_APP_USER_DEVICE_TOKEN),
            Constant::JSON_CITY => $request->request->get(Constant::JSON_CITY),
            Constant::JSON_WORK_AREA => $request->request->get(Constant::JSON_WORK_AREA)
        );
        $jsonResponse = array();
        $serverDate = new \DateTime();

        if ($requestData[Constant::JSON_APP_USER_NAME] && $requestData[Constant::JSON_APP_USER_LAST_NAME] && $requestData[Constant::JSON_APP_USER_EMAIL] &&
            $requestData[Constant::JSON_APP_USER_IDENTIFICATION_NUMBER] /*&& $requestData[Constant::JSON_APP_USER_PASSWORD]*/ && is_numeric
            ($requestData[Constant::JSON_CITY][Constant::JSON_CITY_ID]) && is_numeric($requestData[Constant::JSON_WORK_AREA][Constant::JSON_WORK_AREA_ID]) &&
            $requestData[Constant::JSON_APP_USER_DEVICE_TYPE] && $requestData[Constant::JSON_APP_USER_DEVICE_TOKEN]) {


            $emailValidator = $this->container->get('api.AdminService')->emailVerification($requestData[Constant::JSON_APP_USER_EMAIL]);

            if ($emailValidator == true) {

                $appUserValidatorByIdentificationNumber = $this->repositoryAppUser->findOneBy(array(Constant::APP_USER_IDENTIFICATION_NUMBER =>
                    $requestData[Constant::JSON_APP_USER_IDENTIFICATION_NUMBER]));

                if (count($appUserValidatorByIdentificationNumber) == null) {

                    $appUserValidatorByEmail = $this->repositoryAppUser->findOneBy(array(Constant::APP_USER_EMAIL => $requestData[Constant::JSON_APP_USER_EMAIL]));

                    if (count($appUserValidatorByEmail) == null) {

                        $cityEntity = $this->repositoryCity->find($requestData[Constant::JSON_CITY][Constant::JSON_CITY_ID]);

                        if (count($cityEntity) > 0) {

                            $workAreaEntity = $this->repositoryWorkArea->find($requestData[Constant::JSON_WORK_AREA][Constant::JSON_WORK_AREA_ID]);

                            if (count($workAreaEntity) > 0) {

                                /* web service de LASA para verificar si quies se esta intentando registrar  es trabajaor activo */
                                $lasaWorkerVerificator = $this->lasaWorkerVerificator($requestData[Constant::JSON_APP_USER_IDENTIFICATION_NUMBER]);

                                $appUserEntity = new AppUser();
                                $appUserStatisticsEntity = $this->registerInitialAppUserStatistics();
                                $levelEntity = $this->repositoryLevel->findOneBy(array(Constant::LEVEL_LEVEL_CODE => 0));

                                $appUserEntity->setName($requestData[Constant::JSON_APP_USER_NAME]);
                                $appUserEntity->setLastName($requestData[Constant::JSON_APP_USER_LAST_NAME]);
                                $appUserEntity->setEmail($requestData[Constant::JSON_APP_USER_EMAIL]);
                                $appUserEntity->setIdentificationNumber($requestData[Constant::JSON_APP_USER_IDENTIFICATION_NUMBER]);

                                if ($requestData[Constant::JSON_APP_USER_PASSWORD]) {
                                    $password = password_hash($requestData[Constant::JSON_APP_USER_PASSWORD], PASSWORD_DEFAULT);
                                } else {
                                    $password = password_hash($requestData[Constant::JSON_APP_USER_IDENTIFICATION_NUMBER], PASSWORD_DEFAULT);
                                }
                                $appUserEntity->setPassword($password);
                                $appUserEntity->setDeviceType($requestData[Constant::JSON_APP_USER_DEVICE_TYPE]);
                                $appUserEntity->setDeviceToken($requestData[Constant::JSON_APP_USER_DEVICE_TOKEN]);
                                $appUserEntity->setCityId($cityEntity);
                                $appUserEntity->setWorkAreaId($workAreaEntity);
                                $appUserEntity->setLevelId($levelEntity);
                                $appUserEntity->setAppUserStatisticsId($appUserStatisticsEntity);
                                ($lasaWorkerVerificator->isEmployee == true ? $appUserEntity->setIsLasaWorker(true) : $appUserEntity->setIsLasaWorker(false));
                                ($lasaWorkerVerificator->isEmployee == true ? $appUserEntity->setAccess(true) : $appUserEntity->setAccess(false));
                                $appUserEntity->setHaveFinalTestAccess(false);
                                $this->em->persist($appUserEntity);
                                $this->em->flush();

                                $appModules = $this->repositoryAppModule->findBy(array(Constant::APP_MODULE_APP_MODULE_LEVEL_ID => $levelEntity));
                                $appUserModulesArray = array();
                                foreach ($appModules as $appModule){
                                    $appModules = $this->container->get('api.AppModuleService')->registerAppUserModules($appUserEntity, $appModule);

                                }

                                $jwt = new jsonWebToken();
                                $jwt->setUserId($appUserEntity->getId());
                                $jwt->setStoreId($appUserEntity->getEmail());
                                $jwt->setDate($serverDate->getTimestamp());
                                $token = $jwt->tokenEncrypt();
                                $appUserEntity->setAccountId($token);
                                $this->em->persist($appUserEntity);
                                $this->em->flush();


                                $appUserData = $this->getAppUserData($appUserEntity, false);
                                $appUserStatics = $this->getAppUserStatistics($appUserEntity);

                                $appUserLevelResponse = array(
                                    Constant::JSON_LEVEL_LEVEL_ID => $appUserEntity->getLevelId()->getId(),
                                    Constant::JSON_LEVEL_LEVEL_DESCRIPTION => $appUserEntity->getLevelId()->getLevelDescription(),
                                    Constant::JSON_LEVEL_LEVEL_CODE => $appUserEntity->getLevelId()->getLevelCode(),
                                );

                                $availableAppModulesArray = $this->repositoryAppUserModule->findBy(array(Constant::APP_USER_MODULE_APP_USER_ID => $appUserEntity));
                                $appUserModulesArray = array();
                                foreach ($availableAppModulesArray as $appUserModule){
                                    $appUserModulesArray[] = array(
                                        Constant::JSON_APP_MODULE_APP_MODULE => [
                                            Constant::JSON_APP_MODULE_APP_MODULE_ID => $appUserModule->getAppModuleId()->getId(),
                                            Constant::JSON_APP_MODULE_APP_MODULE_NAME => $appUserModule->getAppModuleId()->getAppModuleName(),
                                            Constant::JSON_LEVEL => [
                                                Constant::JSON_LEVEL_LEVEL_ID => $appUserModule->getAppModuleId()->getLevelId()->getId(),
                                                Constant::JSON_LEVEL_LEVEL_DESCRIPTION => $appUserModule->getAppModuleId()->getLevelId()->getLevelDescription(),
                                                Constant::JSON_LEVEL_LEVEL_CODE => $appUserModule->getAppModuleId()->getLevelId()->getLevelCode(),
                                            ]
                                        ],
                                        Constant::JSON_APP_USER_MODULE_SCORE => $appUserModule->getScore(),
                                        Constant::JSON_APP_USER_MODULE_MAX_SCORE => $appUserModule->getMaxScore(),
                                    );
                                }

                                //Enviar Email al usuario
                                if ($appUserEntity->getAccess()) {
                                    $transport = \Swift_SmtpTransport::newInstance()
                                        ->setHost('smtp.office365.com')//exchange es el provedor del servicio de correo
                                        ->setPort('587')
                                        ->setEncryption('tls')
                                        ->setUsername('sistemas@lasa.com.co')
                                        ->setPassword('@ustr@li@.15*');

                                    $mailer = \Swift_Mailer::newInstance($transport);

                                    $message = \Swift_Message::newInstance()
                                        ->setSubject('Confirmación de Registro App LASA')
                                        ->setFrom('noreply@lasa.com.co')
                                        ->setTo($appUserEntity->getEmail())
                                        ->setContentType('text/html')
                                        ->setBody($this->container->get('templating')->render('default/emailTemplate.html.twig', array('user' => $appUserEntity->getEmail(), 'password' =>
                                            $requestData[Constant::JSON_APP_USER_IDENTIFICATION_NUMBER], 'typeEmail' => 3, 'name' => $appUserEntity->getName())), 'text/html');

                                    $mailer->send($message);
                                }
                                $appUserAchievements = $this->container->get('api.AchievementService')->getAppUserAchievements($appUserEntity);
                                $jsonResponse = array(
                                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                                    Constant::JSON_DATE_SERVER_CURRENT_DATE => $serverDate->getTimestamp(),
                                    Constant::JSON_APP_USER_DATA => $appUserData,
                                    Constant::JSON_APP_USER_STATISTICS => $appUserStatics,
                                    Constant::JSON_APP_USER_LEVEL => $appUserLevelResponse,
                                    Constant::JSON_APP_USER_ACHIEVEMENTS_ARRAY => $appUserAchievements,
                                    Constant::JSON_APP_USER_MODULE_APP_MODULES_PROGRESS_ARRAY => $appUserModulesArray
                                );

                            } else {
                                $jsonResponse = array(
                                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_REQUESTED_WORK_AREA_NOT_FOUND
                                );
                            }

                        } else {
                            $jsonResponse = array(
                                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_REQUESTED_CITY_NOT_FOUND
                            );
                        }

                    } else {
                        $jsonResponse = array(
                            Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                            Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_EMAIL_IS_ALREADY_IN_USE
                        );
                    }

                } else {
                    $jsonResponse = array(
                        Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                        Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_IDENTIFICATION_NUMBER_ALREADY_EXIST
                    );
                }

            } else {
                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_INVALID_EMAIL_FORMAT
                );
            }

        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_PARAMETERS
            );
        }
        return $jsonResponse;
    }

    private function registerInitialAppUserStatistics()
    {
        $severDate = new \DateTime();
        $appUserStatisticsEntity = new AppUserStatistics();

        $appUserStatisticsEntity->setChallengesWon(0);
        $appUserStatisticsEntity->setChallengesLost(0);
        $appUserStatisticsEntity->setLastChallengeAt(0);
        $appUserStatisticsEntity->setLoseStreak(0);
        $appUserStatisticsEntity->setWinStreak(0);
        $appUserStatisticsEntity->setLevelsUnlocked(1);
        $appUserStatisticsEntity->setLastPracticeAt(0);
        $appUserStatisticsEntity->setAchievementsUnlocked(0);
        $appUserStatisticsEntity->setTimePlayed(0);
        $appUserStatisticsEntity->setTotalChallengesPlayed(0);
        $appUserStatisticsEntity->setFinalTestSpendTime(0);
        $appUserStatisticsEntity->setLastUpdateDecreaseProgressAt($severDate->getTimestamp());
        $appUserStatisticsEntity->setMinigamesTotalScore(0);
        $this->em->persist($appUserStatisticsEntity);
        $this->em->flush();

        return $appUserStatisticsEntity;

    }

    private function lasaWorkerVerificator($identificationNumber)
    {
        $serviceUrl = 'http://cio.lasa.com.co/WebServices/LASAVirtual/GetPersonInfo';
        $curl = curl_init($serviceUrl);
        $curl_post_data = array(
            Constant::JSON_APP_USER_IDENTIFICATION_NUMBER => $identificationNumber
        );

        $encodedData = json_encode($curl_post_data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $encodedData);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json'));
        $curl_response = curl_exec($curl);

        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }

        curl_close($curl);
        $decoded = json_decode($curl_response);
        if (isset($decoded->response->status) && $decoded->response->status == 'ERROR') {
            die('error occured: ' . $decoded->response->errormessage);
        }
           $jsonResponse = $decoded;

        return $jsonResponse;

    }

    public function getAppUserData($appUserId, $isAdminRequest)
    {
        $appUserEntity = $this->repositoryAppUser->find($appUserId);

        if (count($appUserEntity) > 0) {
            $request = $this->container->get('request'); 
            $userImage = ($appUserEntity->getUserImage() == null 
                    ? null 
                    : ($request->getScheme() . '://' . $request->getHttpHost().$request->getBasePath().'/'.$appUserEntity->getUserImage()->getWebPath()));

            if ($isAdminRequest == true) {
                $finalTestResults = array();
                foreach ($appUserEntity->getFinalTestResults() as $result){
                    $levelEntity = $this->repositoryLevel->find($result->getLevelId());
                    array_push($finalTestResults, array(
                        Constant::JSON_LEVEL => array(
                            Constant::JSON_LEVEL_LEVEL_ID => $levelEntity->getId(),
                            Constant::JSON_LEVEL_LEVEL_DESCRIPTION => $levelEntity->getLevelDescription(),
                            Constant::JSON_LEVEL_LEVEL_CODE => $levelEntity->getLevelCode()
                        ),
                        Constant::JSON_FINAL_TEST_RESULT_RIGHT_ANSWERS => $result->getRightAnswers(),
                        Constant::JSON_FINAL_TEST_RESULT_WRONG_ANSWERS => $result->getWrongAnswers()
                    ));
                }

                $jsonResponse = array(
                    Constant::JSON_APP_USER_ID => $appUserEntity->getId(),
                    Constant::JSON_APP_USER_NAME => $appUserEntity->getName(),
                    Constant::JSON_APP_USER_LAST_NAME =>$appUserEntity->getLastName(),
                    Constant::JSON_APP_USER_IMAGE => $userImage,
                    Constant::JSON_APP_USER_EMAIL => $appUserEntity->getEmail(),
                    Constant::JSON_APP_USER_IDENTIFICATION_NUMBER => $appUserEntity->getIdentificationNumber(),
                    Constant::JSON_APP_USER_IS_LASA_WORKER => $appUserEntity->getIsLasaWorker(),
                    Constant::JSON_APP_USER_HAVE_FINAL_TEST_ACCESS => $appUserEntity->getHaveFinalTestAccess(),
                    Constant::JSON_APP_USER_ACCESS => $appUserEntity->getAccess(),
                    Constant::JSON_APP_USER_STATISTICS_CHALLENGES_WON => $appUserEntity->getAppUserStatisticsId()->getChallengesWon(),
                    Constant::JSON_LEVEL =>  $appUserEntity->getLevelId()->getLevelCode(),
                    Constant::JSON_FINAL_TEST_RESULT => $finalTestResults
                );

            } else {
                $jsonResponse = array(
                    Constant::JSON_APP_USER_ID => $appUserEntity->getId(),
                    Constant::JSON_APP_USER_NAME => $appUserEntity->getName(),
                    Constant::JSON_APP_USER_LAST_NAME => $appUserEntity->getLastName(),
                    Constant::JSON_APP_USER_IMAGE => $userImage,
                    Constant::JSON_APP_USER_EMAIL => $appUserEntity->getEmail(),
                    Constant::JSON_APP_USER_IDENTIFICATION_NUMBER => $appUserEntity->getIdentificationNumber(),
                    Constant::JSON_APP_USER_ACCOUNT_ID => $appUserEntity->getAccountId(),
                    Constant::JSON_APP_USER_IS_LASA_WORKER => $appUserEntity->getIsLasaWorker(),
                    Constant::JSON_APP_USER_HAVE_FINAL_TEST_ACCESS => $appUserEntity->getHaveFinalTestAccess(),
                );
            }

        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_USER_DOES_NOT_EXIST
            );
        }
        return $jsonResponse;
    }

    public function getAppUserStatistics($appUserId)
    {
        $appUserEntity = $this->repositoryAppUser->find($appUserId);
        if (count($appUserEntity) > 0) {

            $jsonResponse = array(
                Constant::JSON_APP_USER_STATISTICS_CHALLENGES_WON => $appUserEntity->getAppUserStatisticsId()->getChallengesWon(),
                Constant::JSON_APP_USER_STATISTICS_CHALLENGES_LOST => $appUserEntity->getAppUserStatisticsId()->getChallengesLost(),
                Constant::JSON_APP_USER_STATISTICS_TOTAL_CHALLENGES_PLAYED => $appUserEntity->getAppUserStatisticsId()->getTotalChallengesPlayed(),
                Constant::JSON_APP_USER_STATISTICS_LAST_CHALLENGE_AT  => $appUserEntity->getAppUserStatisticsId()->getLastChallengeAt(),
                Constant::JSON_APP_USER_STATISTICS_WIN_STREAK => $appUserEntity->getAppUserStatisticsId()->getWinStreak(),
                Constant::JSON_APP_USER_STATISTICS_LOSE_STREAK => $appUserEntity->getAppUserStatisticsId()->getLoseStreak(),
                Constant::JSON_APP_USER_STATISTICS_LEVELS_UNLOCKED => $appUserEntity->getAppUserStatisticsId()->getLevelsUnlocked(),
                Constant::JSON_APP_USER_STATISTICS_TIME_PLAYED => $this->secondsToHoursMinutesSeconds($appUserEntity->getAppUserStatisticsId()->getTimePlayed()),
                Constant::JSON_APP_USER_STATISTICS_FINAL_TEST_SPEND_TIME => $appUserEntity->getAppUserStatisticsId()->getFinalTestSpendTime(),
                Constant::JSON_APP_USER_STATISTICS_ACHIEVEMENTS_UNLOCKED => $appUserEntity->getAppUserStatisticsId()->getAchievementsUnlocked(),
                Constant::JSON_APP_USER_STATISTICS_MINI_GAMES_TOTAL_SCORE => $appUserEntity->getAppUserStatisticsId()->getMinigamesTotalScore(),
        );

        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_USER_DOES_NOT_EXIST
            );
        }
        return $jsonResponse;
    }

    public function loginAppUser(Request $request)
    {
        $requestData = array(
            Constant::JSON_APP_USER_EMAIL => $request->request->get(Constant::JSON_APP_USER_EMAIL),
            Constant::JSON_APP_USER_PASSWORD => $request->request->get(Constant::JSON_APP_USER_PASSWORD),
            Constant::JSON_APP_USER_DEVICE_TYPE => $request->request->get(Constant::JSON_APP_USER_DEVICE_TYPE),
            Constant::JSON_APP_USER_DEVICE_TOKEN => $request->request->get(Constant::JSON_APP_USER_DEVICE_TOKEN),
        );
        $jsonResponse = array();

        if ($requestData[Constant::JSON_APP_USER_EMAIL] && $requestData[Constant::JSON_APP_USER_PASSWORD] && $requestData[Constant::JSON_APP_USER_DEVICE_TYPE] &&
            $requestData[Constant::JSON_APP_USER_DEVICE_TOKEN]) {

            $emailValidator = $this->container->get('api.AdminService')->emailVerification($requestData[Constant::JSON_APP_USER_EMAIL]);

            if ($emailValidator == true) {

                $appUserEntity = $this->repositoryAppUser->findOneBy(array(Constant::APP_USER_EMAIL => $requestData[Constant::JSON_APP_USER_EMAIL]));

                if (count($appUserEntity) > 0) {

                    if (password_verify($requestData[Constant::JSON_APP_USER_PASSWORD], $appUserEntity->getPassword())) {
                        if ($appUserEntity->getAccess()){

                            /* las siguentes  cuatro lineas se encarga de guardar y/o actualizar los identficadores del dispositivo par el uso de las notificaiones push */
                            $appUserEntity->setDeviceType($requestData[Constant::JSON_APP_USER_DEVICE_TYPE]);
                            $appUserEntity->setDeviceToken($requestData[Constant::JSON_APP_USER_DEVICE_TOKEN]);
                            $this->em->persist($appUserEntity);
                            $this->em->flush();


                            $appUserData = $this->getAppUserData($appUserEntity, false);
                            $appUserStatistics = $this->getAppUserStatistics($appUserEntity);
                            $availableAppModules = $this->container->get('api.AppModuleService')->getAvailableAppUserModules($appUserEntity);
                            $appUserAchievements = $this->container->get('api.AchievementService')->getAppUserAchievements($appUserEntity);

                            $serverDate = new \DateTime();
                            $appUserLevelResponse = array(
                                Constant::JSON_LEVEL_LEVEL_ID => $appUserEntity->getLevelId()->getId(),
                                Constant::JSON_LEVEL_LEVEL_DESCRIPTION => $appUserEntity->getLevelId()->getLevelDescription(),
                                Constant::JSON_LEVEL_LEVEL_CODE => $appUserEntity->getLevelId()->getLevelCode(),
                            );

                            $jsonResponse = array(
                                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                                Constant::JSON_DATE_SERVER_CURRENT_DATE => $serverDate->getTimestamp(),
                                Constant::JSON_APP_USER_DATA => $appUserData,
                                Constant::JSON_APP_USER_STATISTICS => $appUserStatistics,
                                Constant::JSON_APP_USER_LEVEL => $appUserLevelResponse,
                                Constant::JSON_APP_USER_ACHIEVEMENTS_ARRAY => $appUserAchievements,
                                Constant::JSON_APP_USER_MODULE_APP_MODULES_PROGRESS_ARRAY => $availableAppModules
                            );
                        }else{
                            $jsonResponse = array(
                                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_ACCESS_DENIED,
                                Constant::SPANISH_MESSAGE_RESPONSE_MESSAGE => Constant::SPANISH_MESSAGE_ACCESS_DENIED
                            );
                        }
                    } else {
                        $jsonResponse = array(
                            Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                            Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_EMAIL_OR_PASSWORD_DOES_NOT_MATCH
                        );
                    }

                } else {
                    $jsonResponse = array(
                        Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                        Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_EMAIL_OR_PASSWORD_DOES_NOT_MATCH
                    );
                }

            } else {
                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_INVALID_EMAIL_FORMAT
                );
            }

        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_PARAMETERS
            );
        }
        return $jsonResponse;

    }

    public function recoveryUserToken(Request $request){
        $jsonResponse = array();
        $requestData = array(
            Constant::JSON_ADMIN_USER_EMAIL => $request->request->get(Constant::JSON_APP_USER_EMAIL)
        );

        $emailValidator = $this->container->get('api.AdminService')->emailVerification($requestData[Constant::JSON_APP_USER_EMAIL]);

        if ($emailValidator == true) {

            $dataUser = $this->repositoryAppUser->findOneBy(array(Constant::APP_USER_EMAIL => $requestData[Constant::JSON_APP_USER_EMAIL]));

            if(count($dataUser) > 0){
                $token = bin2hex(random_bytes(16));
                $updateDate = new \DateTime();

                $dataUser->setRecoverPasswordToken($token);
                $dataUser->setRecoverPasswordTokenExpiresAt($updateDate->modify('+ 1 day'));
                $this->em->persist($dataUser);
                $this->em->flush();

                //Enviar Email al usuario
                $transport = \Swift_SmtpTransport::newInstance()
                    ->setHost('smtp.office365.com')//exchange es el provedor del servicio de correo
                    ->setPort('587')
                    ->setEncryption('tls')
                    ->setUsername('sistemas@lasa.com.co')
                    ->setPassword('@ustr@li@.15*');

                $mailer = \Swift_Mailer::newInstance($transport);

                $message = \Swift_Message::newInstance()
                    ->setSubject('Recuperar contraseña Virtual Training LASA')
                    ->setFrom('noreply@lasa.com.co')
                    ->setTo($dataUser->getEmail())
                    ->setContentType('text/html')
                    ->setBody($this->container->get('templating')->render('default/emailTemplate.html.twig', array('userToken' => $token,'isAdmin' => 0, 'typeEmail' => 1)), 'text/html');

                $mailer->send($message);

                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_EMAIL_SENT_SUCCESSFULY
                );
            }else{
                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_USER_DOES_NOT_EXIST
                );
            }
        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_INVALID_EMAIL_FORMAT
            );
        }

        return $jsonResponse;
    }

    /*
     * Funcion para obtener el tiempo total jugado, en formato H:i:s
     * */
    public function secondsToHoursMinutesSeconds($timeInSeconds) {

        $hours = floor($timeInSeconds / 3600);
        if ($hours < 10) {
            $realHours = '0'.$hours;
        } else{
            $realHours = $hours;
        }

        $minutes = floor(($timeInSeconds - ($hours * 3600)) / 60);
        if ($minutes < 10) {
            $realMinutes = '0'.$minutes;
        } else {
            $realMinutes = $minutes;
        }

        $seconds = $timeInSeconds - ($hours * 3600) - ($minutes * 60);
        if ($seconds < 10) {
            $realSeconds = '0'.$seconds;
        } else {
            $realSeconds = $seconds;
        }

        return $realHours . ':' . $realMinutes . ":" . $realSeconds;
    }

    /* función para cms*/
    public function getAllAppUsers()
    {
        $appUsersEntities = $this->repositoryAppUser->findby(array(), array(Constant::ID => 'DESC'));
        $jsonResponse = array();

        if (count($appUsersEntities) > 0 ) {

            $appUsersArray = array();
            foreach ($appUsersEntities as $appUsersEntity){

                $appUsersArray[] = $this->getAppUserData($appUsersEntity, true);
            }
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                Constant::JSON_APP_USERS_ARRAY => $appUsersArray,
            );
        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_NO_USERS_FOUND,
                Constant::SPANISH_MESSAGE_RESPONSE_MESSAGE => Constant::SPANISH_MESSAGE_NO_USERS_FOUND,
            );
        }
        return $jsonResponse;
    }

    public function getAllAppUserDetailInfo(Request $request)
    {
        $requestData = array(
            Constant::JSON_APP_USER_ID => $request->request->get(Constant::JSON_APP_USER_ID),
            Constant::JSON_APP_USER_NAME => $request->request->get(Constant::JSON_APP_USER_NAME),
            Constant::JSON_APP_USER_LAST_NAME => $request->request->get(Constant::JSON_APP_USER_LAST_NAME),
            Constant::JSON_APP_USER_EMAIL => $request->request->get(Constant::JSON_APP_USER_EMAIL),
            Constant::JSON_APP_USER_IDENTIFICATION_NUMBER => $request->request->get(Constant::JSON_APP_USER_IDENTIFICATION_NUMBER),
            Constant::JSON_APP_USER_IS_LASA_WORKER => $request->request->get(Constant::JSON_APP_USER_IS_LASA_WORKER),
            Constant::JSON_APP_USER_HAVE_FINAL_TEST_ACCESS => $request->request->get(Constant::JSON_APP_USER_HAVE_FINAL_TEST_ACCESS),

        );
        $jsonResponse = array();

        if (is_numeric($requestData[Constant::JSON_APP_USER_ID])){

            $appUserEntity = $this->repositoryAppUser->findOneBy(array(Constant::ID => $requestData[Constant::JSON_APP_USER_ID]));

            if (count($appUserEntity) > 0) {

                $appUserData = $this->getAppUserData($appUserEntity, true);
                $appUserStatistics = $this->getAppUserStatistics($appUserEntity);
                $availableAppModules = $this->container->get('api.AppModuleService')->getAvailableAppUserModules($appUserEntity);
                $appUserLevelResponse = array(
                    Constant::JSON_LEVEL_LEVEL_ID => $appUserEntity->getLevelId()->getId(),
                    Constant::JSON_LEVEL_LEVEL_DESCRIPTION => $appUserEntity->getLevelId()->getLevelDescription(),
                    Constant::JSON_LEVEL_LEVEL_CODE => $appUserEntity->getLevelId()->getLevelCode(),
                );

                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                    Constant::JSON_APP_USER_DATA => $appUserData,
                    Constant::JSON_APP_USER_STATISTICS => $appUserStatistics,
                    Constant::JSON_APP_USER_LEVEL => $appUserLevelResponse,
                    Constant::JSON_APP_USER_MODULE_APP_MODULES_PROGRESS_ARRAY => $availableAppModules,
                );

            } else {
                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_USER_DOES_NOT_EXIST,
                    Constant::SPANISH_MESSAGE_RESPONSE_MESSAGE => Constant::SPANISH_MESSAGE_USER_DOES_NOT_EXIST
                );
            }

        } else {

            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_PARAMETERS
            );
        }
        return $jsonResponse;

    }

    public function enableOrDisableFinalTestAccess(Request $request)
    {
        $requestData = array(
            Constant::JSON_APP_USER_DATA => $request->request->get(Constant::JSON_APP_USER_DATA),
            Constant::JSON_APP_USER_ENABLE_FINAL_TEST => $request->request->get(Constant::JSON_APP_USER_ENABLE_FINAL_TEST)
        );
        $jsonRsponse = array();

        if(is_numeric($requestData[Constant::JSON_APP_USER_DATA][Constant::JSON_APP_USER_ID]) && is_bool($requestData[Constant::JSON_APP_USER_ENABLE_FINAL_TEST])){

            $appUserEntity = $this->repositoryAppUser->findOneBy(array(Constant::ID => $requestData[Constant::JSON_APP_USER_DATA][Constant::JSON_APP_USER_ID]));

            if (count($appUserEntity) > 0) {


                $requestData[Constant::JSON_APP_USER_ENABLE_FINAL_TEST ] == true ? $appUserEntity->setHaveFinalTestAccess(true) : $appUserEntity->setHaveFinalTestAccess(false);

                $this->em->persist($appUserEntity);
                $this->em->flush($appUserEntity);

                $appUserData = $this->getAppUserData($appUserEntity, true);
                $appUserStatistics = $this->getAppUserStatistics($appUserEntity);
                $availableAppModules = $this->container->get('api.AppModuleService')->getAvailableAppUserModules($appUserEntity);
                $appUserLevelResponse = array(
                    Constant::JSON_LEVEL_LEVEL_ID => $appUserEntity->getLevelId()->getId(),
                    Constant::JSON_LEVEL_LEVEL_DESCRIPTION => $appUserEntity->getLevelId()->getLevelDescription(),
                    Constant::JSON_LEVEL_LEVEL_CODE => $appUserEntity->getLevelId()->getLevelCode(),
                );

                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                    Constant::JSON_APP_USER_DATA => $appUserData,
                    Constant::JSON_APP_USER_STATISTICS => $appUserStatistics,
                    Constant::JSON_APP_USER_LEVEL => $appUserLevelResponse,
                    Constant::JSON_APP_USER_MODULE_APP_MODULES_PROGRESS_ARRAY => $availableAppModules,
                );

            } else {
                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_USER_DOES_NOT_EXIST,
                    Constant::SPANISH_MESSAGE_RESPONSE_MESSAGE => Constant::SPANISH_MESSAGE_USER_DOES_NOT_EXIST
                );
            }

        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_PARAMETERS
            );
        }
        return $jsonResponse;
    }

    public function sendCustomEmail(Request $request)
    {
        $requestData = array(
            Constant::JSON_EMAIL_EMAIL_SUBJECT => $request->request->get(Constant::JSON_EMAIL_EMAIL_SUBJECT),
            Constant::JSON_EMAIL_EMAIL_BODY => $request->request->get(Constant::JSON_EMAIL_EMAIL_BODY),
            Constant::JSON_APP_USER_EMAIL => $request->request->get(Constant::JSON_APP_USER_EMAIL)
        );

        if ($requestData[Constant::JSON_APP_USER_EMAIL] && $requestData[Constant::JSON_EMAIL_EMAIL_SUBJECT] && $requestData[Constant::JSON_EMAIL_EMAIL_BODY]) {

            $emailValidator = $this->container->get('api.AdminService')->emailVerification($requestData[Constant::JSON_APP_USER_EMAIL]);

            if ($emailValidator == true) {

                //Enviar Email al usuario
                $transport = \Swift_SmtpTransport::newInstance()
                    ->setHost('smtp.office365.com')//exchange es el provedor del servicio de correo
                    ->setPort('587')
                    ->setEncryption('tls')
                    ->setUsername('sistemas@lasa.com.co')
                    ->setPassword('@ustr@li@.15*');

                $mailer = \Swift_Mailer::newInstance($transport);

                $message = \Swift_Message::newInstance()
                    ->setSubject($requestData[Constant::JSON_EMAIL_EMAIL_SUBJECT])
                    ->setFrom('noreply@lasa.com.co')
                    ->setTo($requestData[Constant::JSON_APP_USER_EMAIL])
                    ->setContentType('text/html')
                    ->setBody($this->container->get('templating')->render('default/emailTemplate.html.twig', array('body' => $requestData[Constant::JSON_EMAIL_EMAIL_BODY], 'typeEmail' => 4)), 'text/html');

                $mailer->send($message);

                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_EMAIL_SENT_SUCCESSFULY
                );

            } else {
                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_INVALID_EMAIL_FORMAT
                );
            }

        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_PARAMETERS
            );
        }
        return $jsonResponse;
    }
    
    //Funcion para registrar y modificar imagen del usuario
    public function userImage(Request $request) {
        $jsonResponse = array();
        $dataRequest = array(Constant::JSON_APP_USER_IMAGE => $request->files->get(Constant::JSON_APP_USER_IMAGE));
        $authorizationApiToken = $request->headers->get('Authorization');
        if ($authorizationApiToken != null) {
            $jwt = new jsonWebToken();
            $jwt->setToken($authorizationApiToken);
            if ($jwt->tokenDecrypt() > 0) {
                if ($dataRequest[Constant::JSON_APP_USER_IMAGE] != null) {
                    $userId = $jwt->tokenDecrypt();
                    $userData = $this->repositoryAppUser->find($userId);
                    if (count($userData) > 0) {
                        $serverDate = new \DateTime();
                        $file = $dataRequest[Constant::JSON_APP_USER_IMAGE];
                        if (count($userData->getUserImage()) > 0) {
                            $userData->getUserImage()->setAppUserId($userData);
                            $userData->getUserImage()->setFile($file);
                            $this->em->persist($userData->getUserImage());
                            $this->em->flush();
                            $jsonResponse = array(
                                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                                Constant::JSON_DATE_SERVER_CURRENT_DATE => $serverDate->getTimestamp(),
                                Constant::JSON_APP_USER_DATA => $this->getAppUserData($userData, false),
                                Constant::JSON_APP_USER_STATISTICS => $this->getAppUserStatistics($userData),
                                Constant::JSON_APP_USER_LEVEL => array(
                                    Constant::JSON_LEVEL_LEVEL_ID => $userData->getLevelId()->getId(),
                                    Constant::JSON_LEVEL_LEVEL_DESCRIPTION => $userData->getLevelId()->getLevelDescription(),
                                    Constant::JSON_LEVEL_LEVEL_CODE => $userData->getLevelId()->getLevelCode(),
                                ),
                                Constant::JSON_APP_USER_ACHIEVEMENTS_ARRAY => $this->container->get('api.AchievementService')->getAppUserAchievements($userData),
                                Constant::JSON_APP_USER_MODULE_APP_MODULES_PROGRESS_ARRAY => $this->container->get('api.AppModuleService')->getAvailableAppUserModules($userData)
                            );
                        } else {
                            $userImage = new UserImage();
                            $userImage->setAppUserId($userData);
                            $userImage->setFile($file);
                            $this->em->persist($userImage);
                            $this->em->flush();
                            $serverUrl = $request->getScheme() . '://' . $request->getHttpHost().$request->getBasePath();
                            
                            //Remplzarel valor de la array que devuelve la funcion por el valor actualizado de "userImage"
                            $userDataArray = array_replace(
                                    $this->getAppUserData($userData, false), array(Constant::JSON_APP_USER_IMAGE => $serverUrl.'/'.$userImage->getWebPath())
                            );
                            $jsonResponse = array(
                                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                                Constant::JSON_DATE_SERVER_CURRENT_DATE => $serverDate->getTimestamp(),
                                Constant::JSON_APP_USER_DATA => $userDataArray,
                                Constant::JSON_APP_USER_STATISTICS => $this->getAppUserStatistics($userData),
                                Constant::JSON_APP_USER_LEVEL => array(
                                    Constant::JSON_LEVEL_LEVEL_ID => $userData->getLevelId()->getId(),
                                    Constant::JSON_LEVEL_LEVEL_DESCRIPTION => $userData->getLevelId()->getLevelDescription(),
                                    Constant::JSON_LEVEL_LEVEL_CODE => $userData->getLevelId()->getLevelCode(),
                                ),
                                Constant::JSON_APP_USER_ACHIEVEMENTS_ARRAY => $this->container->get('api.AchievementService')->getAppUserAchievements($userData),
                                Constant::JSON_APP_USER_MODULE_APP_MODULES_PROGRESS_ARRAY => $this->container->get('api.AppModuleService')->getAvailableAppUserModules($userData)
                            );
                        }
                    } else {
                        $jsonResponse = array(
                            Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                            Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_USER_DOES_NOT_EXIST
                        );
                    }
                } else {
                    $jsonResponse = array(
                        Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                        Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_PARAMETERS
                    );
                }
            } else {
                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_AUTHORIZATION
                );
            }
        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_AUTHORIZATION
            );
        }
        return $jsonResponse;
    }

    //Funcion para el CMS
    public function denyOrGrantAccess(Request $request)
    {
        $jsonResponse = array();
        $requestData = array(
            Constant::JSON_APP_USER_ID => $request->request->get(Constant::JSON_APP_USER_ID),
            Constant::JSON_APP_USER_ACCESS => $request->request->get(Constant::JSON_APP_USER_ACCESS)
        );
        if (is_bool($requestData[Constant::JSON_APP_USER_ACCESS]) && $requestData[Constant::JSON_APP_USER_ID]) {
            $appUsersEntities = $this->repositoryAppUser->find($requestData[Constant::JSON_APP_USER_ID]);
            if (count($appUsersEntities) > 0) {
                $appUsersEntities->setAccess($requestData[Constant::JSON_APP_USER_ACCESS]);
                $this->em->persist($appUsersEntities);
                $this->em->flush();

                if ($requestData[Constant::JSON_APP_USER_ACCESS] == true){
                    $token = bin2hex(random_bytes(16));
                    $updateDate = new \DateTime();

                    $appUsersEntities->setRecoverPasswordToken($token);
                    $appUsersEntities->setRecoverPasswordTokenExpiresAt($updateDate->modify('+ 1 day'));
                    $this->em->persist($appUsersEntities);
                    $this->em->flush();

                    //Enviar Email al usuario
                    $transport = \Swift_SmtpTransport::newInstance()
                        ->setHost('smtp.office365.com')//exchange es el provedor del servicio de correo
                        ->setPort('587')
                        ->setEncryption('tls')
                        ->setUsername('sistemas@lasa.com.co')
                        ->setPassword('@ustr@li@.15*');

                    $mailer = \Swift_Mailer::newInstance($transport);

                    $message = \Swift_Message::newInstance()
                        ->setSubject('Habilitacion de acceso Virtual Training LASA')
                        ->setFrom('noreply@lasa.com.co')
                        ->setTo($appUsersEntities->getEmail())
                        ->setContentType('text/html')
                        ->setBody($this->container->get('templating')->render('default/emailTemplate.html.twig', array('userToken' => $token,'isAdmin' => 0, 'typeEmail' => 5)), 'text/html');

                    $mailer->send($message);
                }

                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_TRUE,
                    Constant::JSON_APP_USER_DATA => $this->getAppUserData($appUsersEntities, true),
                    Constant::JSON_APP_USER_STATISTICS => $this->getAppUserStatistics($appUsersEntities),
                    Constant::JSON_APP_USER_LEVEL => array(
                        Constant::JSON_LEVEL_LEVEL_ID => $appUsersEntities->getLevelId()->getId(),
                        Constant::JSON_LEVEL_LEVEL_DESCRIPTION => $appUsersEntities->getLevelId()->getLevelDescription(),
                        Constant::JSON_LEVEL_LEVEL_CODE => $appUsersEntities->getLevelId()->getLevelCode(),
                    ),
                    Constant::JSON_APP_USER_MODULE_APP_MODULES_PROGRESS_ARRAY => $this->container->get('api.AppModuleService')->getAvailableAppUserModules($appUsersEntities),
                );
            } else {
                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_USER_DOES_NOT_EXIST,
                    Constant::SPANISH_MESSAGE_RESPONSE_MESSAGE => Constant::SPANISH_MESSAGE_USER_DOES_NOT_EXIST
                );
            }
        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_PARAMETERS,
                Constant::SPANISH_MESSAGE_RESPONSE_MESSAGE => Constant::SPANISH_MESSAGE_MISSING_PARAMETERS
            );
        }
        return $jsonResponse;
    }

}