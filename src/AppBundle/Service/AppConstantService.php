<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 1/02/17
 * Time: 7:48 PM
 */

namespace AppBundle\Service;

use AppBundle\Entity\AppUser;
use AppBundle\Entity\AppUserModule;
use AppBundle\Entity\AppUserStatistics;
use AppBundle\Utility\jsonWebToken;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;
use AppBundle\Constant\Constant;
use Symfony\Component\Validator\Tests\Fixtures\ConstraintA;

class AppConstantService
{
    private $repositoryAppConstant;
    private $container;
    private $em;

    public function __construct(EntityManager $entityManager, ContainerInterface $container)
    {
        $this->em = $entityManager;
        $this->container = $container;
        $this->repositoryAppConstant = $this->em->getRepository(Constant::ENTITY_APP_CONSTANT);
    }

    public function getAllAppHelpfulConstant()
    {
        $appConstantEntities = $this->repositoryAppConstant->findBy(array());

        if(count($appConstantEntities) > 0){
            $helpfulConstantsArray = array();
            foreach ($appConstantEntities as $appConstantEntity){
                $helpfulConstantsArray[] = array(
                    Constant::JSON_APP_CONSTANT_APP_CONSTANT_NAME => $appConstantEntity->getAppConstantName(),
                    Constant::JSON_APP_CONSTANT_APP_CONSTANT_VALUE => $appConstantEntity->getAppConstantValue()
                );
            }

        } else {
            $helpfulConstantsArray = array();
        }

        return $helpfulConstantsArray;
    }
}