<?php

namespace AppBundle\Service;

use AppBundle\Entity\AppUser;
use AppBundle\Entity\AppUserModule;
use AppBundle\Entity\UserImage;
use AppBundle\Entity\AppUserStatistics;
use AppBundle\Utility\jsonWebToken;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;
use AppBundle\Constant\Constant;
use Symfony\Component\Validator\Tests\Fixtures\ConstraintA;

class RankingService {

    private $repositoryAdminModule;
    private $repositoryAdminUser;
    private $repositoryAnswer;
    private $repositoryAppModule;
    private $repositoryAppUser;
    private $repositoryAppUserStatistics;
    private $repositoryAppUserModule;
    private $repositoryCity;
    private $repositoryLevel;
    private $repositoryQuestion;
    private $repositoryTopic;
    private $repositoryUserAdminModule;
    private $repositoryWorkArea;
    private $repositoryUserImage;
    private $container;
    private $em;

    public function __construct(EntityManager $entityManager, ContainerInterface $container) {
        $this->em = $entityManager;
        $this->container = $container;
        $this->repositoryAdminModule = $this->em->getRepository(Constant::ENTITY_ADMIN_MODULE);
        $this->repositoryAdminUser = $this->em->getRepository(Constant::ENTITY_ADMIN_USER);
        $this->repositoryAnswer = $this->em->getRepository(Constant::ENTITY_ANSWER);
        $this->repositoryAppModule = $this->em->getRepository(Constant::ENTITY_APP_MODULE);
        $this->repositoryAppUser = $this->em->getRepository(Constant::ENTITY_APP_USER);
        $this->repositoryAppUserStatistics = $this->em->getRepository(Constant::ENTITY_APP_USER_STATISTICS);
        $this->repositoryAppUserModule = $this->em->getRepository(Constant::ENTITY_APP_USER_MODULE);
        $this->repositoryCity = $this->em->getRepository(Constant::ENTITY_CITY);
        $this->repositoryLevel = $this->em->getRepository(Constant::ENTITY_LEVEL);
        $this->repositoryQuestion = $this->em->getRepository(Constant::ENTITY_QUESTION);
        $this->repositoryTopic = $this->em->getRepository(Constant::ENTITY_TOPIC);
        $this->repositoryUserAdminModule = $this->em->getRepository(Constant::ENTITY_USER_ADMIN_MODULE);
        $this->repositoryWorkArea = $this->em->getRepository(Constant::ENTITY_WORK_AREA);
        $this->repositoryUserImage = $this->em->getRepository(Constant::ENTITY_USER_IMAGE);
    }

    public function getRanking(Request $request) {
        $jsonResponse = array();
        $authorizationApiToken = $request->headers->get('Authorization');
        if ($authorizationApiToken != null) {
            $jwt = new jsonWebToken();
            $jwt->setToken($authorizationApiToken);
            if ($jwt->tokenDecrypt() > 0) {
                $userId = $jwt->tokenDecrypt();
                $userData = $this->repositoryAppUser->find($userId);
                if (count($userData) > 0) {
                    //el criterio de ranking aqui abajo es el real, estimado en los requisitos, se basas en los duelos.
                    $usersStatistics = $this->repositoryAppUserStatistics->findBy(array(), array(Constant::APP_USER_STATISTICS_CHALLENGES_WON => 'DESC',
                        Constant::APP_USER_STATISTICS_CHALLENGES_LOST => 'ASC'));
                    //el criterio aqui abajo es que se habilitó para la version preliminar de lanzamiento. y se basa en el puntaje de los minijuegos.
                    /*$usersStatistics = $this->repositoryAppUserStatistics->findBy(array(), array(Constant::APP_USER_STATISTICS_MINI_GAMES_TOTAL_SCORE => 'DESC'));*/
                    if (count($usersStatistics) > 0) {
                        foreach ($usersStatistics as $key => $userStatistics) {
                            if ($key < Constant::RANKING_USERS_TOTAL) {
                                if ($userStatistics->getAppUser()->getAccess()){
                                    array_push($jsonResponse, array(
                                        Constant::JSON_APP_RANKING_USER_POSITION => $key + 1,
                                        Constant::JSON_APP_USER_DATA => array(
                                            Constant::JSON_APP_USER_ID => $userStatistics->getAppUser()->getId(),
                                            Constant::JSON_APP_USER_NAME => $userStatistics->getAppUser()->getName(),
                                            Constant::JSON_APP_USER_IMAGE => ($userStatistics->getAppUser()->getUserImage() == null ? null : ($request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . '/' . $userStatistics->getAppUser()->getUserImage()->getWebPath())),
                                            Constant::JSON_APP_USER_LAST_NAME => $userStatistics->getAppUser()->getLastName(),
                                            Constant::JSON_APP_USER_EMAIL => $userStatistics->getAppUser()->getEmail(),
                                            Constant::JSON_APP_USER_IDENTIFICATION_NUMBER => $userStatistics->getAppUser()->getIdentificationNumber(),
                                            Constant::JSON_APP_USER_ACCOUNT_ID => $userStatistics->getAppUser()->getAccountId(),
                                            Constant::JSON_APP_USER_IS_LASA_WORKER => $userStatistics->getAppUser()->getIsLasaWorker(),
                                            Constant::JSON_APP_USER_HAVE_FINAL_TEST_ACCESS => $userStatistics->getAppUser()->getHaveFinalTestAccess(),
                                        ),
                                        Constant::JSON_APP_USER_STATISTICS => array(
                                            Constant::JSON_APP_USER_STATISTICS_CHALLENGES_WON => $userStatistics->getChallengesWon(),
                                            Constant::JSON_APP_USER_STATISTICS_CHALLENGES_LOST => $userStatistics->getChallengesLost(),
                                            Constant::JSON_APP_USER_STATISTICS_TOTAL_CHALLENGES_PLAYED => $userStatistics->getTotalChallengesPlayed(),
                                            Constant::JSON_APP_USER_STATISTICS_LAST_CHALLENGE_AT => $userStatistics->getLastChallengeAt(),
                                            Constant::JSON_APP_USER_STATISTICS_WIN_STREAK => $userStatistics->getWinStreak(),
                                            Constant::JSON_APP_USER_STATISTICS_LOSE_STREAK => $userStatistics->getLoseStreak(),
                                            Constant::JSON_APP_USER_STATISTICS_LEVELS_UNLOCKED => $userStatistics->getLevelsUnlocked(),
                                            Constant::JSON_APP_USER_STATISTICS_FINAL_TEST_SPEND_TIME => $userStatistics->getFinalTestSpendTime(),
                                            Constant::JSON_APP_USER_STATISTICS_MINI_GAMES_TOTAL_SCORE => $userStatistics->getMinigamesTotalScore(),
                                        )
                                    ));
                                }
                            } else {
                                break;
                            }
                        }
                        $jsonResponse = array(Constant::JSON_APP_RANKING => $jsonResponse);
                    } else {
                        $jsonResponse = array(
                            Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                            Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_USER_DOES_NOT_EXIST
                        );
                    }
                } else {
                    $jsonResponse = array(
                        Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                        Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_USER_DOES_NOT_EXIST
                    );
                }
            } else {
                $jsonResponse = array(
                    Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                    Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_AUTHORIZATION
                );
            }
        } else {
            $jsonResponse = array(
                Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE,
                Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_MISSING_AUTHORIZATION
            );
        }

        return $jsonResponse;
    }

    public function getTotalScoreByMiniGames($userId)
    {
        $sql = 'SELECT SUM(score) as total FROM `app_user_module` WHERE app_user_id = :idUser and (app_module_id = 1 OR app_module_id = 5 OR app_module_id = 9 OR app_module_id = 13 OR
         app_module_id = 17 OR app_module_id = 21 OR app_module_id = 25 OR app_module_id = 29 OR app_module_id = 37)';
        $stm = $this->em->getConnection()->prepare($sql);
        $stm->bindParam('idUser', $userId);
        $stm->execute();
        $resulset = $stm->fetchAll();

        $total = 0;
        foreach ($resulset as $key){
            $total = $key;
        }
        return $total;
    }
}
