<?php

namespace AppBundle\Controller\Api;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use AppBundle\Constant\Constant;

/**
 * Admin API Controller.
 *
 * @Route("api/admin")
 */
class AdminController extends Controller
{
    /**
     * @ApiDoc(
     *  description="CMS User create"
     * )
     * @Route("/users/register", name="admin_api_user_register", defaults={"_format": "json"})
     * @Method({"POST"})
     * @param Request $request
     * @return $response
     */
    public function registerAdminUserAction(Request $request) {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.AdminService')->registerAdminUser($request);
            if ($dateService != null) {
                $jsonResponse = $dateService;
                $code = JsonResponse::HTTP_OK;
            } else {
                $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            }
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode($code);
            return $response;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_JSON_INVALID, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            return $response;
        }
    }

    /**
     * @ApiDoc(
     *  description="CMS User Login"
     * )
     * @Route("/login", name="admin_api_user_login", defaults={"_format": "json"})
     * @Method({"POST"})
     * @param Request $request
     * @return $response
     */
    public function loginAdminUserAction(Request $request) {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.AdminService')->loginAdminUser($request);
            if ($dateService != null) {
                $jsonResponse = $dateService;
                $code = JsonResponse::HTTP_OK;
            } else {
                $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            }
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode($code);
            return $response;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_JSON_INVALID, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            return $response;
        }
    }

    /**
     * @ApiDoc(
     *  description="CMS User Logout"
     * )
     * @Route("/logout", name="admin_api_user_logout", defaults={"_format": "json"})
     * @Method({"POST"})
     * @param Request $request
     * @return $response
     */
    public function logoutAdminUserAction(Request $request) {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.AdminService')->logoutAdminUser($request);
            if ($dateService != null) {
                $jsonResponse = $dateService;
                $code = JsonResponse::HTTP_OK;
            } else {
                $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            }
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode($code);
            return $response;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_JSON_INVALID, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            return $response;
        }
    }

    /**
     * @ApiDoc(
     *  description="CMS  Get All Admins"
     * )
     * @Route("/users", name="admin_api_users", defaults={"_format": "json"})
     * @Method({"GET"})
     * @param Request $request
     * @return $response
     */
    public function getAllAdminUsersAction(Request $request) {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.AdminService')->getAllAdminUsers($request);
            if ($dateService != null) {
                $jsonResponse = $dateService;
                $code = JsonResponse::HTTP_OK;
            } else {
                $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            }
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode($code);
            return $response;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_JSON_INVALID, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            return $response;
        }
    }

    /**
     * @ApiDoc(
     *  description="CMS  Edit Admin Data"
     * )
     * @Route("/users/edit", name="admin_api_user_edit", defaults={"_format": "json"})
     * @Method({"POST"})
     * @param Request $request
     * @return $response
     */
    public function editAdminUserAction(Request $request) {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.AdminService')->editAdminUser($request);
            if ($dateService != null) {
                $jsonResponse = $dateService;
                $code = JsonResponse::HTTP_OK;
            } else {
                $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            }
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode($code);
            return $response;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_JSON_INVALID, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            return $response;
        }
    }

    /**
     * @ApiDoc(
     *  description="CMS Delete All Admin Data"
     * )
     * @Route("/users/delete", name="admin_api_user_delete", defaults={"_format": "json"})
     * @Method({"POST"})
     * @param Request $request
     * @return $response
     */
    public function deleteUserAdminAction(Request $request) {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.AdminService')->deleteUserAdmin($request);
            if ($dateService != null) {
                $jsonResponse = $dateService;
                $code = JsonResponse::HTTP_OK;
            } else {
                $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            }
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode($code);
            return $response;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_JSON_INVALID, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            return $response;
        }
    }

    /**
     * @ApiDoc(
     *  description="Question create"
     * )
     * @Route("/questions/create", name="admin_api_question_create", defaults={"_format": "form-data"})
     * @Method({"POST"})
     * @param Request $request
     * @return $response
     */
    public function createQuestionAction(Request $request) {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.QuestionService')->createQuestion($request);
            if ($dateService != null) {
                $jsonResponse = $dateService;
                $code = JsonResponse::HTTP_OK;
            } else {
                $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            }
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode($code);
            return $response;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_JSON_INVALID, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            return $response;
        }
    }

    /**
     * @ApiDoc(
     *  description="Question Edit"
     * )
     * @Route("/questions/edit", name="admin_api_question_edit", defaults={"_format": "json"})
     * @Method({"POST"})
     * @param Request $request
     * @return $response
     */
    public function editQuestionAction(Request $request) {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.QuestionService')->editQuestion($request);
            if ($dateService != null) {
                $jsonResponse = $dateService;
                $code = JsonResponse::HTTP_OK;
            } else {
                $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            }
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode($code);
            return $response;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_JSON_INVALID, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            return $response;
        }
    }

    /**
     * @ApiDoc(
     *  description="Question Delete"
     * )
     * @Route("/questions/delete", name="admin_api_question_delete", defaults={"_format": "json"})
     * @Method({"POST"})
     * @param  Request $request
     * @return $response
     */
    public function deleteQuestionAction(Request $request) {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.QuestionService')->deleteQuestion($request);
            if ($dateService != null) {
                $jsonResponse = $dateService;
                $code = JsonResponse::HTTP_OK;
            } else {
                $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            }
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode($code);
            return $response;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_JSON_INVALID, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            return $response;
        }
}

    /**
     * @ApiDoc(
     *  description="Get All Questions"
     * )
     * @Route("/questions", name="admin_api_question_get_all", defaults={"_format": "json"})
     * @Method({"GET"})
     * @param Request $request
     * @return $response
     */
    public function getAllQuestionsAction(Request $request) {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        /*if ($jsonFormatter->jsonFormatter($request)) {*/
            $dateService = $this->get('api.QuestionService')->getAllQuestions($request);
            if ($dateService != null) {
                $jsonResponse = $dateService;
                $code = JsonResponse::HTTP_OK;
            } else {
                $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            }
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode($code);
            return $response;
        /*} else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_JSON_INVALID, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            return $response;
        }*/
    }

    /**
     * @ApiDoc(
     *  description="Get All Questions"
     * )
     * @Route("/questions/all/contentTypeTest", name="admin_api_question_get_all_contentTypeTest", defaults={"_format": "json"})
     * @Method({"GET"})
     * @param Request $request
     * @return $response
     */
    public function getAllQuestionsContentTypeTestAction(Request $request) {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.QuestionService')->getAllQuestions($request);
            if ($dateService != null) {
                $jsonResponse = $dateService;
                $code = JsonResponse::HTTP_OK;
            } else {
                $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            }
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode($code);
            return $response;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_JSON_INVALID, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            return $response;
        }
    }
    
    /**
     * @ApiDoc(
     *  description="CMS Recovery user token"
     * )
     * @Route("/users/recover-token", name="admin_api_user_recovery_token", defaults={"_format": "json"})
     * @Method({"POST"})
     * @param Request $request
     * @return $response
     */
    public function recoveryUserTokenAction(Request $request) {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.AdminService')->recoveryUserToken($request);
            if ($dateService != null) {
                $jsonResponse = $dateService;
                $code = JsonResponse::HTTP_OK;
            } else {
                $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            }
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode($code);
            return $response;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_JSON_INVALID, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            return $response;
        }
    }

    /**
     * @ApiDoc(
     *  description="Update User Password via Token"
     * )
     * @Route("/users/update_password", name="admin_api_user_update_password", defaults={"_format": "json"})
     * @Method({"POST"})
     * @param Request $request
     * @return $response
     */
    public function updatePasswordFromRecoverModeAction(Request $request) {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.AdminService')->updatePasswordFromRecoverMode($request);
            if ($dateService != null) {
                $jsonResponse = $dateService;
                $code = JsonResponse::HTTP_OK;
            } else {
                $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            }
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode($code);
            return $response;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_JSON_INVALID, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            return $response;
        }
    }

    /**
     * @ApiDoc(
     *  description="User Password Matcher"
     * )
     * @Route("/users/password_matcher", name="admin_api_user_password_matcher", defaults={"_format": "json"})
     * @Method({"POST"})
     * @param Request $request
     * @return $response
     */
    public function passwordMatcherAction(Request $request) {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.AdminService')->passwordMatcher($request);
            if ($dateService != null) {
                $jsonResponse = $dateService;
                $code = JsonResponse::HTTP_OK;
            } else {
                $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            }
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode($code);
            return $response;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_JSON_INVALID, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            return $response;
        }
    }

    /**
     * @ApiDoc(
     *  description="Create News"
     * )
     * @Route("/news/create", name="admin_api_news_create", defaults={"_format": "json"})
     * @Method({"POST"})
     * @param Request $request
     * @return $response
     */
    public function createNewsAction(Request $request) {
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;

        $dateService = $this->get('api.NewsService')->createNews($request);
        if ($dateService != null) {
            $jsonResponse = $dateService;
            $code = JsonResponse::HTTP_OK;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
        }
        $response = new Response(json_encode($jsonResponse));
        $response->setStatusCode($code);
        return $response;
    }

    /**
     * @ApiDoc(
     *  description="Get All News"
     * )
     * @Route("/news", name="admin_api_news_get_all", defaults={"_format": "json"})
     * @Method({"GET"})
     * @param Request $request
     * @return $response
     */
    public function getAllNewsAction(Request $request) {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.NewsService')->getAllNews($request);
            if ($dateService != null) {
                $jsonResponse = $dateService;
                $code = JsonResponse::HTTP_OK;
            } else {
                $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            }
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode($code);
            return $response;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_JSON_INVALID, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            return $response;
        }
    }

    /**
     * @ApiDoc(
     *  description="Update News"
     * )
     * @Route("/news/edit", name="admin_api_news_edit", defaults={"_format": "json"})
     * @Method({"POST"})
     * @param Request $request
     * @return $response
     */
    public function updateNewsAction(Request $request) {
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;

        $dateService = $this->get('api.NewsService')->updateNews($request);
        if ($dateService != null) {
            $jsonResponse = $dateService;
            $code = JsonResponse::HTTP_OK;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
        }
        $response = new Response(json_encode($jsonResponse));
        $response->setStatusCode($code);
        return $response;
    }

    /**
     * @ApiDoc(
     *  description="Delete News"
     * )
     * @Route("/news/delete", name="admin_api_news_delete", defaults={"_format": "json"})
     * @Method({"POST"})
     * @param Request $request
     * @return $response
     */
    public function deleteNewsAction(Request $request) {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.NewsService')->deleteNews($request);
            if ($dateService != null) {
                $jsonResponse = $dateService;
                $code = JsonResponse::HTTP_OK;
            } else {
                $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            }
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode($code);
            return $response;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_JSON_INVALID, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            return $response;
        }
    }

    /**
     * @ApiDoc(
     *  description="Get All AppUsers"
     * )
     * @Route("/app_users", name="admin_api_news_get_all_app_users", defaults={"_format": "json"})
     * @Method({"GET"})
     * @param Request $request
     * @return $response
     */
    public function getAllAppUsersAction(Request $request) {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.AppUserService')->getAllAppUsers($request);
            if ($dateService != null) {
                $jsonResponse = $dateService;
                $code = JsonResponse::HTTP_OK;
            } else {
                $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            }
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode($code);
            return $response;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_JSON_INVALID, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            return $response;
        }
    }

    /**
     * @ApiDoc(
     *  description="Get AppUser Detail Info"
     * )
     * @Route("/app_users/detail", name="admin_api_news_get_app_user_detail_info", defaults={"_format": "json"})
     * @Method({"POST"})
     * @param Request $request
     * @return $response
     */
    public function getAllAppUserDetailInfoAction(Request $request) {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.AppUserService')->getAllAppUserDetailInfo($request);
            if ($dateService != null) {
                $jsonResponse = $dateService;
                $code = JsonResponse::HTTP_OK;
            } else {
                $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            }
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode($code);
            return $response;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_JSON_INVALID, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            return $response;
        }
    }

    /**
     * @ApiDoc(
     *  description="Enable or Disable Final Test Access"
     * )
     * @Route("/app_users/final_test_permission", name="admin_api_app_user_enable_or_disable_final_test", defaults={"_format": "json"})
     * @Method({"POST"})
     * @param Request $request
     * @return $response
     */
    public function enableOrDisableFinalTestAccessAction(Request $request) {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.AppUserService')->enableOrDisableFinalTestAccess($request);
            if ($dateService != null) {
                $jsonResponse = $dateService;
                $code = JsonResponse::HTTP_OK;
            } else {
                $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            }
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode($code);
            return $response;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_JSON_INVALID, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            return $response;
        }
    }

    /**
     * @ApiDoc(
     *  description="Create Reading"
     * )
     * @Route("/readings", name="admin_api_get_all_readings", defaults={"_format": "json"})
     * @Method({"GET"})
     * @param Request $request
     * @return $response
     */
    public function getAllReadingsAction(Request $request) {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.ReadingService')->getAllReadings($request);
            if ($dateService != null) {
                $jsonResponse = $dateService;
                $code = JsonResponse::HTTP_OK;
            } else {
                $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            }
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode($code);
            return $response;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_JSON_INVALID, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            return $response;
        }
    }

    /**
     * @ApiDoc(
     *  description="Create Reading"
     * )
     * @Route("/readings/create", name="admin_api_readings_create", defaults={"_format": "json"})
     * @Method({"POST"})
     * @param Request $request
     * @return $response
     */
    public function createReadingAction(Request $request)
    {
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;

        $dateService = $this->get('api.ReadingService')->createReading($request);
        if ($dateService != null) {
            $jsonResponse = $dateService;
            $code = JsonResponse::HTTP_OK;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
        }
        $response = new Response(json_encode($jsonResponse));
        $response->setStatusCode($code);
        return $response;
    }

    /**
     * @ApiDoc(
     *  description="Update Reading"
     * )
     * @Route("/readings/edit", name="admin_api_readings_edit", defaults={"_format": "json"})
     * @Method({"POST"})
     * @param Request $request
     * @return $response
     */
    public function updateReadingAction(Request $request)
    {
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;

        $dateService = $this->get('api.ReadingService')->updateReading($request);
        if ($dateService != null) {
            $jsonResponse = $dateService;
            $code = JsonResponse::HTTP_OK;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
        }
        $response = new Response(json_encode($jsonResponse));
        $response->setStatusCode($code);
        return $response;
    }

    /**
     * @ApiDoc(
     *  description="Delete Reading"
     * )
     * @Route("/readings/delete", name="admin_api_reading_delete", defaults={"_format": "json"})
     * @Method({"POST"})
     * @param Request $request
     * @return $response
     */
    public function deleteReadingAction(Request $request) {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.ReadingService')->deleteReading($request);
            if ($dateService != null) {
                $jsonResponse = $dateService;
                $code = JsonResponse::HTTP_OK;
            } else {
                $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            }
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode($code);
            return $response;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_JSON_INVALID, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            return $response;
        }
    }

    /**
     * @ApiDoc(
     *  description="Get All Push Notifications"
     * )
     * @Route("/push_notifications", name="admin_api_push_notification_get_all", defaults={"_format": "json"})
     * @Method({"GET"})
     * @param Request $request
     * @return $response
     */
    public function getAllPushNotificationsAction(Request $request) {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.PushNotificationService')->getAllPushNotifications($request);
            if ($dateService != null) {
                $jsonResponse = $dateService;
                $code = JsonResponse::HTTP_OK;
            } else {
                $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            }
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode($code);
            return $response;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_JSON_INVALID, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            return $response;
        }
    }

    /**
     * @ApiDoc(
     *  description="Create Push Notification"
     * )
     * @Route("/push_notifications/create", name="admin_api_push_notification_create", defaults={"_format": "json"})
     * @Method({"POST"})
     * @param Request $request
     * @return $response
     */
    public function createPushNotificationAction(Request $request) {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.PushNotificationService')->createPushNotification($request);
            if ($dateService != null) {
                $jsonResponse = $dateService;
                $code = JsonResponse::HTTP_OK;
            } else {
                $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            }
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode($code);
            return $response;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_JSON_INVALID, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            return $response;
        }
    }

    /**
     * @ApiDoc(
     *  description="Send Custom Email"
     * )
     * @Route("/app_users/send_custom_email", name="admin_api_app_user_send_custom_email", defaults={"_format": "json"})
     * @Method({"POST"})
     * @param Request $request
     * @return $response
     */
    public function sendCustomEmailAction(Request $request) {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.AppUserService')->sendCustomEmail($request);
            if ($dateService != null) {
                $jsonResponse = $dateService;
                $code = JsonResponse::HTTP_OK;
            } else {
                $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            }
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode($code);
            return $response;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_JSON_INVALID, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            return $response;
        }
    }

    /**
     * @ApiDoc(
     *  description="CronJob"
     * )
     * @Route("/cronjobtask", name="app_execute_cron_job_task", defaults={"_format": "json"})
     * @Method({"GET"})
     * @param Request $request
     * @return $response
     */
    public function cronJobTaskDecreaseProgressAction(Request $request) {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.ProgressManagementService')->cronJobTaskDecreaseProgress($request);
            if ($dateService != null) {
                $jsonResponse = $dateService;
                $code = JsonResponse::HTTP_OK;
            } else {
                $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            }
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode($code);
            return $response;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_JSON_INVALID, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            return $response;
        }
    }

    /**
     * @ApiDoc(
     *  description="Deny Or Grant Access"
     * )
     * @Route("/app_users/deny-or-grant-access", name="admin_api_app_user_deny_or_grant_access", defaults={"_format": "json"})
     * @Method({"POST"})
     * @param Request $request
     * @return $response
     */
    public function denyOrGrantAccess(Request $request)
    {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.AppUserService')->denyOrGrantAccess($request);
            if ($dateService != null) {
                $jsonResponse = $dateService;
                $code = JsonResponse::HTTP_OK;
            } else {
                $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            }
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode($code);
            return $response;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_JSON_INVALID, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            return $response;
        }
    }

}