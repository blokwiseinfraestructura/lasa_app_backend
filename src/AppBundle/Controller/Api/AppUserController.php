<?php
/**
 * Created by PhpStorm.
 * User: higom
 * Date: 26/12/2016
 * Time: 9:23 AM
 */

namespace AppBundle\Controller\Api;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use AppBundle\Constant\Constant;

/**
 * Admin API Controller.
 *
 * @Route("api/app")
 */
class AppUserController extends Controller
{
    /**
     * @ApiDoc(
     *  description="App get General Parameters"
     * )
     * @Route("/parameters", name="app_api_get_general_parameters", defaults={"_format": "json"})
     * @Method({"GET"})
     * @param Request $request
     * @return $response
     */
    public function getAppGeneralParametersAction(Request $request) {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.AppUserService')->getAppGeneralParameters($request);
            if ($dateService != null) {
                $jsonResponse = $dateService;
                $code = JsonResponse::HTTP_OK;
            } else {
                $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            }
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode($code);
            return $response;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_JSON_INVALID, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            return $response;
        }
    }

    /**
     * @ApiDoc(
     *  description="App User Register"
     * )
     * @Route("/register", name="app_api_user_register", defaults={"_format": "json"})
     * @Method({"POST"})
     * @param Request $request
     * @return $response
     */
    public function registerAppUserAction(Request $request) {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.AppUserService')->registerAppUser($request);
            if ($dateService != null) {
                $jsonResponse = $dateService;
                $code = JsonResponse::HTTP_OK;
            } else {
                $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            }
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode($code);
            return $response;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_JSON_INVALID, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            return $response;
        }
    }

    /**
     * @ApiDoc(
     *  description="App User Login"
     * )
     * @Route("/login", name="app_api_user_login", defaults={"_format": "json"})
     * @Method({"POST"})
     * @param Request $request
     * @return $response
     */
    public function loginAppUserAction(Request $request) {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.AppUserService')->loginAppUser($request);
            if ($dateService != null) {
                $jsonResponse = $dateService;
                $code = JsonResponse::HTTP_OK;
            } else {
                $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            }
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode($code);
            return $response;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_JSON_INVALID, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            return $response;
        }
    }

    /**
     * @ApiDoc(
     *  description="App User Update Module Progress"
     * )
     * @Route("/users/modules/progress", name="app_api_user_update_module_progress", defaults={"_format": "json"})
     * @Method({"POST"})
     * @param Request $request
     * @return $response
     */
    public function updateModuleProgressAction(Request $request) {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.AppModuleService')->updateModuleProgress($request);
            if ($dateService != null) {
                $jsonResponse = $dateService;
                $code = JsonResponse::HTTP_OK;
            } else {
                $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            }
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode($code);
            return $response;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_JSON_INVALID, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            return $response;
        }
    }

    /**
     * @ApiDoc(
     *  description="App User Update Multiple Module Progress"
     * )
     * @Route("/users/modules/progress/multiple", name="app_api_user_update_multiple_module_progress", defaults={"_format": "json"})
     * @Method({"POST"})
     * @param Request $request
     * @return $response
     */
    public function updateMultipleProgressAction(Request $request) {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.AppModuleService')->updateMultipleProgress($request);
            if ($dateService != null) {
                $jsonResponse = $dateService;
                $code = JsonResponse::HTTP_OK;
            } else {
                $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            }
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode($code);
            return $response;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_JSON_INVALID, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            return $response;
        }
    }

    /**
     * @ApiDoc(
     *  description="APP Recovery user token"
     * )
     * @Route("/users/recover-token", name="app_api_user_recovery_token", defaults={"_format": "json"})
     * @Method({"POST"})
     * @param Request $request
     * @return $response
     */
    public function recoveryUserTokenAction(Request $request) {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.AppUserService')->recoveryUserToken($request);
            if ($dateService != null) {
                $jsonResponse = $dateService;
                $code = JsonResponse::HTTP_OK;
            } else {
                $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            }
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode($code);
            return $response;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_JSON_INVALID, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            return $response;
        }
    }
    
    /**
     * @ApiDoc(
     *  description="User Image"
     * )
     * @Route("/users/image", name="app_api_user_image", defaults={"_format": "json"})
     * @Method({"POST"})
     * @param Request $request
     * @return $response
     */
    public function userImageAction(Request $request) {
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;

        $dateService = $this->get('api.AppUserService')->userImage($request);
        if ($dateService != null) {
            $jsonResponse = $dateService;
            $code = JsonResponse::HTTP_OK;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
        }
        $response = new Response(json_encode($jsonResponse));
        $response->setStatusCode($code);
        return $response;
    }
    
    /**
     * @ApiDoc(
     *  description="App get Ranking"
     * )
     * @Route("/get-ranking", name="app_api_ranking", defaults={"_format": "json"})
     * @Method({"GET"})
     * @param Request $request
     * @return $response
     */
    public function getRankingAction(Request $request) {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.RankingService')->getRanking($request);
            if ($dateService != null) {
                $jsonResponse = $dateService;
                $code = JsonResponse::HTTP_OK;
            } else {
                $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            }
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode($code);
            return $response;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_JSON_INVALID, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            return $response;
        }
    }
    
    /**
     * @ApiDoc(
     *  description="App User Create Challenge"
     * )
     * @Route("/challenges/create_or_respond", name="app_api_user_create_challenge", defaults={"_format": "json"})
     * @Method({"POST"})
     * @param Request $request
     * @return $response
     */
    public function createChallengeAction(Request $request) {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.ChallengeService')->createChallengeV2($request);
            if ($dateService != null) {
                $jsonResponse = $dateService;
                $code = JsonResponse::HTTP_OK;
            } else {
                $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            }
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode($code);
            return $response;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_JSON_INVALID, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            return $response;
        }
    }

    /**
     * @ApiDoc(
     *  description="App User Get Challenges Module"
     * )
     * @Route("/challenges", name="app_api_user_get_challenges_module", defaults={"_format": "json"})
     * @Method({"GET"})
     * @param Request $request
     * @return $response
     */
    public function getChallengeModuleAction(Request $request) {
        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.ChallengeService')->getChallengeModule($request);
            if ($dateService != null) {
                $jsonResponse = $dateService;
                $code = JsonResponse::HTTP_OK;
            } else {
                $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            }
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode($code);
            return $response;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_JSON_INVALID, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            return $response;
        }
    }

    /**
     * @ApiDoc(
     *  description="App User Decline or Delete Challenge"
     * )
     * @Route("/challenges/decline_or_delete", name="app_api_user_decline_challenge", defaults={"_format": "json"})
     * @Method({"POST"})
     * @param Request $request
     * @return $response
     */
    public function declineChallengeAction(Request $request) {

        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.ChallengeService')->declineChallenge($request);
            if ($dateService != null) {
                $jsonResponse = $dateService;
                $code = JsonResponse::HTTP_OK;
            } else {
                $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            }
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode($code);
            return $response;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_JSON_INVALID, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            return $response;
        }
    }

    /**
     * @ApiDoc(
     *  description="Insert Final Test Result"
     * )
     * @Route("/users/final_test_result", name="app_api_user_final_test_result", defaults={"_format": "json"})
     * @Method({"POST"})
     * @param Request $request
     * @return $response
     */
    public function insertFinalTestResultAction(Request $request) {

        $jsonFormatter = $this->get('utility.jsonService');
        $jsonResponse = null;
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        if ($jsonFormatter->jsonFormatter($request)) {
            $dateService = $this->get('api.AppModuleService')->insertFinalTestResult($request);
            if ($dateService != null) {
                $jsonResponse = $dateService;
                $code = JsonResponse::HTTP_OK;
            } else {
                $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_ERROR_FATAL, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            }
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode($code);
            return $response;
        } else {
            $jsonResponse = array(Constant::MESSAGE_RESPONSE_MESSAGE => Constant::MESSAGE_RESPONSE_JSON_INVALID, Constant::MESSAGE_RESPONSE_STATUS => Constant::STATUS_FALSE);
            $response = new Response(json_encode($jsonResponse));
            $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
            return $response;
        }
    }
}