<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WorkArea
 *
 * @ORM\Table(name="work_area")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\WorkAreaRepository")
 */
class WorkArea
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="work_area_name", type="string", length=100)
     */
    private $workAreaName;

    /**
     * @ORM\OneToMany(targetEntity="AppUser", mappedBy="workAreaId")
     */
    protected $appUsersByWorkArea;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->appUsersByWorkArea = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set workAreaName
     *
     * @param string $workAreaName
     *
     * @return WorkArea
     */
    public function setWorkAreaName($workAreaName)
    {
        $this->workAreaName = $workAreaName;

        return $this;
    }

    /**
     * Get workAreaName
     *
     * @return string
     */
    public function getWorkAreaName()
    {
        return $this->workAreaName;
    }

    /**
     * Add appUsersByWorkArea
     *
     * @param \AppBundle\Entity\AppUser $appUsersByWorkArea
     *
     * @return WorkArea
     */
    public function addAppUsersByWorkArea(\AppBundle\Entity\AppUser $appUsersByWorkArea)
    {
        $this->appUsersByWorkArea[] = $appUsersByWorkArea;

        return $this;
    }

    /**
     * Remove appUsersByWorkArea
     *
     * @param \AppBundle\Entity\AppUser $appUsersByWorkArea
     */
    public function removeAppUsersByWorkArea(\AppBundle\Entity\AppUser $appUsersByWorkArea)
    {
        $this->appUsersByWorkArea->removeElement($appUsersByWorkArea);
    }

    /**
     * Get appUsersByWorkArea
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAppUsersByWorkArea()
    {
        return $this->appUsersByWorkArea;
    }
}
