<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AppUserAchievement
 *
 * @ORM\Table(name="app_user_achievement")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AppUserAchievementRepository")
 */
class AppUserAchievement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @var AppUser @ORM\ManyToOne(targetEntity="AppBundle\Entity\AppUser",inversedBy="adminUserModulesUser")
     * @ORM\JoinColumn(name="app_user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $appUserId;

    /**
     *
     * @var Achievement @ORM\ManyToOne(targetEntity="AppBundle\Entity\Achievement",inversedBy="appUserAchievements")
     * @ORM\JoinColumn(name="achievement_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $achievementId;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set appUserId
     *
     * @param \AppBundle\Entity\AppUser $appUserId
     *
     * @return AppUserAchievement
     */
    public function setAppUserId(\AppBundle\Entity\AppUser $appUserId = null)
    {
        $this->appUserId = $appUserId;

        return $this;
    }

    /**
     * Get appUserId
     *
     * @return \AppBundle\Entity\AppUser
     */
    public function getAppUserId()
    {
        return $this->appUserId;
    }

    /**
     * Set achievementId
     *
     * @param \AppBundle\Entity\Achievement $achievementId
     *
     * @return AppUserAchievement
     */
    public function setAchievementId(\AppBundle\Entity\Achievement $achievementId = null)
    {
        $this->achievementId = $achievementId;

        return $this;
    }

    /**
     * Get achievementId
     *
     * @return \AppBundle\Entity\Achievement
     */
    public function getAchievementId()
    {
        return $this->achievementId;
    }
}
