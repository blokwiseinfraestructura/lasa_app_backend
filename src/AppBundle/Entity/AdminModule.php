<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AdminModule
 *
 * @ORM\Table(name="admin_module")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AdminModuleRepository")
 */
class AdminModule
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="module_name", type="string", length=60)
     */
    private $moduleName;

    /**
     * @var int
     *
     * @ORM\Column(name="module_code", type="integer")
     */
    private $moduleCode;

    /**
     * @ORM\OneToMany(targetEntity="UserAdminModule", mappedBy="adminModuleId")
     */
    protected $adminUserModulesModule;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->adminUserModulesModule = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set moduleName
     *
     * @param string $moduleName
     *
     * @return AdminModule
     */
    public function setModuleName($moduleName)
    {
        $this->moduleName = $moduleName;

        return $this;
    }

    /**
     * Get moduleName
     *
     * @return string
     */
    public function getModuleName()
    {
        return $this->moduleName;
    }

    /**
     * Set moduleCode
     *
     * @param integer $moduleCode
     *
     * @return AdminModule
     */
    public function setModuleCode($moduleCode)
    {
        $this->moduleCode = $moduleCode;

        return $this;
    }

    /**
     * Get moduleCode
     *
     * @return integer
     */
    public function getModuleCode()
    {
        return $this->moduleCode;
    }

    /**
     * Add adminUserModulesModule
     *
     * @param \AppBundle\Entity\UserAdminModule $adminUserModulesModule
     *
     * @return AdminModule
     */
    public function addAdminUserModulesModule(\AppBundle\Entity\UserAdminModule $adminUserModulesModule)
    {
        $this->adminUserModulesModule[] = $adminUserModulesModule;

        return $this;
    }

    /**
     * Remove adminUserModulesModule
     *
     * @param \AppBundle\Entity\UserAdminModule $adminUserModulesModule
     */
    public function removeAdminUserModulesModule(\AppBundle\Entity\UserAdminModule $adminUserModulesModule)
    {
        $this->adminUserModulesModule->removeElement($adminUserModulesModule);
    }

    /**
     * Get adminUserModulesModule
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAdminUserModulesModule()
    {
        return $this->adminUserModulesModule;
    }
}
