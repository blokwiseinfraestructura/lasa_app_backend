<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AppUserModule
 *
 * @ORM\Table(name="app_user_module")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AppUserModuleRepository")
 */
class AppUserModule
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="score", type="integer")
     */
    private $score;

    /**
     * @var int
     *
     * @ORM\Column(name="max_score", type="integer")
     */
    private $maxScore;

    /**
     *
     * @var AppModule @ORM\ManyToOne(targetEntity="AppBundle\Entity\AppModule", inversedBy="appModulesModule")
     * @ORM\JoinColumn(name="app_module_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $appModuleId;

    /**
     *
     * @var AppUser @ORM\ManyToOne(targetEntity="AppBundle\Entity\AppUser",inversedBy="appModulesUser")
     * @ORM\JoinColumn(name="app_user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $appUserId;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set score
     *
     * @param integer $score
     *
     * @return AppUserModule
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     *
     * @return integer
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set maxScore
     *
     * @param integer $maxScore
     *
     * @return AppUserModule
     */
    public function setMaxScore($maxScore)
    {
        $this->maxScore = $maxScore;

        return $this;
    }

    /**
     * Get maxScore
     *
     * @return integer
     */
    public function getMaxScore()
    {
        return $this->maxScore;
    }

    /**
     * Set appModuleId
     *
     * @param \AppBundle\Entity\AppModule $appModuleId
     *
     * @return AppUserModule
     */
    public function setAppModuleId(\AppBundle\Entity\AppModule $appModuleId = null)
    {
        $this->appModuleId = $appModuleId;

        return $this;
    }

    /**
     * Get appModuleId
     *
     * @return \AppBundle\Entity\AppModule
     */
    public function getAppModuleId()
    {
        return $this->appModuleId;
    }

    /**
     * Set appUserId
     *
     * @param \AppBundle\Entity\AppUser $appUserId
     *
     * @return AppUserModule
     */
    public function setAppUserId(\AppBundle\Entity\AppUser $appUserId = null)
    {
        $this->appUserId = $appUserId;

        return $this;
    }

    /**
     * Get appUserId
     *
     * @return \AppBundle\Entity\AppUser
     */
    public function getAppUserId()
    {
        return $this->appUserId;
    }
}
