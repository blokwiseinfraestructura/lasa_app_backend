<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Answer
 *
 * @ORM\Table(name="answer")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AnswerRepository")
 */
class Answer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="answer_description", type="string", length=150)
     */
    private $answerDescription;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_correct", type="boolean")
     */
    private $isCorrect;

    /**
     *
     * @var Question @ORM\ManyToOne(targetEntity="AppBundle\Entity\Question",inversedBy="answers")
     * @ORM\JoinColumn(name="question_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $questionId;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set answerDescription
     *
     * @param string $answerDescription
     *
     * @return Answer
     */
    public function setAnswerDescription($answerDescription)
    {
        $this->answerDescription = $answerDescription;

        return $this;
    }

    /**
     * Get answerDescription
     *
     * @return string
     */
    public function getAnswerDescription()
    {
        return $this->answerDescription;
    }

    /**
     * Set isCorrect
     *
     * @param boolean $isCorrect
     *
     * @return Answer
     */
    public function setIsCorrect($isCorrect)
    {
        $this->isCorrect = $isCorrect;

        return $this;
    }

    /**
     * Get isCorrect
     *
     * @return boolean
     */
    public function getIsCorrect()
    {
        return $this->isCorrect;
    }

    /**
     * Set questionId
     *
     * @param \AppBundle\Entity\Question $questionId
     *
     * @return Answer
     */
    public function setQuestionId(\AppBundle\Entity\Question $questionId = null)
    {
        $this->questionId = $questionId;

        return $this;
    }

    /**
     * Get questionId
     *
     * @return \AppBundle\Entity\Question
     */
    public function getQuestionId()
    {
        return $this->questionId;
    }
}
