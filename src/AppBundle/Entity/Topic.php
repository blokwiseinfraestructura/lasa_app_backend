<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Topic
 *
 * @ORM\Table(name="topic")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TopicRepository")
 */
class Topic
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="topic_description", type="string", length=45)
     */
    private $topicDescription;

    /**
     * @var int
     *
     * @ORM\Column(name="topic_code", type="integer")
     */
    private $topicCode;

    /**
     * @ORM\OneToMany(targetEntity="Question", mappedBy="topicId")
     */
    protected $questionsPerTopic;

    /**
     * @ORM\OneToMany(targetEntity="Reading", mappedBy="topicId")
     */
    protected $readingsByTopic;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->questionsPerTopic = new \Doctrine\Common\Collections\ArrayCollection();
        $this->readingsByTopic = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set topicDescription
     *
     * @param string $topicDescription
     *
     * @return Topic
     */
    public function setTopicDescription($topicDescription)
    {
        $this->topicDescription = $topicDescription;

        return $this;
    }

    /**
     * Get topicDescription
     *
     * @return string
     */
    public function getTopicDescription()
    {
        return $this->topicDescription;
    }

    /**
     * Set topicCode
     *
     * @param integer $topicCode
     *
     * @return Topic
     */
    public function setTopicCode($topicCode)
    {
        $this->topicCode = $topicCode;

        return $this;
    }

    /**
     * Get topicCode
     *
     * @return integer
     */
    public function getTopicCode()
    {
        return $this->topicCode;
    }

    /**
     * Add questionsPerTopic
     *
     * @param \AppBundle\Entity\Question $questionsPerTopic
     *
     * @return Topic
     */
    public function addQuestionsPerTopic(\AppBundle\Entity\Question $questionsPerTopic)
    {
        $this->questionsPerTopic[] = $questionsPerTopic;

        return $this;
    }

    /**
     * Remove questionsPerTopic
     *
     * @param \AppBundle\Entity\Question $questionsPerTopic
     */
    public function removeQuestionsPerTopic(\AppBundle\Entity\Question $questionsPerTopic)
    {
        $this->questionsPerTopic->removeElement($questionsPerTopic);
    }

    /**
     * Get questionsPerTopic
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestionsPerTopic()
    {
        return $this->questionsPerTopic;
    }

    /**
     * Add readingsByTopic
     *
     * @param \AppBundle\Entity\Reading $readingsByTopic
     *
     * @return Topic
     */
    public function addReadingsByTopic(\AppBundle\Entity\Reading $readingsByTopic)
    {
        $this->readingsByTopic[] = $readingsByTopic;

        return $this;
    }

    /**
     * Remove readingsByTopic
     *
     * @param \AppBundle\Entity\Reading $readingsByTopic
     */
    public function removeReadingsByTopic(\AppBundle\Entity\Reading $readingsByTopic)
    {
        $this->readingsByTopic->removeElement($readingsByTopic);
    }

    /**
     * Get readingsByTopic
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReadingsByTopic()
    {
        return $this->readingsByTopic;
    }
}
