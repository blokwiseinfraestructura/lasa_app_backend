<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AppUserStatistics
 *
 * @ORM\Table(name="app_user_statistics")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AppUserStatisticsRepository")
 */
class AppUserStatistics
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="challenges_won", type="integer")
     */
    private $challengesWon;

    /**
     * @var int
     *
     * @ORM\Column(name="challenges_lost", type="integer")
     */
    private $challengesLost;

    /**
     * @var int
     *
     * @ORM\Column(name="win_streak", type="integer")
     */
    private $winStreak;

    /**
     * @var int
     *
     * @ORM\Column(name="lose_streak", type="integer")
     */
    private $loseStreak;

    /**
     * @var int
     *
     * @ORM\Column(name="last_challenge_at", type="integer")
     */
    private $lastChallengeAt;

    /**
     * @var int
     *
     * @ORM\Column(name="total_challenges_played", type="integer")
     */
    private $totalChallengesPlayed;

    /**
     * @var \int
     *
     * @ORM\Column(name="time_played", type="integer")
     */
    private $timePlayed;

    /**
     * @var \int
     *
     * @ORM\Column(name="final_test_spend_time", type="integer")
     */
    private $finalTestSpendTime;

    /**
     * @var int
     *
     * @ORM\Column(name="last_practice_at", type="integer", nullable=true)
     */
    private $lastPracticeAt;

    /**
     * @var int
     *
     * @ORM\Column(name="achievements_unlocked", type="integer")
     */
    private $achievementsUnlocked;

    /**
     * @var int
     *
     * @ORM\Column(name="levels_unlocked", type="integer")
     */
    private $levelsUnlocked;

    /**
     * @var int
     *
     * @ORM\Column(name="last_update_decrease_progress_at", type="integer", nullable=true)
     */
    private $lastUpdateDecreaseProgressAt;

    /**
     * @var int
     *
     * @ORM\Column(name="minigames_total_score", type="integer", nullable=true)
     */
    private $minigamesTotalScore;

    /**
     * @ORM\OneToOne(targetEntity="AppUser", mappedBy="appUserStatisticsId")
     */
    protected $appUser;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set challengesWon
     *
     * @param integer $challengesWon
     *
     * @return AppUserStatistics
     */
    public function setChallengesWon($challengesWon)
    {
        $this->challengesWon = $challengesWon;

        return $this;
    }

    /**
     * Get challengesWon
     *
     * @return integer
     */
    public function getChallengesWon()
    {
        return $this->challengesWon;
    }

    /**
     * Set challengesLost
     *
     * @param integer $challengesLost
     *
     * @return AppUserStatistics
     */
    public function setChallengesLost($challengesLost)
    {
        $this->challengesLost = $challengesLost;

        return $this;
    }

    /**
     * Get challengesLost
     *
     * @return integer
     */
    public function getChallengesLost()
    {
        return $this->challengesLost;
    }

    /**
     * Set winStreak
     *
     * @param integer $winStreak
     *
     * @return AppUserStatistics
     */
    public function setWinStreak($winStreak)
    {
        $this->winStreak = $winStreak;

        return $this;
    }

    /**
     * Get winStreak
     *
     * @return integer
     */
    public function getWinStreak()
    {
        return $this->winStreak;
    }

    /**
     * Set loseStreak
     *
     * @param integer $loseStreak
     *
     * @return AppUserStatistics
     */
    public function setLoseStreak($loseStreak)
    {
        $this->loseStreak = $loseStreak;

        return $this;
    }

    /**
     * Get loseStreak
     *
     * @return integer
     */
    public function getLoseStreak()
    {
        return $this->loseStreak;
    }

    /**
     * Set lastChallengeAt
     *
     * @param integer $lastChallengeAt
     *
     * @return AppUserStatistics
     */
    public function setLastChallengeAt($lastChallengeAt)
    {
        $this->lastChallengeAt = $lastChallengeAt;

        return $this;
    }

    /**
     * Get lastChallengeAt
     *
     * @return integer
     */
    public function getLastChallengeAt()
    {
        return $this->lastChallengeAt;
    }

    /**
     * Set totalChallengesPlayed
     *
     * @param integer $totalChallengesPlayed
     *
     * @return AppUserStatistics
     */
    public function setTotalChallengesPlayed($totalChallengesPlayed)
    {
        $this->totalChallengesPlayed = $totalChallengesPlayed;

        return $this;
    }

    /**
     * Get totalChallengesPlayed
     *
     * @return integer
     */
    public function getTotalChallengesPlayed()
    {
        return $this->totalChallengesPlayed;
    }

    /**
     * Set timePlayed
     *
     * @param integer $timePlayed
     *
     * @return AppUserStatistics
     */
    public function setTimePlayed($timePlayed)
    {
        $this->timePlayed = $timePlayed;

        return $this;
    }

    /**
     * Get timePlayed
     *
     * @return integer
     */
    public function getTimePlayed()
    {
        return $this->timePlayed;
    }

    /**
     * Set finalTestSpendTime
     *
     * @param integer $finalTestSpendTime
     *
     * @return AppUserStatistics
     */
    public function setFinalTestSpendTime($finalTestSpendTime)
    {
        $this->finalTestSpendTime = $finalTestSpendTime;

        return $this;
    }

    /**
     * Get finalTestSpendTime
     *
     * @return integer
     */
    public function getFinalTestSpendTime()
    {
        return $this->finalTestSpendTime;
    }

    /**
     * Set lastPracticeAt
     *
     * @param integer $lastPracticeAt
     *
     * @return AppUserStatistics
     */
    public function setLastPracticeAt($lastPracticeAt)
    {
        $this->lastPracticeAt = $lastPracticeAt;

        return $this;
    }

    /**
     * Get lastPracticeAt
     *
     * @return integer
     */
    public function getLastPracticeAt()
    {
        return $this->lastPracticeAt;
    }

    /**
     * Set achievementsUnlocked
     *
     * @param integer $achievementsUnlocked
     *
     * @return AppUserStatistics
     */
    public function setAchievementsUnlocked($achievementsUnlocked)
    {
        $this->achievementsUnlocked = $achievementsUnlocked;

        return $this;
    }

    /**
     * Get achievementsUnlocked
     *
     * @return integer
     */
    public function getAchievementsUnlocked()
    {
        return $this->achievementsUnlocked;
    }

    /**
     * Set levelsUnlocked
     *
     * @param integer $levelsUnlocked
     *
     * @return AppUserStatistics
     */
    public function setLevelsUnlocked($levelsUnlocked)
    {
        $this->levelsUnlocked = $levelsUnlocked;

        return $this;
    }

    /**
     * Get levelsUnlocked
     *
     * @return integer
     */
    public function getLevelsUnlocked()
    {
        return $this->levelsUnlocked;
    }

    /**
     * Set lastUpdateDecreaseProgressAt
     *
     * @param integer $lastUpdateDecreaseProgressAt
     *
     * @return AppUserStatistics
     */
    public function setLastUpdateDecreaseProgressAt($lastUpdateDecreaseProgressAt)
    {
        $this->lastUpdateDecreaseProgressAt = $lastUpdateDecreaseProgressAt;

        return $this;
    }

    /**
     * Get lastUpdateDecreaseProgressAt
     *
     * @return integer
     */
    public function getLastUpdateDecreaseProgressAt()
    {
        return $this->lastUpdateDecreaseProgressAt;
    }

    /**
     * Set minigamesTotalScore
     *
     * @param integer $minigamesTotalScore
     *
     * @return AppUserStatistics
     */
    public function setMinigamesTotalScore($minigamesTotalScore)
    {
        $this->minigamesTotalScore = $minigamesTotalScore;

        return $this;
    }

    /**
     * Get minigamesTotalScore
     *
     * @return integer
     */
    public function getMinigamesTotalScore()
    {
        return $this->minigamesTotalScore;
    }

    /**
     * Set appUser
     *
     * @param \AppBundle\Entity\AppUser $appUser
     *
     * @return AppUserStatistics
     */
    public function setAppUser(\AppBundle\Entity\AppUser $appUser = null)
    {
        $this->appUser = $appUser;

        return $this;
    }

    /**
     * Get appUser
     *
     * @return \AppBundle\Entity\AppUser
     */
    public function getAppUser()
    {
        return $this->appUser;
    }
}
