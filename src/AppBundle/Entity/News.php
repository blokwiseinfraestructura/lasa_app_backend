<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * News
 *
 * @ORM\Table(name="news")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\NewsRepository")
 */
class News
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="news_title", type="string", length=60)
     */
    private $newsTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="news_description", type="string", length=5000)
     */
    private $newsDescription;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @Assert\File(maxSize="6000000")
     * @Assert\NotBlank(message="Please enter a valid file extention(png,jpg,jpeg,gif)")
     * @Assert\File(mimeTypes={ "image/png", "image/jpg", "image/jpeg", "image/gif" })
     * @Assert\Image(minWidth = 120,maxWidth = 2568,minHeight = 20,maxHeight = 1600)
     */
    private $newsImageName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_at", type="datetime", nullable=true)
     */
    private $createAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_at", type="datetime", nullable=true)
     */
    private $updateAt;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set newsTitle
     *
     * @param string $newsTitle
     *
     * @return News
     */
    public function setNewsTitle($newsTitle)
    {
        $this->newsTitle = $newsTitle;

        return $this;
    }

    /**
     * Get newsTitle
     *
     * @return string
     */
    public function getNewsTitle()
    {
        return $this->newsTitle;
    }

    /**
     * Set newsDescription
     *
     * @param string $newsDescription
     *
     * @return News
     */
    public function setNewsDescription($newsDescription)
    {
        $this->newsDescription = $newsDescription;

        return $this;
    }

    /**
     * Get newsDescription
     *
     * @return string
     */
    public function getNewsDescription()
    {
        return $this->newsDescription;
    }

    /**
     * Set newsImageName
     *
     * @param string $newsImageName
     *
     * @return News
     */
    public function setNewsImageName($newsImageName)
    {
        $this->newsImageName = $newsImageName;

        return $this;
    }

    /**
     * Get newsImageName
     *
     * @return string
     */
    public function getNewsImageName()
    {
        return $this->newsImageName;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     *
     * @return News
     */
    public function setCreateAt($createAt)
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * Get createAt
     *
     * @return \DateTime
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * Set updateAt
     *
     * @param \DateTime $updateAt
     *
     * @return News
     */
    public function setUpdateAt($updateAt)
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    /**
     * Get updateAt
     *
     * @return \DateTime
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }
}
