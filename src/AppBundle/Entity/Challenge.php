<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Challenge
 *
 * @ORM\Table(name="challenge")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ChallengeRepository")
 */
class Challenge
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="challenge_identifier", type="string", length=255)
     */
    private $challengeIdentifier;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="challenge_create_at", type="datetime")
     */
    private $challengeCreateAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="challenge_finished_at", type="datetime", nullable=true)
     */
    private $challengeFinishedAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_finished", type="boolean")
     */
    private $isFinished;

    /**
     * @ORM\OneToMany(targetEntity="AppUserChallenge", mappedBy="challengeId")
     */
    protected $challengesAppUser;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->challengesAppUser = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set challengeIdentifier
     *
     * @param string $challengeIdentifier
     *
     * @return Challenge
     */
    public function setChallengeIdentifier($challengeIdentifier)
    {
        $this->challengeIdentifier = $challengeIdentifier;

        return $this;
    }

    /**
     * Get challengeIdentifier
     *
     * @return string
     */
    public function getChallengeIdentifier()
    {
        return $this->challengeIdentifier;
    }

    /**
     * Set challengeCreateAt
     *
     * @param \DateTime $challengeCreateAt
     *
     * @return Challenge
     */
    public function setChallengeCreateAt($challengeCreateAt)
    {
        $this->challengeCreateAt = $challengeCreateAt;

        return $this;
    }

    /**
     * Get challengeCreateAt
     *
     * @return \DateTime
     */
    public function getChallengeCreateAt()
    {
        return $this->challengeCreateAt;
    }

    /**
     * Set challengeFinishedAt
     *
     * @param \DateTime $challengeFinishedAt
     *
     * @return Challenge
     */
    public function setChallengeFinishedAt($challengeFinishedAt)
    {
        $this->challengeFinishedAt = $challengeFinishedAt;

        return $this;
    }

    /**
     * Get challengeFinishedAt
     *
     * @return \DateTime
     */
    public function getChallengeFinishedAt()
    {
        return $this->challengeFinishedAt;
    }

    /**
     * Set isFinished
     *
     * @param boolean $isFinished
     *
     * @return Challenge
     */
    public function setIsFinished($isFinished)
    {
        $this->isFinished = $isFinished;

        return $this;
    }

    /**
     * Get isFinished
     *
     * @return boolean
     */
    public function getIsFinished()
    {
        return $this->isFinished;
    }

    /**
     * Add challengesAppUser
     *
     * @param \AppBundle\Entity\AppUserChallenge $challengesAppUser
     *
     * @return Challenge
     */
    public function addChallengesAppUser(\AppBundle\Entity\AppUserChallenge $challengesAppUser)
    {
        $this->challengesAppUser[] = $challengesAppUser;

        return $this;
    }

    /**
     * Remove challengesAppUser
     *
     * @param \AppBundle\Entity\AppUserChallenge $challengesAppUser
     */
    public function removeChallengesAppUser(\AppBundle\Entity\AppUserChallenge $challengesAppUser)
    {
        $this->challengesAppUser->removeElement($challengesAppUser);
    }

    /**
     * Get challengesAppUser
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChallengesAppUser()
    {
        return $this->challengesAppUser;
    }
}
