<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * City
 *
 * @ORM\Table(name="city")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CityRepository")
 */
class City
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="city_name", type="string", length=100)
     */
    private $cityName;

    /**
     * @ORM\OneToMany(targetEntity="AppUser", mappedBy="cityId")
     */
    protected $appUsersByCity;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->appUsersByCity = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cityName
     *
     * @param string $cityName
     *
     * @return City
     */
    public function setCityName($cityName)
    {
        $this->cityName = $cityName;

        return $this;
    }

    /**
     * Get cityName
     *
     * @return string
     */
    public function getCityName()
    {
        return $this->cityName;
    }

    /**
     * Add appUsersByCity
     *
     * @param \AppBundle\Entity\AppUser $appUsersByCity
     *
     * @return City
     */
    public function addAppUsersByCity(\AppBundle\Entity\AppUser $appUsersByCity)
    {
        $this->appUsersByCity[] = $appUsersByCity;

        return $this;
    }

    /**
     * Remove appUsersByCity
     *
     * @param \AppBundle\Entity\AppUser $appUsersByCity
     */
    public function removeAppUsersByCity(\AppBundle\Entity\AppUser $appUsersByCity)
    {
        $this->appUsersByCity->removeElement($appUsersByCity);
    }

    /**
     * Get appUsersByCity
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAppUsersByCity()
    {
        return $this->appUsersByCity;
    }
}
