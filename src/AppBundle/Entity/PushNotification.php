<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PushNotification
 *
 * @ORM\Table(name="push_notification")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PushNotificationRepository")
 */
class PushNotification
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=30)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=140)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_at", type="datetime")
     */
    private $createAt;

    /**
     *
     * @var AdminUser @ORM\ManyToOne(targetEntity="AppBundle\Entity\AdminUser",inversedBy="adminUserPushNotifications")
     * @ORM\JoinColumn(name="admin_user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $adminUserId;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return PushNotification
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return PushNotification
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     *
     * @return PushNotification
     */
    public function setCreateAt($createAt)
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * Get createAt
     *
     * @return \DateTime
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * Set adminUserId
     *
     * @param \AppBundle\Entity\AdminUser $adminUserId
     *
     * @return PushNotification
     */
    public function setAdminUserId(\AppBundle\Entity\AdminUser $adminUserId = null)
    {
        $this->adminUserId = $adminUserId;

        return $this;
    }

    /**
     * Get adminUserId
     *
     * @return \AppBundle\Entity\AdminUser
     */
    public function getAdminUserId()
    {
        return $this->adminUserId;
    }
}
