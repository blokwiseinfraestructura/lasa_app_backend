<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AppConstant
 *
 * @ORM\Table(name="app_constant")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AppConstantRepository")
 */
class AppConstant
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="app_constant_name", type="string", length=45)
     */
    private $appConstantName;

    /**
     * @var int
     *
     * @ORM\Column(name="app_constant_value", type="integer")
     */
    private $appConstantValue;

    /**
     * @var int
     *
     * @ORM\Column(name="app_constant_code", type="integer")
     */
    private $appConstantCode;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set appConstantName
     *
     * @param string $appConstantName
     *
     * @return AppConstant
     */
    public function setAppConstantName($appConstantName)
    {
        $this->appConstantName = $appConstantName;

        return $this;
    }

    /**
     * Get appConstantName
     *
     * @return string
     */
    public function getAppConstantName()
    {
        return $this->appConstantName;
    }

    /**
     * Set appConstantValue
     *
     * @param integer $appConstantValue
     *
     * @return AppConstant
     */
    public function setAppConstantValue($appConstantValue)
    {
        $this->appConstantValue = $appConstantValue;

        return $this;
    }

    /**
     * Get appConstantValue
     *
     * @return integer
     */
    public function getAppConstantValue()
    {
        return $this->appConstantValue;
    }

    /**
     * Set appConstantCode
     *
     * @param integer $appConstantCode
     *
     * @return AppConstant
     */
    public function setAppConstantCode($appConstantCode)
    {
        $this->appConstantCode = $appConstantCode;

        return $this;
    }

    /**
     * Get appConstantCode
     *
     * @return integer
     */
    public function getAppConstantCode()
    {
        return $this->appConstantCode;
    }
}
