<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FinalTestResult
 *
 * @ORM\Table(name="final_test_result")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FinalTestResultRepository")
 */
class FinalTestResult
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @var AppUser @ORM\ManyToOne(targetEntity="AppBundle\Entity\AppUser", inversedBy="finalTestResults")
     * @ORM\JoinColumn(name="app_user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $appUserId;

    /**
     *
     * @var Level @ORM\ManyToOne(targetEntity="AppBundle\Entity\Level", inversedBy="finalTestResults")
     * @ORM\JoinColumn(name="level_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $levelId;

    /**
     * @var int
     *
     * @ORM\Column(name="right_answers", type="integer")
     */
    private $rightAnswers;

    /**
     * @var int
     *
     * @ORM\Column(name="wrong_answers", type="integer")
     */
    private $wrongAnswers;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rightAnswers
     *
     * @param integer $rightAnswers
     *
     * @return FinalTestResult
     */
    public function setRightAnswers($rightAnswers)
    {
        $this->rightAnswers = $rightAnswers;

        return $this;
    }

    /**
     * Get rightAnswers
     *
     * @return integer
     */
    public function getRightAnswers()
    {
        return $this->rightAnswers;
    }

    /**
     * Set wrongAnswers
     *
     * @param integer $wrongAnswers
     *
     * @return FinalTestResult
     */
    public function setWrongAnswers($wrongAnswers)
    {
        $this->wrongAnswers = $wrongAnswers;

        return $this;
    }

    /**
     * Get wrongAnswers
     *
     * @return integer
     */
    public function getWrongAnswers()
    {
        return $this->wrongAnswers;
    }

    /**
     * Set appUserId
     *
     * @param \AppBundle\Entity\AppUser $appUserId
     *
     * @return FinalTestResult
     */
    public function setAppUserId(\AppBundle\Entity\AppUser $appUserId = null)
    {
        $this->appUserId = $appUserId;

        return $this;
    }

    /**
     * Get appUserId
     *
     * @return \AppBundle\Entity\AppUser
     */
    public function getAppUserId()
    {
        return $this->appUserId;
    }

    /**
     * Set levelId
     *
     * @param \AppBundle\Entity\Level $levelId
     *
     * @return FinalTestResult
     */
    public function setLevelId(\AppBundle\Entity\Level $levelId = null)
    {
        $this->levelId = $levelId;

        return $this;
    }

    /**
     * Get levelId
     *
     * @return \AppBundle\Entity\Level
     */
    public function getLevelId()
    {
        return $this->levelId;
    }
}
