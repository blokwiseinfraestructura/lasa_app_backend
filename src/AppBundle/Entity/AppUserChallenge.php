<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AppUserChallenge
 *
 * @ORM\Table(name="app_user_challenge")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AppUserChallengeRepository")
 */
class AppUserChallenge
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="score", type="integer")
     */
    private $score;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_challenger", type="boolean")
     */
    private $isChallenger;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_winner", type="boolean")
     */
    private $isWinner;

    /**
     *
     * @var AppUser @ORM\ManyToOne(targetEntity="AppBundle\Entity\AppUser", inversedBy="appUserChallenges")
     * @ORM\JoinColumn(name="app_user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $appUserId;

    /**
     *
     * @var Challenge @ORM\ManyToOne(targetEntity="AppBundle\Entity\Challenge", inversedBy="challengesAppUser")
     * @ORM\JoinColumn(name="challenge_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $challengeId;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set score
     *
     * @param integer $score
     *
     * @return AppUserChallenge
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     *
     * @return integer
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set isChallenger
     *
     * @param boolean $isChallenger
     *
     * @return AppUserChallenge
     */
    public function setIsChallenger($isChallenger)
    {
        $this->isChallenger = $isChallenger;

        return $this;
    }

    /**
     * Get isChallenger
     *
     * @return boolean
     */
    public function getIsChallenger()
    {
        return $this->isChallenger;
    }

    /**
     * Set isWinner
     *
     * @param boolean $isWinner
     *
     * @return AppUserChallenge
     */
    public function setIsWinner($isWinner)
    {
        $this->isWinner = $isWinner;

        return $this;
    }

    /**
     * Get isWinner
     *
     * @return boolean
     */
    public function getIsWinner()
    {
        return $this->isWinner;
    }

    /**
     * Set appUserId
     *
     * @param \AppBundle\Entity\AppUser $appUserId
     *
     * @return AppUserChallenge
     */
    public function setAppUserId(\AppBundle\Entity\AppUser $appUserId = null)
    {
        $this->appUserId = $appUserId;

        return $this;
    }

    /**
     * Get appUserId
     *
     * @return \AppBundle\Entity\AppUser
     */
    public function getAppUserId()
    {
        return $this->appUserId;
    }

    /**
     * Set challengeId
     *
     * @param \AppBundle\Entity\Challenge $challengeId
     *
     * @return AppUserChallenge
     */
    public function setChallengeId(\AppBundle\Entity\Challenge $challengeId = null)
    {
        $this->challengeId = $challengeId;

        return $this;
    }

    /**
     * Get challengeId
     *
     * @return \AppBundle\Entity\Challenge
     */
    public function getChallengeId()
    {
        return $this->challengeId;
    }
}
