<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AppUser
 *
 * @ORM\Table(name="app_user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AppUserRepository")
 */
class AppUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=60)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=60)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="identification_number", type="string", length=20)
     */
    private $identificationNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="account_id", type="string", length=255, nullable=true)
     */
    private $accountId;

    /**
     * @var string
     *
     * @ORM\Column(name="recover_password_token", type="string", length=255, nullable=true)
     */
    private $recoverPasswordToken;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="recover_password_token_expires_at", type="datetime", nullable=true)
     */
    private $recoverPasswordTokenExpiresAt;


    /**
     * @var string
     *
     * @ORM\Column(name="device_type", type="string", length=30)
     */
    private $deviceType;

    /**
     * @var string
     *
     * @ORM\Column(name="device_token", type="string", length=255)
     */
    private $deviceToken;


    /**
     * @var bool
     *
     * @ORM\Column(name="is_lasa_worker", type="boolean")
     */
    private $isLasaWorker;

    /**
     * @var bool
     *
     * @ORM\Column(name="have_final_test_access", type="boolean")
     */
    private $haveFinalTestAccess;

    /**
     *
     * @var WorkArea @ORM\ManyToOne(targetEntity="AppBundle\Entity\WorkArea",inversedBy="appUsersByWorkArea")
     * @ORM\JoinColumn(name="work_area_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $workAreaId;

    /**
     *
     * @var City @ORM\ManyToOne(targetEntity="AppBundle\Entity\City",inversedBy="appUsersByCity")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $cityId;
    /**
     *
     * @var Level @ORM\ManyToOne(targetEntity="AppBundle\Entity\Level", inversedBy="appUserByLevel")
     * @ORM\JoinColumn(name="level_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $levelId;

    /**
     *
     * @var AppUserStatistics @ORM\OneToOne(targetEntity="AppBundle\Entity\AppUserStatistics", inversedBy="appUser")
     * @ORM\JoinColumn(name="app_user_statistics_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $appUserStatisticsId;

    /**
     * @var bool
     *
     * @ORM\Column(name="access", type="boolean")
     */
    private $access;


    /**
     * @ORM\OneToMany(targetEntity="AppUserModule", mappedBy="appUserId")
     */
    protected $appModulesUser;

    /**
     * @ORM\OneToMany(targetEntity="AppUserChallenge", mappedBy="appUserId")
     */
    protected $appUserChallenges;
    
    /**
     * @ORM\OneToOne(targetEntity="UserImage", mappedBy="appUserId")
     */
    protected $userImage;
    
    /**
     * @ORM\OneToMany(targetEntity="AppUserAchievement", mappedBy="appUserId")
     */
    protected $appUserAchievements;

    /**
     * @ORM\OneToMany(targetEntity="FinalTestResult", mappedBy="appUserId")
     */
    protected $finalTestResults;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->appModulesUser = new \Doctrine\Common\Collections\ArrayCollection();
        $this->appUserChallenges = new \Doctrine\Common\Collections\ArrayCollection();
        $this->appUserAchievements = new \Doctrine\Common\Collections\ArrayCollection();
        $this->finalTestResults = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return AppUser
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return AppUser
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set identificationNumber
     *
     * @param string $identificationNumber
     *
     * @return AppUser
     */
    public function setIdentificationNumber($identificationNumber)
    {
        $this->identificationNumber = $identificationNumber;

        return $this;
    }

    /**
     * Get identificationNumber
     *
     * @return string
     */
    public function getIdentificationNumber()
    {
        return $this->identificationNumber;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return AppUser
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return AppUser
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set accountId
     *
     * @param string $accountId
     *
     * @return AppUser
     */
    public function setAccountId($accountId)
    {
        $this->accountId = $accountId;

        return $this;
    }

    /**
     * Get accountId
     *
     * @return string
     */
    public function getAccountId()
    {
        return $this->accountId;
    }

    /**
     * Set recoverPasswordToken
     *
     * @param string $recoverPasswordToken
     *
     * @return AppUser
     */
    public function setRecoverPasswordToken($recoverPasswordToken)
    {
        $this->recoverPasswordToken = $recoverPasswordToken;

        return $this;
    }

    /**
     * Get recoverPasswordToken
     *
     * @return string
     */
    public function getRecoverPasswordToken()
    {
        return $this->recoverPasswordToken;
    }

    /**
     * Set recoverPasswordTokenExpiresAt
     *
     * @param \DateTime $recoverPasswordTokenExpiresAt
     *
     * @return AppUser
     */
    public function setRecoverPasswordTokenExpiresAt($recoverPasswordTokenExpiresAt)
    {
        $this->recoverPasswordTokenExpiresAt = $recoverPasswordTokenExpiresAt;

        return $this;
    }

    /**
     * Get recoverPasswordTokenExpiresAt
     *
     * @return \DateTime
     */
    public function getRecoverPasswordTokenExpiresAt()
    {
        return $this->recoverPasswordTokenExpiresAt;
    }

    /**
     * Set deviceType
     *
     * @param string $deviceType
     *
     * @return AppUser
     */
    public function setDeviceType($deviceType)
    {
        $this->deviceType = $deviceType;

        return $this;
    }

    /**
     * Get deviceType
     *
     * @return string
     */
    public function getDeviceType()
    {
        return $this->deviceType;
    }

    /**
     * Set deviceToken
     *
     * @param string $deviceToken
     *
     * @return AppUser
     */
    public function setDeviceToken($deviceToken)
    {
        $this->deviceToken = $deviceToken;

        return $this;
    }

    /**
     * Get deviceToken
     *
     * @return string
     */
    public function getDeviceToken()
    {
        return $this->deviceToken;
    }

    /**
     * Set isLasaWorker
     *
     * @param boolean $isLasaWorker
     *
     * @return AppUser
     */
    public function setIsLasaWorker($isLasaWorker)
    {
        $this->isLasaWorker = $isLasaWorker;

        return $this;
    }

    /**
     * Get isLasaWorker
     *
     * @return boolean
     */
    public function getIsLasaWorker()
    {
        return $this->isLasaWorker;
    }

    /**
     * Set haveFinalTestAccess
     *
     * @param boolean $haveFinalTestAccess
     *
     * @return AppUser
     */
    public function setHaveFinalTestAccess($haveFinalTestAccess)
    {
        $this->haveFinalTestAccess = $haveFinalTestAccess;

        return $this;
    }

    /**
     * Get haveFinalTestAccess
     *
     * @return boolean
     */
    public function getHaveFinalTestAccess()
    {
        return $this->haveFinalTestAccess;
    }

    /**
     * Set access
     *
     * @param boolean $access
     *
     * @return AppUser
     */
    public function setAccess($access)
    {
        $this->access = $access;

        return $this;
    }

    /**
     * Get access
     *
     * @return boolean
     */
    public function getAccess()
    {
        return $this->access;
    }

    /**
     * Set workAreaId
     *
     * @param \AppBundle\Entity\WorkArea $workAreaId
     *
     * @return AppUser
     */
    public function setWorkAreaId(\AppBundle\Entity\WorkArea $workAreaId = null)
    {
        $this->workAreaId = $workAreaId;

        return $this;
    }

    /**
     * Get workAreaId
     *
     * @return \AppBundle\Entity\WorkArea
     */
    public function getWorkAreaId()
    {
        return $this->workAreaId;
    }

    /**
     * Set cityId
     *
     * @param \AppBundle\Entity\City $cityId
     *
     * @return AppUser
     */
    public function setCityId(\AppBundle\Entity\City $cityId = null)
    {
        $this->cityId = $cityId;

        return $this;
    }

    /**
     * Get cityId
     *
     * @return \AppBundle\Entity\City
     */
    public function getCityId()
    {
        return $this->cityId;
    }

    /**
     * Set levelId
     *
     * @param \AppBundle\Entity\Level $levelId
     *
     * @return AppUser
     */
    public function setLevelId(\AppBundle\Entity\Level $levelId = null)
    {
        $this->levelId = $levelId;

        return $this;
    }

    /**
     * Get levelId
     *
     * @return \AppBundle\Entity\Level
     */
    public function getLevelId()
    {
        return $this->levelId;
    }

    /**
     * Set appUserStatisticsId
     *
     * @param \AppBundle\Entity\AppUserStatistics $appUserStatisticsId
     *
     * @return AppUser
     */
    public function setAppUserStatisticsId(\AppBundle\Entity\AppUserStatistics $appUserStatisticsId = null)
    {
        $this->appUserStatisticsId = $appUserStatisticsId;

        return $this;
    }

    /**
     * Get appUserStatisticsId
     *
     * @return \AppBundle\Entity\AppUserStatistics
     */
    public function getAppUserStatisticsId()
    {
        return $this->appUserStatisticsId;
    }

    /**
     * Add appModulesUser
     *
     * @param \AppBundle\Entity\AppUserModule $appModulesUser
     *
     * @return AppUser
     */
    public function addAppModulesUser(\AppBundle\Entity\AppUserModule $appModulesUser)
    {
        $this->appModulesUser[] = $appModulesUser;

        return $this;
    }

    /**
     * Remove appModulesUser
     *
     * @param \AppBundle\Entity\AppUserModule $appModulesUser
     */
    public function removeAppModulesUser(\AppBundle\Entity\AppUserModule $appModulesUser)
    {
        $this->appModulesUser->removeElement($appModulesUser);
    }

    /**
     * Get appModulesUser
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAppModulesUser()
    {
        return $this->appModulesUser;
    }

    /**
     * Add appUserChallenge
     *
     * @param \AppBundle\Entity\AppUserChallenge $appUserChallenge
     *
     * @return AppUser
     */
    public function addAppUserChallenge(\AppBundle\Entity\AppUserChallenge $appUserChallenge)
    {
        $this->appUserChallenges[] = $appUserChallenge;

        return $this;
    }

    /**
     * Remove appUserChallenge
     *
     * @param \AppBundle\Entity\AppUserChallenge $appUserChallenge
     */
    public function removeAppUserChallenge(\AppBundle\Entity\AppUserChallenge $appUserChallenge)
    {
        $this->appUserChallenges->removeElement($appUserChallenge);
    }

    /**
     * Get appUserChallenges
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAppUserChallenges()
    {
        return $this->appUserChallenges;
    }

    /**
     * Set userImage
     *
     * @param \AppBundle\Entity\UserImage $userImage
     *
     * @return AppUser
     */
    public function setUserImage(\AppBundle\Entity\UserImage $userImage = null)
    {
        $this->userImage = $userImage;

        return $this;
    }

    /**
     * Get userImage
     *
     * @return \AppBundle\Entity\UserImage
     */
    public function getUserImage()
    {
        return $this->userImage;
    }

    /**
     * Add appUserAchievement
     *
     * @param \AppBundle\Entity\AppUserAchievement $appUserAchievement
     *
     * @return AppUser
     */
    public function addAppUserAchievement(\AppBundle\Entity\AppUserAchievement $appUserAchievement)
    {
        $this->appUserAchievements[] = $appUserAchievement;

        return $this;
    }

    /**
     * Remove appUserAchievement
     *
     * @param \AppBundle\Entity\AppUserAchievement $appUserAchievement
     */
    public function removeAppUserAchievement(\AppBundle\Entity\AppUserAchievement $appUserAchievement)
    {
        $this->appUserAchievements->removeElement($appUserAchievement);
    }

    /**
     * Get appUserAchievements
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAppUserAchievements()
    {
        return $this->appUserAchievements;
    }

    /**
     * Add finalTestResult
     *
     * @param \AppBundle\Entity\FinalTestResult $finalTestResult
     *
     * @return AppUser
     */
    public function addFinalTestResult(\AppBundle\Entity\FinalTestResult $finalTestResult)
    {
        $this->finalTestResults[] = $finalTestResult;

        return $this;
    }

    /**
     * Remove finalTestResult
     *
     * @param \AppBundle\Entity\FinalTestResult $finalTestResult
     */
    public function removeFinalTestResult(\AppBundle\Entity\FinalTestResult $finalTestResult)
    {
        $this->finalTestResults->removeElement($finalTestResult);
    }

    /**
     * Get finalTestResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFinalTestResults()
    {
        return $this->finalTestResults;
    }
}
