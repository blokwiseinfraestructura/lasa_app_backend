<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Achievement
 *
 * @ORM\Table(name="achievement")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AchievementRepository")
 */
class Achievement {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="achievement_name", type="string", length=255)
     */
    private $achievementName;

    /**
     * @var int
     *
     * @ORM\Column(name="achievement_code", type="integer")
     */
    private $achievementCode;

    /**
     * @var string
     *
     * @ORM\Column(name="achievement_detail", type="string", length=255)
     */
    private $achievementDetail;

    /**
     * @ORM\OneToMany(targetEntity="AppUserAchievement", mappedBy="achievementId")
     */
    protected $appUserAchievement;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->appUserAchievement = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set achievementName
     *
     * @param string $achievementName
     *
     * @return Achievement
     */
    public function setAchievementName($achievementName)
    {
        $this->achievementName = $achievementName;

        return $this;
    }

    /**
     * Get achievementName
     *
     * @return string
     */
    public function getAchievementName()
    {
        return $this->achievementName;
    }

    /**
     * Set achievementCode
     *
     * @param integer $achievementCode
     *
     * @return Achievement
     */
    public function setAchievementCode($achievementCode)
    {
        $this->achievementCode = $achievementCode;

        return $this;
    }

    /**
     * Get achievementCode
     *
     * @return integer
     */
    public function getAchievementCode()
    {
        return $this->achievementCode;
    }

    /**
     * Set achievementDetail
     *
     * @param string $achievementDetail
     *
     * @return Achievement
     */
    public function setAchievementDetail($achievementDetail)
    {
        $this->achievementDetail = $achievementDetail;

        return $this;
    }

    /**
     * Get achievementDetail
     *
     * @return string
     */
    public function getAchievementDetail()
    {
        return $this->achievementDetail;
    }

    /**
     * Add appUserAchievement
     *
     * @param \AppBundle\Entity\AppUserAchievement $appUserAchievement
     *
     * @return Achievement
     */
    public function addAppUserAchievement(\AppBundle\Entity\AppUserAchievement $appUserAchievement)
    {
        $this->appUserAchievement[] = $appUserAchievement;

        return $this;
    }

    /**
     * Remove appUserAchievement
     *
     * @param \AppBundle\Entity\AppUserAchievement $appUserAchievement
     */
    public function removeAppUserAchievement(\AppBundle\Entity\AppUserAchievement $appUserAchievement)
    {
        $this->appUserAchievement->removeElement($appUserAchievement);
    }

    /**
     * Get appUserAchievement
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAppUserAchievement()
    {
        return $this->appUserAchievement;
    }
}
