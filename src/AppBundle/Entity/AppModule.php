<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AppModule
 *
 * @ORM\Table(name="app_module")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AppModuleRepository")
 */
class AppModule
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="app_module_name", type="string", length=30)
     */
    private $appModuleName;

    /**
     *
     * @var Level @ORM\ManyToOne(targetEntity="AppBundle\Entity\Level",inversedBy="questionsPerLevel")
     * @ORM\JoinColumn(name="level_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $levelId;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set appModuleName
     *
     * @param string $appModuleName
     *
     * @return AppModule
     */
    public function setAppModuleName($appModuleName)
    {
        $this->appModuleName = $appModuleName;

        return $this;
    }

    /**
     * Get appModuleName
     *
     * @return string
     */
    public function getAppModuleName()
    {
        return $this->appModuleName;
    }

    /**
     * Set levelId
     *
     * @param \AppBundle\Entity\Level $levelId
     *
     * @return AppModule
     */
    public function setLevelId(\AppBundle\Entity\Level $levelId = null)
    {
        $this->levelId = $levelId;

        return $this;
    }

    /**
     * Get levelId
     *
     * @return \AppBundle\Entity\Level
     */
    public function getLevelId()
    {
        return $this->levelId;
    }
}
