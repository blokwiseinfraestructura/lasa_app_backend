<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Question
 *
 * @ORM\Table(name="question")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\QuestionRepository")
 */
class Question
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="question_description", type="string", length=255)
     */
    private $questionDescription;

    /**
     *
     * @var Topic @ORM\ManyToOne(targetEntity="AppBundle\Entity\Topic",inversedBy="questionsPerTopic")
     * @ORM\JoinColumn(name="topic_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $topicId;

    /**
     *
     * @var Level @ORM\ManyToOne(targetEntity="AppBundle\Entity\Level",inversedBy="questionsPerLevel")
     * @ORM\JoinColumn(name="level_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $levelId;

    /**
     * @ORM\OneToMany(targetEntity="Answer", mappedBy="questionId")
     */
    protected $answers;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->answers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set questionDescription
     *
     * @param string $questionDescription
     *
     * @return Question
     */
    public function setQuestionDescription($questionDescription)
    {
        $this->questionDescription = $questionDescription;

        return $this;
    }

    /**
     * Get questionDescription
     *
     * @return string
     */
    public function getQuestionDescription()
    {
        return $this->questionDescription;
    }

    /**
     * Set topicId
     *
     * @param \AppBundle\Entity\Topic $topicId
     *
     * @return Question
     */
    public function setTopicId(\AppBundle\Entity\Topic $topicId = null)
    {
        $this->topicId = $topicId;

        return $this;
    }

    /**
     * Get topicId
     *
     * @return \AppBundle\Entity\Topic
     */
    public function getTopicId()
    {
        return $this->topicId;
    }

    /**
     * Set levelId
     *
     * @param \AppBundle\Entity\Level $levelId
     *
     * @return Question
     */
    public function setLevelId(\AppBundle\Entity\Level $levelId = null)
    {
        $this->levelId = $levelId;

        return $this;
    }

    /**
     * Get levelId
     *
     * @return \AppBundle\Entity\Level
     */
    public function getLevelId()
    {
        return $this->levelId;
    }

    /**
     * Add answer
     *
     * @param \AppBundle\Entity\Answer $answer
     *
     * @return Question
     */
    public function addAnswer(\AppBundle\Entity\Answer $answer)
    {
        $this->answers[] = $answer;

        return $this;
    }

    /**
     * Remove answer
     *
     * @param \AppBundle\Entity\Answer $answer
     */
    public function removeAnswer(\AppBundle\Entity\Answer $answer)
    {
        $this->answers->removeElement($answer);
    }

    /**
     * Get answers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnswers()
    {
        return $this->answers;
    }
}
