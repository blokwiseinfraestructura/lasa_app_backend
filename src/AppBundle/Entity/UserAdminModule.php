<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserAdminModule
 *
 * @ORM\Table(name="user_admin_module")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserAdminModuleRepository")
 */
class UserAdminModule
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_available", type="boolean")
     */
    private $isAvailable;

    /**
     *
     * @var AdminModule @ORM\ManyToOne(targetEntity="AppBundle\Entity\AdminModule",inversedBy="adminUserModulesModule")
     * @ORM\JoinColumn(name="admin_module_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $adminModuleId;

    /**
     *
     * @var AdminUser @ORM\ManyToOne(targetEntity="AppBundle\Entity\AdminUser",inversedBy="adminUserModulesUser")
     * @ORM\JoinColumn(name="admin_user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $adminUserId;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isAvailable
     *
     * @param boolean $isAvailable
     *
     * @return UserAdminModule
     */
    public function setIsAvailable($isAvailable)
    {
        $this->isAvailable = $isAvailable;

        return $this;
    }

    /**
     * Get isAvailable
     *
     * @return boolean
     */
    public function getIsAvailable()
    {
        return $this->isAvailable;
    }

    /**
     * Set adminModuleId
     *
     * @param \AppBundle\Entity\AdminModule $adminModuleId
     *
     * @return UserAdminModule
     */
    public function setAdminModuleId(\AppBundle\Entity\AdminModule $adminModuleId = null)
    {
        $this->adminModuleId = $adminModuleId;

        return $this;
    }

    /**
     * Get adminModuleId
     *
     * @return \AppBundle\Entity\AdminModule
     */
    public function getAdminModuleId()
    {
        return $this->adminModuleId;
    }

    /**
     * Set adminUserId
     *
     * @param \AppBundle\Entity\AdminUser $adminUserId
     *
     * @return UserAdminModule
     */
    public function setAdminUserId(\AppBundle\Entity\AdminUser $adminUserId = null)
    {
        $this->adminUserId = $adminUserId;

        return $this;
    }

    /**
     * Get adminUserId
     *
     * @return \AppBundle\Entity\AdminUser
     */
    public function getAdminUserId()
    {
        return $this->adminUserId;
    }
}
