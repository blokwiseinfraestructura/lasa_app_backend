<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AdminUser
 *
 * @ORM\Table(name="admin_user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AdminUserRepository")
 */
class AdminUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=60)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=60)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=60)
     */
    private $role;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="admin_token", type="string", length=255, nullable=true)
     */
    private $adminToken;

    /**
     * @var string
     *
     * @ORM\Column(name="recover_password_token", type="string", length=255, nullable=true)
     */
    private $recoverPasswordToken;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="recover_password_token_expires_at", type="datetime", nullable=true)
     */
    private $recoverPasswordTokenExpiresAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_at", type="datetime")
     */
    private $createAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_at", type="datetime", nullable=true)
     */
    private $updateAt;

    /**
     * @ORM\OneToMany(targetEntity="UserAdminModule", mappedBy="adminUserId")
     */
    protected $adminUserModulesUser;

    /**
     * @ORM\OneToMany(targetEntity="PushNotification", mappedBy="adminUserId")
     */
    protected $adminUserPushNotifications;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->adminUserModulesUser = new \Doctrine\Common\Collections\ArrayCollection();
        $this->adminUserPushNotifications = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return AdminUser
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return AdminUser
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set role
     *
     * @param string $role
     *
     * @return AdminUser
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return AdminUser
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return AdminUser
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set adminToken
     *
     * @param string $adminToken
     *
     * @return AdminUser
     */
    public function setAdminToken($adminToken)
    {
        $this->adminToken = $adminToken;

        return $this;
    }

    /**
     * Get adminToken
     *
     * @return string
     */
    public function getAdminToken()
    {
        return $this->adminToken;
    }

    /**
     * Set recoverPasswordToken
     *
     * @param string $recoverPasswordToken
     *
     * @return AdminUser
     */
    public function setRecoverPasswordToken($recoverPasswordToken)
    {
        $this->recoverPasswordToken = $recoverPasswordToken;

        return $this;
    }

    /**
     * Get recoverPasswordToken
     *
     * @return string
     */
    public function getRecoverPasswordToken()
    {
        return $this->recoverPasswordToken;
    }

    /**
     * Set recoverPasswordTokenExpiresAt
     *
     * @param \DateTime $recoverPasswordTokenExpiresAt
     *
     * @return AdminUser
     */
    public function setRecoverPasswordTokenExpiresAt($recoverPasswordTokenExpiresAt)
    {
        $this->recoverPasswordTokenExpiresAt = $recoverPasswordTokenExpiresAt;

        return $this;
    }

    /**
     * Get recoverPasswordTokenExpiresAt
     *
     * @return \DateTime
     */
    public function getRecoverPasswordTokenExpiresAt()
    {
        return $this->recoverPasswordTokenExpiresAt;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     *
     * @return AdminUser
     */
    public function setCreateAt($createAt)
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * Get createAt
     *
     * @return \DateTime
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * Set updateAt
     *
     * @param \DateTime $updateAt
     *
     * @return AdminUser
     */
    public function setUpdateAt($updateAt)
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    /**
     * Get updateAt
     *
     * @return \DateTime
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }

    /**
     * Add adminUserModulesUser
     *
     * @param \AppBundle\Entity\UserAdminModule $adminUserModulesUser
     *
     * @return AdminUser
     */
    public function addAdminUserModulesUser(\AppBundle\Entity\UserAdminModule $adminUserModulesUser)
    {
        $this->adminUserModulesUser[] = $adminUserModulesUser;

        return $this;
    }

    /**
     * Remove adminUserModulesUser
     *
     * @param \AppBundle\Entity\UserAdminModule $adminUserModulesUser
     */
    public function removeAdminUserModulesUser(\AppBundle\Entity\UserAdminModule $adminUserModulesUser)
    {
        $this->adminUserModulesUser->removeElement($adminUserModulesUser);
    }

    /**
     * Get adminUserModulesUser
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAdminUserModulesUser()
    {
        return $this->adminUserModulesUser;
    }

    /**
     * Add adminUserPushNotification
     *
     * @param \AppBundle\Entity\PushNotification $adminUserPushNotification
     *
     * @return AdminUser
     */
    public function addAdminUserPushNotification(\AppBundle\Entity\PushNotification $adminUserPushNotification)
    {
        $this->adminUserPushNotifications[] = $adminUserPushNotification;

        return $this;
    }

    /**
     * Remove adminUserPushNotification
     *
     * @param \AppBundle\Entity\PushNotification $adminUserPushNotification
     */
    public function removeAdminUserPushNotification(\AppBundle\Entity\PushNotification $adminUserPushNotification)
    {
        $this->adminUserPushNotifications->removeElement($adminUserPushNotification);
    }

    /**
     * Get adminUserPushNotifications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAdminUserPushNotifications()
    {
        return $this->adminUserPushNotifications;
    }
}
