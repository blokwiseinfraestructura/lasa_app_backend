<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Level
 *
 * @ORM\Table(name="level")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LevelRepository")
 */
class Level
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="level_description", type="string", length=45)
     */
    private $levelDescription;

    /**
     * @var int
     *
     * @ORM\Column(name="level_code", type="integer")
     */
    private $levelCode;

    /**
     * @ORM\OneToMany(targetEntity="Question", mappedBy="levelId")
     */
    protected $questionsPerLevel;


    /**
     * @ORM\OneToMany(targetEntity="AppModule", mappedBy="levelId")
     */
    protected $appModules;

    /**
     * @ORM\OneToMany(targetEntity="AppUser", mappedBy="levelId")
     */
    protected $appUserByLevel;

    /**
     * @ORM\OneToMany(targetEntity="Reading", mappedBy="levelId")
     */
    protected $readingsByLevel;

    /**
     * @ORM\OneToMany(targetEntity="FinalTestResult", mappedBy="levelId")
     */
    protected $finalTestResults;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->questionsPerLevel = new \Doctrine\Common\Collections\ArrayCollection();
        $this->appModules = new \Doctrine\Common\Collections\ArrayCollection();
        $this->appUserByLevel = new \Doctrine\Common\Collections\ArrayCollection();
        $this->readingsByLevel = new \Doctrine\Common\Collections\ArrayCollection();
        $this->finalTestResults = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set levelDescription
     *
     * @param string $levelDescription
     *
     * @return Level
     */
    public function setLevelDescription($levelDescription)
    {
        $this->levelDescription = $levelDescription;

        return $this;
    }

    /**
     * Get levelDescription
     *
     * @return string
     */
    public function getLevelDescription()
    {
        return $this->levelDescription;
    }

    /**
     * Set levelCode
     *
     * @param integer $levelCode
     *
     * @return Level
     */
    public function setLevelCode($levelCode)
    {
        $this->levelCode = $levelCode;

        return $this;
    }

    /**
     * Get levelCode
     *
     * @return integer
     */
    public function getLevelCode()
    {
        return $this->levelCode;
    }

    /**
     * Add questionsPerLevel
     *
     * @param \AppBundle\Entity\Question $questionsPerLevel
     *
     * @return Level
     */
    public function addQuestionsPerLevel(\AppBundle\Entity\Question $questionsPerLevel)
    {
        $this->questionsPerLevel[] = $questionsPerLevel;

        return $this;
    }

    /**
     * Remove questionsPerLevel
     *
     * @param \AppBundle\Entity\Question $questionsPerLevel
     */
    public function removeQuestionsPerLevel(\AppBundle\Entity\Question $questionsPerLevel)
    {
        $this->questionsPerLevel->removeElement($questionsPerLevel);
    }

    /**
     * Get questionsPerLevel
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestionsPerLevel()
    {
        return $this->questionsPerLevel;
    }

    /**
     * Add appModule
     *
     * @param \AppBundle\Entity\AppModule $appModule
     *
     * @return Level
     */
    public function addAppModule(\AppBundle\Entity\AppModule $appModule)
    {
        $this->appModules[] = $appModule;

        return $this;
    }

    /**
     * Remove appModule
     *
     * @param \AppBundle\Entity\AppModule $appModule
     */
    public function removeAppModule(\AppBundle\Entity\AppModule $appModule)
    {
        $this->appModules->removeElement($appModule);
    }

    /**
     * Get appModules
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAppModules()
    {
        return $this->appModules;
    }

    /**
     * Add appUserByLevel
     *
     * @param \AppBundle\Entity\AppUser $appUserByLevel
     *
     * @return Level
     */
    public function addAppUserByLevel(\AppBundle\Entity\AppUser $appUserByLevel)
    {
        $this->appUserByLevel[] = $appUserByLevel;

        return $this;
    }

    /**
     * Remove appUserByLevel
     *
     * @param \AppBundle\Entity\AppUser $appUserByLevel
     */
    public function removeAppUserByLevel(\AppBundle\Entity\AppUser $appUserByLevel)
    {
        $this->appUserByLevel->removeElement($appUserByLevel);
    }

    /**
     * Get appUserByLevel
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAppUserByLevel()
    {
        return $this->appUserByLevel;
    }

    /**
     * Add readingsByLevel
     *
     * @param \AppBundle\Entity\Reading $readingsByLevel
     *
     * @return Level
     */
    public function addReadingsByLevel(\AppBundle\Entity\Reading $readingsByLevel)
    {
        $this->readingsByLevel[] = $readingsByLevel;

        return $this;
    }

    /**
     * Remove readingsByLevel
     *
     * @param \AppBundle\Entity\Reading $readingsByLevel
     */
    public function removeReadingsByLevel(\AppBundle\Entity\Reading $readingsByLevel)
    {
        $this->readingsByLevel->removeElement($readingsByLevel);
    }

    /**
     * Get readingsByLevel
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReadingsByLevel()
    {
        return $this->readingsByLevel;
    }

    /**
     * Add finalTestResult
     *
     * @param \AppBundle\Entity\FinalTestResult $finalTestResult
     *
     * @return Level
     */
    public function addFinalTestResult(\AppBundle\Entity\FinalTestResult $finalTestResult)
    {
        $this->finalTestResults[] = $finalTestResult;

        return $this;
    }

    /**
     * Remove finalTestResult
     *
     * @param \AppBundle\Entity\FinalTestResult $finalTestResult
     */
    public function removeFinalTestResult(\AppBundle\Entity\FinalTestResult $finalTestResult)
    {
        $this->finalTestResults->removeElement($finalTestResult);
    }

    /**
     * Get finalTestResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFinalTestResults()
    {
        return $this->finalTestResults;
    }
}
