<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * UserImage
 *
 * @ORM\Table(name="user_image")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserImageRepository")
 * @ORM\HasLifecycleCallbacks
 */
class UserImage {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     *
     * @var AppUser @ORM\OneToOne(targetEntity="AppBundle\Entity\AppUser",inversedBy="userImage")
     * @ORM\JoinColumn(name="app_user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $appUserId;

    /**
     * @ORM\Column(name="imagePath", type="string", length=500)
     */
    private $imagePath;

    /**
     *
     * @Assert\File(maxSize="6000000")
     */
    private $file;
    private $temp;

    public function getAbsolutePath() {
        return null === $this->imagePath ? null : $this->getUploadRootDir() . '/' . $this->imagePath;
    }

    public function getWebPath() {
        return null === $this->imagePath ? null : $this->getUploadDir() . '/' . $this->imagePath;
    }

    protected function getUploadRootDir() {
        // la ruta absoluta del directorio donde se deben
        // guardar los archivos cargados
        return __DIR__ . '/../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir() {
        // se deshace del __DIR__ para no meter la pata
        // al mostrar el documento/imagen cargada en la vista.
        return 'uploads/userimages';
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload() {
        if (null !== $this->getFile()) {
            // haz lo que quieras para generar un nombre único
            $filename = sha1(uniqid());
            $this->imagePath = $filename . '.' . $this->getFile()->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload() {
        if (null === $this->getFile()) {
            return;
        }

        // si hay un error al mover el archivo, move() automáticamente
        // envía una excepción. This will properly prevent
        // the entity from being persisted to the database on error

        $this->getFile()->move($this->getUploadRootDir(), $this->imagePath);

        // check if we have an old image
        if (isset($this->temp)) {
            // delete the old image
            unlink($this->getUploadRootDir() . '/' . $this->temp);
            // clear the temp image path
            $this->temp = null;
        }
        $this->file = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload() {
        /* $file = $this->getAbsolutePath();

          if (file_exists($this->file) && is_writable($this->file)){
          unlink($file);
          } */

        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null) {
        $this->file = $file;
        // check if we have an old image path
        if (isset($this->imagePath)) {
            // store the old name to delete after the update
            $this->temp = $this->imagePath;
            $this->imagePath = null;
        } else {
            $this->imagePath = 'initial';
        }
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile() {
        return $this->file;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set imagePath
     *
     * @param string $imagePath
     *
     * @return UserImage
     */
    public function setImagePath($imagePath) {
        $this->imagePath = $imagePath;

        return $this;
    }

    /**
     * Get imagePath
     *
     * @return string
     */
    public function getImagePath() {
        return $this->imagePath;
    }


    /**
     * Set appUserId
     *
     * @param \AppBundle\Entity\AppUser $appUserId
     *
     * @return UserImage
     */
    public function setAppUserId(\AppBundle\Entity\AppUser $appUserId = null)
    {
        $this->appUserId = $appUserId;

        return $this;
    }

    /**
     * Get appUserId
     *
     * @return \AppBundle\Entity\AppUser
     */
    public function getAppUserId()
    {
        return $this->appUserId;
    }
}
