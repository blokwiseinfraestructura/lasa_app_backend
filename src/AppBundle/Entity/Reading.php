<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Reading
 *
 * @ORM\Table(name="reading")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReadingRepository")
 */
class Reading
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="reading_description", type="string", length=512)
     */
    private $readingDescription;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @Assert\File(maxSize="6000000")
     * @Assert\NotBlank(message="Please enter a valid file extention(png,jpg,jpeg,gif)")
     * @Assert\File(mimeTypes={ "image/png", "image/jpg", "image/jpeg", "image/gif" })
     * @Assert\Image(minWidth = 120,maxWidth = 2568,minHeight = 20,maxHeight = 1600)
     */
    private $readingImageName;

    /**
     *
     * @var Topic @ORM\ManyToOne(targetEntity="AppBundle\Entity\Topic",inversedBy="readingsByTopic")
     * @ORM\JoinColumn(name="topic_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $topicId;

    /**
     *
     * @var Level @ORM\ManyToOne(targetEntity="AppBundle\Entity\Level",inversedBy="readingsByLevel")
     * @ORM\JoinColumn(name="level_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $levelId;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set readingDescription
     *
     * @param string $readingDescription
     *
     * @return Reading
     */
    public function setReadingDescription($readingDescription)
    {
        $this->readingDescription = $readingDescription;

        return $this;
    }

    /**
     * Get readingDescription
     *
     * @return string
     */
    public function getReadingDescription()
    {
        return $this->readingDescription;
    }

    /**
     * Set readingImageName
     *
     * @param string $readingImageName
     *
     * @return Reading
     */
    public function setReadingImageName($readingImageName)
    {
        $this->readingImageName = $readingImageName;

        return $this;
    }

    /**
     * Get readingImageName
     *
     * @return string
     */
    public function getReadingImageName()
    {
        return $this->readingImageName;
    }

    /**
     * Set topicId
     *
     * @param \AppBundle\Entity\Topic $topicId
     *
     * @return Reading
     */
    public function setTopicId(\AppBundle\Entity\Topic $topicId = null)
    {
        $this->topicId = $topicId;

        return $this;
    }

    /**
     * Get topicId
     *
     * @return \AppBundle\Entity\Topic
     */
    public function getTopicId()
    {
        return $this->topicId;
    }

    /**
     * Set levelId
     *
     * @param \AppBundle\Entity\Level $levelId
     *
     * @return Reading
     */
    public function setLevelId(\AppBundle\Entity\Level $levelId = null)
    {
        $this->levelId = $levelId;

        return $this;
    }

    /**
     * Get levelId
     *
     * @return \AppBundle\Entity\Level
     */
    public function getLevelId()
    {
        return $this->levelId;
    }
}
